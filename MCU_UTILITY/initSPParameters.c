/*
 * initSPParameters.c
 *
 *  Created on: 31 mar 2019
 *      Author: Luca
 */

#include "initSPParameters.h"
#include <string.h>
#include "../API/level1/SPProtocol.h"
#include "../API/config/SPConfiguration.h"

#define MAX_STRING 16

void toUpperString(const char *src, char *dst) {
  for (uint8_t i = 0; i < MAX_STRING; i++) {
    dst[i] = 0;
  }
  for (uint8_t i = 0; i < strlen(src); i++) {
    dst[i] = (src[i] >= 97 && src[i] <= 122) ? src[i] - 32 : src[i];
  }
}

char tmp[MAX_STRING] = "\0";

void parseSPConfiguration(const char *key, const char *value) {
  if (strcmp(key, "description") == 0) {
    strcpy(spConfiguration.description, value);
  } else if (strcmp(key, "hostcontroller") == 0) {
    // strcpy(spConfiguration.host_controller, value);
  } else if (strcmp(key, "apiowner") == 0) {
    // strcpy(spConfiguration.api_owner, value);
  } else if (strcmp(key, "mcu") == 0) {
    // strcpy(spConfiguration.mcu, value);
    if (strcmp(value, "ESP8266") == 0) {
      // initSPProtocolESP8266();
    }
  } else if (strcmp(key, "protocol") == 0) {
    if (strcmp(value, "SENSIBUS") == 0) {
      // spConfiguration.protocol = &spProtocol;
      spConfiguration.driverName = PROTOCOL_SENSIBUS;
    } else if (strcmp(value, "I2C") == 0) {
      spConfiguration.driverName = PROTOCOL_I2C;
    } else if (strcmp(value, "SPI") == 0) {
      spConfiguration.driverName = PROTOCOL_SPI;
    }
  } else if (strcmp(key, "addressingtype") == 0) {
    if (strcmp(value, "NoAddress") == 0) {
      spConfiguration.protocol->addressingMode = NO_ADDRESS;
    } else if (strcmp(value, "ShortAddress") == 0) {
      spConfiguration.protocol->addressingMode = SHORT_ADDRESS;
    } else if (strcmp(value, "FullAddress") == 0) {
      spConfiguration.protocol->addressingMode = FULL_ADDRESS;
    }
  } else if (strcmp(key, "clusterid") == 0) {
    // spConfiguration.cluster = &spCluster;
  } else if (strcmp(key, "vcc") == 0) {

    toUpperString(value, tmp);

    //  strcpy(spConfiguration.vccStringValue, value);
    if (strcmp(tmp, "RATIOMETRIC") == 0) {
      spConfiguration.setVCC(&spConfiguration, VCC_RATIOMETRIC, NULL);
    } else {
      spConfiguration.setVCC(&spConfiguration, VCC_NORMAL, atoi(tmp));
    }
  }
}

void parseSPCluster(const char *key, const char *value) {
  if (strcmp(key, "clusterid") == 0) {
    spCluster.clusterID = strtoul(value + 2, NULL, 16);
    // strcpy(spCluster.clusterID, value);
  } else if (strcmp(key, "multicastcluster") == 0) {
    spCluster.multicastChip->methods->setSerialNumber(spCluster.multicastChip, value);
    // strcpy(spCluster.multicastAddress, value);
  }
}

void parseSPFamily(const char *key, const char *value) {
  if (strcmp(key, "familyname") == 0) {
    // strcpy(spFamily.name, value);
    // spFamily.measureTypeDim = 0;
  } else if (strcmp(key, "familyid") == 0) {
    spFamily.id = strtoul(value + 2, NULL, 16);
    // strcpy(spFamily.id, value);
  } else if (strcmp(key, "hwversion") == 0) {
    strcpy(spFamily.hwVersion, value);
  } else if (strcmp(key, "sysclock") == 0) {
    spFamily.sysClock = atoi(value);
  } else if (strcmp(key, "osctrim") == 0) {
    spFamily.oscTrim = strtoul(value + 2, NULL, 16);
    // strcpy(spFamily.oscTrim, value);
  } else if (strcmp(key, "measuretype") == 0) {
    // strcpy(spFamily.measureTypesList[spFamily.measureTypeDim++], value);
  } else if (strcmp(key, "multicastdefault") == 0) {
    // strcpy(spFamily.multicastDefaultAddress, value);
  } else if (strcmp(key, "broadcast") == 0) {
    // strcpy(spFamily.broadcastAddress, value);
    // strcpy(spCluster.broadcastAddress, value);
  } else if (strcmp(key, "broadcastslow") == 0) {
    // strcpy(spFamily.broadcastSlowAddress, value);
    // strcpy(spCluster.broadcastSlowAddress, value);
    spCluster.broadcastChip->methods->setSerialNumber(spCluster.broadcastChip, value);
  }
}

void parseSPChip(const char *key, const char *value, int chipCount) {
  if (strcmp(key, "familyid") == 0) {
    // if (strcmp(spFamily.id, value) == 0)
    // 	spCluster.chipList[chipCount].family = &spFamily;
    if (spFamily.id == strtoul(value + 2, NULL, 16))
      spCluster.chipList[chipCount].family = &spFamily;
  } else if (strcmp(key, "serialnumber") == 0) {
    // strcpy(spCluster.chipList[chipCount].serialNumber, value);
    spCluster.chipList[chipCount].methods->setSerialNumber(&spCluster.chipList[chipCount], value);
  } else if (strcmp(key, "i2CADDRESS") == 0) {
    // strcpy(spCluster.chipList[chipCount].i2cAddress, value);
    spCluster.chipList[chipCount].i2cAddress = strtoul(value + 2, NULL, 16);
  } else if (strcmp(key, "osctrim") == 0) {
    spCluster.chipList[chipCount].oscTrimOverride = spCluster.chipList[chipCount].family->oscTrim;
  }
}

void parseSPSensingElement(const char *key, const char *value, int count) {

  if (strcmp(key, "rsense") == 0 && strcmp(value, "null") != 0) {
    if (strcmp(value, "50") == 0) {
      spSensingElement[count].rSense.value = spSensingElement[count].rSense.items[RSENSE_50];
    } else if (strcmp(value, "500") == 0) {
      spSensingElement[count].rSense.value = spSensingElement[count].rSense.items[RSENSE_500];
    } else if (strcmp(value, "5000") == 0) {
      spSensingElement[count].rSense.value = spSensingElement[count].rSense.items[RSENSE_5000];
    } else if (strcmp(value, "50000") == 0) {
      spSensingElement[count].rSense.value = spSensingElement[count].rSense.items[RSENSE_50000];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "name") == 0) {
    strcpy(spSensingElement[count].name, value);
  } else if (strcmp(key, "inGain") == 0 && strcmp(value, "null") != 0) {
    if (strcmp(value, "1") == 0) {
      spSensingElement[count].inGain.value = spSensingElement[count].inGain.items[INGAIN_1];
    } else if (strcmp(value, "10") == 0) {
      spSensingElement[count].inGain.value = spSensingElement[count].inGain.items[INGAIN_10];
    } else if (strcmp(value, "20") == 0) {
      spSensingElement[count].inGain.value = spSensingElement[count].inGain.items[INGAIN_20];
    } else if (strcmp(value, "50") == 0 || strcmp(value, "40") == 0) {
      spSensingElement[count].inGain.value = spSensingElement[count].inGain.items[INGAIN_50];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "outGain") == 0 && strcmp(value, "null") != 0) {
    spSensingElement[count].outGain.value = atoi(value);
  } else if (strcmp(key, "contacts") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "TWO") == 0) {
      spSensingElement[count].contacts.value = spSensingElement[count].contacts.items[CONTACTS_TWO];
    } else if (strcmp(tmp, "FOUR") == 0) {
      spSensingElement[count].contacts.value = spSensingElement[count].contacts.items[CONTACTS_FOUR];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "frequency") == 0 && strcmp(value, "null") != 0) {
    spSensingElement[count].frequency = atof(value);
  } else if (strcmp(key, "modeVI") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "VOUT_IIN") == 0) {
      spSensingElement[count].modeVI.value = spSensingElement[count].modeVI.items[VOUT_IIN];
    } else if (strcmp(tmp, "VOUT_VIN") == 0) {
      spSensingElement[count].modeVI.value = spSensingElement[count].modeVI.items[VOUT_VIN];
    } else if (strcmp(tmp, "IOUT_IIN") == 0) {
      spSensingElement[count].modeVI.value = spSensingElement[count].modeVI.items[IOUT_IIN];
    } else if (strcmp(tmp, "IOUT_VIN") == 0) {
      spSensingElement[count].modeVI.value = spSensingElement[count].modeVI.items[IOUT_VIN];
    } else {
      printf("Error!!\n");
    }
  }
    // else if (strcmp(key, "measureTechnique") == 0)
    // {
    // 	strcpy(spSensingElement[count].measureTecnique, value);
    // }
  else if (strcmp(key, "measureType") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "IN_PHASE") == 0 || strcmp(tmp, "IN-PHASE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_IN_PHASE];
    } else if (strcmp(tmp, "QUADRATURE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_QUADRATURE];
    } else if (strcmp(tmp, "CONDUCTANCE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_CONDUCTANCE];
    } else if (strcmp(tmp, "SUSCEPTANCE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_SUSCEPTANCE];
    } else if (strcmp(tmp, "MODULE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_MODULE];
    } else if (strcmp(tmp, "PHASE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_PHASE];
    } else if (strcmp(tmp, "RESISTANCE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_RESISTANCE];
    } else if (strcmp(tmp, "CAPACITANCE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_CAPACITANCE];
    } else if (strcmp(tmp, "INDUCTANCE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_INDUCTANCE];
    } else if (strcmp(tmp, "VOLTAGE") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_VOLTAGE];
    } else if (strcmp(tmp, "CURRENT") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_CURRENT];
    } else if (strcmp(tmp, "DELTA_V_APPLIED") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_DELTA_V_APPLIED];
    } else if (strcmp(tmp, "CURRENT_APPLIED") == 0) {
      spSensingElement[count].measure.value = spSensingElement[count].measure.items[MEASURES_CURRENT_APPLIED];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "filter") == 0) {
    spSensingElement[count].filter.value = (int) log2(atoi(value));
  } else if (strcmp(key, "phaseShiftMode") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "QUADRANTS") == 0) {
      spSensingElement[count].phaseShiftMode.value = spSensingElement[count].phaseShiftMode.items[PHASESHIFTMODE_QUADRANTS];
    } else if (strcmp(tmp, "COARSE") == 0) {
      spSensingElement[count].phaseShiftMode.value = spSensingElement[count].phaseShiftMode.items[PHASESHIFTMODE_COARSE];
    } else if (strcmp(tmp, "FINE") == 0) {
      spSensingElement[count].phaseShiftMode.value = spSensingElement[count].phaseShiftMode.items[PHASESHIFTMODE_FINE];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "phaseShift") == 0 && strcmp(value, "null") != 0) {
    spSensingElement[count].phaseShift.value = atoi(value);
  } else if (strcmp(key, "iq") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "IN_PHASE") == 0) {
      spSensingElement[count].qi.value = spSensingElement[count].qi.items[QI_IN_PHASE];
    } else if (strcmp(tmp, "QUADRATURE") == 0) {
      spSensingElement[count].qi.value = spSensingElement[count].qi.items[QI_QUADRATURE];
    } else if (strcmp(tmp, "ANTI_PHASE") == 0) {
      spSensingElement[count].qi.value = spSensingElement[count].qi.items[QI_ANTI_PHASE];
    } else if (strcmp(tmp, "ANT_QUADRATURE") == 0) {
      spSensingElement[count].qi.value = spSensingElement[count].qi.items[QI_ANT_QUADRATURE];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "conversionRate") == 0) {
    spSensingElement[count].ADCConversionRate.value = atoi(value);
  } else if (strcmp(key, "inportADC") == 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "IA") == 0) {
      spSensingElement[count].inPortADC.value = spSensingElement[count].inPortADC.items[PORTADC_IA];
    } else if (strcmp(tmp, "MUX") == 0) {
      spSensingElement[count].inPortADC.value = spSensingElement[count].inPortADC.items[PORTADC_MUX];
    } else if (strcmp(tmp, "DAC") == 0) {
#if defined(RUN7) || defined(RUN8)
      spSensingElement[count].inPortADC.value = spSensingElement[count].inPortADC.items[PORTADC_DAC];
#endif
    } else if (strcmp(tmp, "PPR") == 0) {
#if defined(RUN7) || defined(RUN8)

      spSensingElement[count].inPortADC.value = spSensingElement[count].inPortADC.items[PORTADC_PPR];
#endif
    } else {
      printf("Error!!\n");
    }
  }
    // else if (strcmp(key, "measureUnit") == 0)
    // {
    // 	strcpy(spSensingElement[count].measureUnit, value);
    // }
    // else if (strcmp(key, "rangeMin") == 0)
    // {
    // 	spSensingElement[count].rangeMin = atof(value);
    // }
    // else if (strcmp(key, "rangeMax") == 0)
    // {
    // 	spSensingElement[count].rangeMax = atof(value);
    // }
    // else if (strcmp(key, "alarmThreshold") == 0)
    // {
    // 	spSensingElement[count].defaultAlarmThreshold = atof(value);
    // }
    // else if (strcmp(key, "multiplier") == 0)
    // {
    // 	spSensingElement[count].multipier = atof(value);
    // }
  else if (strcmp(key, "ndata") == 0) {
    spSensingElement[count].NData = atoi(value);
  } else if (strcmp(key, "harmonic") == 0 && strcmp(value, "null") != 0) {
    toUpperString(value, tmp);
    if (strcmp(tmp, "FIRST_HARMONIC") == 0) {
      spSensingElement[count].harmonic.value = spSensingElement[count].harmonic.items[FIRST_HARMONIC];
    } else if (strcmp(tmp, "SECOND_HARMONIC") == 0) {
      spSensingElement[count].harmonic.value = spSensingElement[count].harmonic.items[SECOND_HARMONIC];
    } else if (strcmp(tmp, "THIRD_HARMONIC") == 0) {
      spSensingElement[count].harmonic.value = spSensingElement[count].harmonic.items[THIRD_HARMONIC];
    } else {
      printf("Error!!\n");
    }
  } else if (strcmp(key, "dcbiasP") == 0 && strcmp(value, "null") != 0) {
    spSensingElement[count].dcBiasP.value = atoi(value);
  } else if (strcmp(key, "dcbiasN") == 0 && strcmp(value, "null") != 0) {
    spSensingElement[count].dcBiasN.value = atoi(value);
  }
}

int findPort(const char *value) {
  if (strcmp(value, "PORT0") == 0) {
    return PORT0;
  } else if (strcmp(value, "PORT1") == 0) {
    return PORT1;
  } else if (strcmp(value, "PORT2") == 0) {
    return PORT2;
  } else if (strcmp(value, "PORT3") == 0) {
    return PORT3;
  } else if (strcmp(value, "PORT4") == 0) {
    return PORT4;
  } else if (strcmp(value, "PORT5") == 0) {
    return PORT5;
  } else if (strcmp(value, "PORT6") == 0) {
    return PORT6;
  } else if (strcmp(value, "PORT7") == 0) {
    return PORT7;
  } else if (strcmp(value, "PORT8") == 0) {
    return PORT8;
  } else if (strcmp(value, "PORT9") == 0) {
    return PORT9;
  } else if (strcmp(value, "PORT10") == 0) {
    return PORT10;
  } else if (strcmp(value, "PORT11") == 0) {
    return PORT11;
  } else if (strcmp(value, "PORT_HP") == 0) {
    return PORT_HP;
  } else if (strcmp(value, "PORT_EXT1") == 0) {
    return PORT_EXT1;
  } else if (strcmp(value, "PORT_EXT2") == 0) {
    return PORT_EXT2;
  } else if (strcmp(value, "PORT_EXT3") == 0) {
    return PORT_EXT3;
  } else if (strcmp(value, "PORT_EXT1_1") == 0) {
    return PORT_EXT1_1;
  } else if (strcmp(value, "PORT_EXT2_1") == 0) {
    return PORT_EXT2_1;
  } else if (strcmp(value, "PORT_EXT3_1") == 0) {
    return PORT_EXT3_1;
  } else if (strcmp(value, "PORT_EXT1_2") == 0) {
    return PORT_EXT1_2;
  } else if (strcmp(value, "PORT_EXT3_2") == 0) {
    return PORT_EXT3_2;
  } else if (strcmp(value, "PORT_EXT3_3") == 0) {
    return PORT_EXT3_3;
  } else if (strcmp(value, "PORT_TEMPERATURE") == 0) {
    return PORT_TEMPERATURE;
  } else if (strcmp(value, "PORT_VOLTAGE") == 0) {
    return PORT_VOLTAGE;
  } else if (strcmp(value, "PORT_LIGHT") == 0) {
    return PORT_LIGHT;
  } else if (strcmp(value, "PORT_DARK") == 0) {
    return PORT_DARK;
  } else if (strcmp(value, "PORT_NA") == 0) {
    return PORT_NA;
  } else if (strcmp(value, "PORT_SHORT") == 0) {
    return PORT_SHORT;
  } else if (strcmp(value, "PORT_OPEN") == 0) {
    return PORT_OPEN;
  } else if (strcmp(value, "SHA") == 0) {
    return SHA;
  } else if (strcmp(value, "AUX") == 0) {
    return AUX;
  } else if (strcmp(value, "PORT_27") == 0) {
    return PORT_27;
  } else if (strcmp(value, "PORT_EXT1_3") == 0) {
    return PORT_EXT1_3;
  } else if (strcmp(value, "PORT_EXT2_2") == 0) {
    return PORT_EXT2_2;
  } else if (strcmp(value, "DPORT_INT") == 0) {
    return DPORT_INT;
  } else if (strcmp(value, "DPORT_RDY") == 0) {
    return DPORT_RDY;
  } else if (strcmp(value, "DPORT_SDO") == 0) {
    return DPORT_SDO;
  } else if (strcmp(value, "DPORT_SCL") == 0) {
    return DPORT_SCL;
  } else if (strcmp(value, "DPORT_CS") == 0) {
    return DPORT_CS;
  } else if (strcmp(value, "PORT_EXT1_SP2") == 0) {
    return PORT_EXT1_SP2;
  } else if (strcmp(value, "PORT_HP_ID") == 0) {
    return PORT_HP_ID;
  } else if (strcmp(value, "PORT_DAS_CSENSE") == 0) {
    return PORT_DAS_CSENSE;
  } else if (strcmp(value, "PORT_AUX_CSENSE") == 0) {
    return PORT_AUX_CSENSE;
  } else if (strcmp(value, "PORT_DRIVE0") == 0) {
    return PORT_DRIVE0;
  } else if (strcmp(value, "PORT_DRIVE1") == 0) {
    return PORT_DRIVE1;
  } else if (strcmp(value, "PORT_DRIVE2") == 0) {
    return PORT_DRIVE2;
  } else if (strcmp(value, "PORT_DRIVE3") == 0) {
    return PORT_DRIVE3;
  } else if (strcmp(value, "PORT_DRIVE4") == 0) {
    return PORT_DRIVE4;
  } else if (strcmp(value, "PORT_DRIVE5") == 0) {
    return PORT_DRIVE5;
  } else if (strcmp(value, "PORT_DRIVE6") == 0) {
    return PORT_DRIVE6;
  } else if (strcmp(value, "PORT_DRIVE7") == 0) {
    return PORT_DRIVE7;
  }
  return -1;
}

void parseSPSensingElementOnFamily(const char *key, const char *value, int count) {
  if (strcmp(key, "sensingelementid") == 0) {
    strcpy(spSeOnFamily[count].id, value);
  } else if (strcmp(key, "sensingelementname") == 0) {
    spSeOnFamily[count].spSensingElement = &spSensingElement[count];
  } else if (strcmp(key, "sensingelementport") == 0) {
    initSPPort(&spSeOnFamily[count].port);
    spSeOnFamily[count].port.portIndex = findPort(value);
  }
}

int findSeOnFamily(const char *value) {
  for (int i = 0; i < NUM_SE_ON_FAMILY; i++) {
    if (strcmp(spSeOnFamily[i].id, value) == 0) {
      return i;
    }
  }
  return -1;
}

void parseSPSensingElementOnChip(const char *key, const char *value, int chipCount, int seOnChipCount) {
  if (strcmp(key, "sensingelementid") == 0) {
    spSeOnChip[chipCount][seOnChipCount].spSensingElementOnFamily = &spSeOnFamily[findSeOnFamily(value)];
  }
}

void parseSPCalibrationParameters(const char *key, const char *value, int chipCount, int seOnChipCount) {
  if (strcmp(key, "m") == 0) {
    spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.m = atof(value);
  } else if (strcmp(key, "n") == 0) {
    spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.n = atof(value);
  } else if (strcmp(key, "threshold") == 0 && strcmp(value, "null") != 0) {
    spSeOnChip[chipCount][seOnChipCount].spCalibrationParameters.threshold = atof(value);
  }
}
