
/*
 * fileHandlerMCU.cpp
 *
 *  Created on: 16 mar 2019
 *      Author: Luca
 */

#include "../EXT_OPERATION/fileHandlerMCU.h"

extern "C" {
#include "JsonListener_c.h"
#include "JsonStreamingParser_c.h"
#include "./API/config/config.h"
}

#if defined (ARDUINO)

#include "SPIFFS.h"
#define SPIFF_BUFFER_SIZE 256

//typedef struct test_struct jsonTest;
//#define REDIR_DEBUG
#ifdef REDIR_DEBUG
extern HardwareSerial RedirSerial;
#endif

int writeFile(unsigned char value) {
  int out = -1;
  SPIFFS.begin(true);
  File file = SPIFFS.open("/data.txt", "a");
  if (!file) {
    //Serial.println("ERRORE CREAZIONE FILE");
    return out;
  }
  //Serial.printf("Scrivo: %c \n", value);
  out = file.print((char) value);
  file.close();
  //SPIFFS.end();

  return out;
}

void readFile() {

    SPIFFS.begin(true);
    File file;
#if defined(RUN9)
    if (!SPIFFS.exists("/data_run9_noaddr.txt")) {
        Serial.println("Configuration FILE doesn't exists.");
    } else {
        file = SPIFFS.open("/data_run9_noaddr.txt", "r");
        if (!file) {
            Serial.println("Configuration FILE opening error.");
            return;
        } else {
            Serial.println("Configuration FILE opened correctly.");
        }
    }
#elif defined(RUN8)
    if (!SPIFFS.exists("/data_run8_noaddr.txt")) {
        Serial.println("Configuration FILE doesn't exists.");
    } else {
        file = SPIFFS.open("/data_run8_noaddr.txt", "r");
        if (!file) {
            Serial.println("Configuration FILE opening error.");
            return;
        } else {
            Serial.println("Configuration FILE opened correctly.");
        }
    }
#endif

  JsonStreamingParser_c parser;
  initJsonStreamingParser(&parser);
  JsonListener_c listener;
  initJsonListener(&listener);
  parser.setListener(&parser, &listener);

  int toParse = 0;
  char buffer[SPIFF_BUFFER_SIZE];

  while (file.available()) {
    toParse = file.readBytes(buffer, SPIFF_BUFFER_SIZE);
    for (int i = 0; i < toParse; i++) {
      parser.parse(&parser, buffer[i]);
    }
  }

  file.close();
}


void deleteFile() {
  SPIFFS.begin(true);
  if (SPIFFS.exists("/data.txt")) {
    SPIFFS.remove("/data.txt");
  } else {
    //Serial.println("FILE INESISTENTE!!");
  }
}

void formatFile() {
  SPIFFS.begin(true);
  SPIFFS.format();
}

#endif

