/*
 * testSteamingParser_c.c
 *
 *  Created on: Jul 23, 2020
 *      Author: zen
 */

#include "JsonListener_c.h"

#include "initSPParameters.h"


void testWhitespace(JsonListener_c* this_c, char c) {
}

void testKey(JsonListener_c* this_c, char key[]) {
	strncpy(this_c->_lastkey, key, MAX_KEY_LENGTH);
}

void testValue(JsonListener_c* this_c, char value[]) {
	switch(this_c->_currentState){
	case IN_CONFIGURATION:
		parseSPConfiguration(this_c->_lastkey, value);
		break;
	case IN_CLUSTER:
		parseSPCluster(this_c->_lastkey, value);
		break;
	case IN_CHIP:
		parseSPChip(this_c->_lastkey, value, this_c->_chipCounter);
		break;
	case IN_FAMILY:
		parseSPFamily(this_c->_lastkey, value);
		break;
	case IN_SENSING_ELEMENT:
		if(!this_c->_allSeOnFamilySet){
			parseSPSensingElement(this_c->_lastkey, value, this_c->_seCounter);
		}
		break;
	case IN_SE_ON_CHIP:
		parseSPSensingElementOnChip(this_c->_lastkey, value, this_c->_chipCounter, this_c->_seOnChipCounter);
		break;
	case IN_CALIBRATION_PARAMETER:
		parseSPCalibrationParameters(this_c->_lastkey, value, this_c->_chipCounter, this_c->_seOnChipCounter);
		break;
	case IN_SE_ON_FAMILY:
		if(!this_c->_allSeOnFamilySet){
			parseSPSensingElementOnFamily(this_c->_lastkey, value, this_c->_seCounter);
		}
		break;
	default:
		break;
	}
}

void testStartDocument(JsonListener_c* this_c) {

}

void testEndDocument(JsonListener_c* this_c) {

	spCluster.dimChipList = this_c->_chipCounter+1;

//	Serial.printf("Family: %s  ID: %s\n", spCluster.familyOfChips->name, spCluster.familyOfChips->id);
//	Serial.printf("Family measureType: %s, %s, %s\n",spCluster.familyOfChips->measureTypesList[0],
//			spCluster.familyOfChips->measureTypesList[1], spCluster.familyOfChips->measureTypesList[2]);
//	Serial.printf("sensingelementonfamily: \n");
//	for(int i = 0; i < 25; i++){
//		Serial.printf("SensingElementOnFamily: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].id);
//		Serial.printf("SensingElementOnFamily port LABEL: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].port->portLabel);
//		Serial.printf("SensingElementOnFamily port VALUE: %s \n", spCluster.familyOfChips->spSensingElementOnFamilyList[i].port->portValue);
//		Serial.printf("SensingElementOnFamily sensing Element Name: %s \n", spFamily.spSensingElementOnFamilyList[i].spSensingElement->name);
//		Serial.printf("SensingElementOnFamily sensing Element Rsense: %s \n", spFamily.spSensingElementOnFamilyList[i].spSensingElement->rsense);
//	}
//	Serial.printf("\n\nSensingElementOnChip (chip 1): \n");
//	for(int i = 0; i < 16; i++){
//		Serial.printf("SensingElementOnChip: %s \n", spCluster.chipList[0].spSensingElementOnChipList[i].spSensingElementOnFamily->id);
//		Serial.printf("SensingElementOnChip m: %f \n", spCluster.chipList[0].spSensingElementOnChipList[i].spCalibrationParameters.m);
//		Serial.printf("SensingElementOnChip n: %f \n", spCluster.chipList[0].spSensingElementOnChipList[i].spCalibrationParameters.n);
//	}
//	Serial.printf("\n\nSensingElementOnChip (chip 2): \n");
//	for(int i = 0; i < 16; i++){
//		Serial.printf("SensingElementOnChip: %s \n", spCluster.chipList[1].spSensingElementOnChipList[i].spSensingElementOnFamily->id);
//		Serial.printf("SensingElementOnChip m: %f \n", spCluster.chipList[1].spSensingElementOnChipList[i].spCalibrationParameters.m);
//		Serial.printf("SensingElementOnChip n: %f \n", spCluster.chipList[1].spSensingElementOnChipList[i].spCalibrationParameters.n);
//	}

	this_c->_chipCounter = -1;
}

void testStartArray(JsonListener_c* this_c) {
	switch(this_c->_currentState){
	case IN_CLUSTER:
		//if(this->lastkey.equals("chip")){
		if (strncmp(this_c->_lastkey, "chip", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_CHIPLIST;
			this_c->_arrayState = IN_CHIPLIST;
		}
		break;
	case IN_FAMILY:
		//if (this->lastkey.equals("sensingelementonfamily")){
		if (strncmp(this_c->_lastkey, "sensingelementonfamily", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_SE_ON_FAMILY_LIST;
		this_c->_arrayState = IN_SE_ON_FAMILY_LIST;
		} else if (strncmp(this_c->_lastkey, "analyte", MAX_KEY_LENGTH) == 0){//if(this->lastkey.equals("analyte")){
			this_c->_currentState = IN_ANALYTE_LIST;
			this_c->_arrayState = IN_ANALYTE_LIST;
		}
		break;
	case IN_CHIP:
		//if (this->lastkey.equals("sensingelementonchip")){
		if (strncmp(this_c->_lastkey, "sensingelementonchip", MAX_KEY_LENGTH) == 0){
			this_c->_currentState  = IN_SE_ON_CHIP_LIST;
			this_c->_arrayState  = IN_SE_ON_CHIP_LIST;
		}
		break;

	default:
		break;
	}

}

void testEndArray(JsonListener_c* this_c) {
	switch(this_c->_currentState){
	case IN_SE_ON_FAMILY_LIST:
		this_c->_currentState = IN_FAMILY;
		this_c->_arrayState = IN_CHIPLIST;
		this_c->_allSeOnFamilySet = true;
		//Serial.printf("%d se on family!!!\n", seCounter+1);
		break;
	case IN_ANALYTE_LIST:
		this_c->_currentState = IN_FAMILY;
		this_c->_arrayState = IN_CHIPLIST;
		break;
	case IN_SE_ON_CHIP_LIST:
		this_c->_currentState = IN_CHIP;
		this_c->_arrayState = IN_CHIPLIST;
		//this->allSeOnChipSet = true;
		//Serial.printf("%d se on chip!!!\n", seOnChipCounter);
		//Serial.printf("%d calibration parameters!!!\n", calParamCounter+1);
		break;
	case IN_CHIPLIST:
		this_c->_currentState = IN_CLUSTER;
		this_c->_arrayState = NO_ARRAY;
		//Serial.printf("\n\nCHIP TOTALI %d\n\n", chipCounter+1);
		//Serial.printf("\n\n\n\n****************************************FINE CHIP LIST*************************************\n\n\n\n");
		//chipCounter = 0;
		break;
	default:
		break;
	}
}

void tetStartObject(JsonListener_c* this_c) {
	switch(this_c->_currentState){
	case START_OBJECT:
		this_c->_currentState = IN_CONFIGURATION;
		break;
	case IN_CONFIGURATION:
		//if(this->lastkey.equals("cluster")){
		if (strncmp(this_c->_lastkey, "cluster", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_CLUSTER;
		}
		break;
	case IN_CHIPLIST:
		this_c->_currentState = IN_CHIP;
		this_c->_chipCounter++;
		//Serial.printf("******************ENTRO NEL CHIP %d******************\n",chipCounter+1);
		this_c->_seCounter = this_c->_seOnChipCounter = this_c->_calParamCounter = -1;
		break;
	case IN_CHIP:
		//if(this->lastkey.equals("spFamily")){
		if (strncmp(this_c->_lastkey, "spFamily", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_FAMILY;
		}
		break;
	case IN_SE_ON_FAMILY_LIST:
		this_c->_currentState = IN_SE_ON_FAMILY;
		this_c->_seCounter++;
		break;
	case IN_SE_ON_FAMILY:
		//if(this->lastkey.equals("sensingElement")){
		if (strncmp(this_c->_lastkey, "sensingElement", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_SENSING_ELEMENT;
		}
		break;
	case IN_ANALYTE_LIST:
		this_c->_currentState = IN_ANALYTE;
		break;
	case IN_SE_ON_CHIP_LIST:
		this_c->_currentState = IN_SE_ON_CHIP;
		this_c->_seOnChipCounter++;
		break;
	case IN_SE_ON_CHIP:
		//if(this->lastkey.equals("sensingElementOnFamily")){
		if (strncmp(this_c->_lastkey, "sensingElementOnFamily", MAX_KEY_LENGTH) == 0){
			this_c->_currentState = IN_SE_ON_FAMILY;
		} else if (strncmp(this_c->_lastkey, "calibrationparameter", MAX_KEY_LENGTH) == 0){ //if(this->lastkey.equals("calibrationparameter")){
			this_c->_currentState = IN_CALIBRATION_PARAMETER;
			this_c->_calParamCounter++;
		}
		break;
	case IN_CLUSTER:
		this_c->_currentState = IN_CHIP;
		break;
	default:
		break;
	}
}

void testEndObject(JsonListener_c* this_c) {
	switch(this_c->_currentState){
	case IN_SENSING_ELEMENT:
		this_c->_currentState = IN_SE_ON_FAMILY;
		break;
	case IN_SE_ON_FAMILY:
		if(this_c->_arrayState == IN_SE_ON_FAMILY_LIST){
			this_c->_currentState = IN_SE_ON_FAMILY_LIST;
		} else if(this_c->_arrayState == IN_SE_ON_CHIP_LIST){
			this_c->_currentState = IN_SE_ON_CHIP;
		}
		break;
	case IN_ANALYTE:
		this_c->_currentState = IN_ANALYTE_LIST;
		break;
	case IN_FAMILY:
		this_c->_currentState = IN_CHIP;
		this_c->_allSeSet = true;
		break;
	case IN_CALIBRATION_PARAMETER:
		this_c->_currentState = IN_SE_ON_CHIP;
		break;
	case IN_SE_ON_CHIP:
		this_c->_currentState = IN_SE_ON_CHIP_LIST;
		break;
	case IN_CHIP:
		this_c->_currentState = IN_CHIPLIST;
		break;
	case IN_CLUSTER:
		this_c->_currentState = IN_CONFIGURATION;
		break;
	default:
		break;
	}
}

void initJsonListener(JsonListener_c* this_c){
	this_c->endObject = &testEndObject;
	this_c->startObject = &tetStartObject;
	this_c->endArray = &testEndArray;
	this_c->startArray = &testStartArray;
	this_c->endDocument = &testEndDocument;
	this_c->startDocument = &testStartDocument;
	this_c->value= &testValue;
	this_c->key = &testKey;
	this_c->whitespace = &testWhitespace;

	memset(&this_c->_lastkey[0], 0, sizeof(this_c->_lastkey)); //azzero lastkey
	this_c->_currentState = START_OBJECT;
	this_c->_arrayState = NO_ARRAY;
	this_c->_allSeSet = false;
	this_c->_allSeOnChipSet = false;
	this_c->_allSeOnFamilySet = false;

	this_c->_seCounter = -1;
	this_c->_seOnChipCounter = -1;
	this_c->_chipCounter = -1;
	this_c->_calParamCounter = -1;
}
