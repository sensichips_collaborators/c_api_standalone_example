/*
 * testSteamingParser_c.h
 *
 *  Created on: Jul 23, 2020
 *      Author: zen
 */

#ifndef MCU_UTILITY_TESTSTEAMINGPARSER_C_H_
#define MCU_UTILITY_TESTSTEAMINGPARSER_C_H_

#include <stdbool.h>


#define NO_ARRAY					0

#define START_OBJECT			 	0
#define IN_CONFIGURATION		 	1
#define IN_CLUSTER			 		2
#define IN_CHIPLIST			 		3
#define IN_CHIP				 		4
#define IN_FAMILY			 		5
#define IN_SE_ON_FAMILY_LIST		6
#define IN_SE_ON_FAMILY				7
#define IN_SENSING_ELEMENT	  	    8
#define IN_ANALYTE_LIST		   	    9
#define IN_ANALYTE				   10
#define IN_SE_ON_CHIP_LIST		   11
#define IN_SE_ON_CHIP			   12
#define IN_CALIBRATION_PARAMETER   13
#define IN_PORT

#define MAX_KEY_LENGTH  50

typedef struct JsonListener_c JsonListener_c;

struct JsonListener_c{
	/* Don't call directly -> private  */
	/* Structure variables */
	char _lastkey[MAX_KEY_LENGTH];  // 512???
	int _currentState;
	int _arrayState;
	bool _allSeSet;
	bool _allSeOnChipSet;
	bool _allSeOnFamilySet;

	int _seCounter;
	int _seOnChipCounter;
	int _chipCounter;
	int _calParamCounter;

	 /* Access functions -> public  */
	void (*whitespace)(JsonListener_c* this_c, char c);
	void (*startDocument)(JsonListener_c* this_c);
	void (*key)(JsonListener_c* this_c, char key[]);
	void (*value)(JsonListener_c* this_c, char value[]);
	void (*endArray)(JsonListener_c* this_c);
	void (*endObject)(JsonListener_c* this_c);
	void (*endDocument)(JsonListener_c* this_c);
	void (*startArray)(JsonListener_c* this_c);
	void (*startObject)(JsonListener_c* this_c);
};

void initJsonListener(JsonListener_c* this_c);

#endif /* MCU_UTILITY_TESTSTEAMINGPARSER_C_H_ */
