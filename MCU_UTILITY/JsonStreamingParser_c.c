/*
 * JsonStreamingParser_c.c
 *
 *  Created on: Jul 22, 2020
 *      Author: zen
 */

#include <string.h>
#include "JsonStreamingParser_c.h"

#define _min(a, b) ((a)<(b)?(a):(b))

void reset(JsonStreamingParser_c *this_c) {
  this_c->_state = STATE_START_DOCUMENT;
  this_c->_bufferPos = 0;
  this_c->_unicodeEscapeBufferPos = 0;
  this_c->_unicodeBufferPos = 0;
  this_c->_characterCounter = 0;
}

void parse(JsonStreamingParser_c *this_c, char c) {
  //System.out.print(c);
  // valid whitespace characters in JSON (from RFC4627 for JSON) include:
  // space, horizontal tab, line feed or new line, and carriage return.
  // thanks:
  // http://stackoverflow.com/questions/16042274/definition-of-whitespace-in-json
  if ((c == ' ' || c == '\t' || c == '\n' || c == '\r')
      && !(this_c->_state == STATE_IN_STRING || this_c->_state == STATE_UNICODE || this_c->_state == STATE_START_ESCAPE
           || this_c->_state == STATE_IN_NUMBER || this_c->_state == STATE_START_DOCUMENT)) {
    return;
  }
  switch (this_c->_state) {
    case STATE_IN_STRING:
      if (c == '"') {
        this_c->_endString(this_c); //spett
      } else if (c == '\\') {
        this_c->_state = STATE_START_ESCAPE;
      } else if ((c < 0x1f) || (c == 0x7f)) {
        //throw new RuntimeException("Unescaped control character encountered: " + c + " at position" + characterCounter);
      } else {
        this_c->_buffer[this_c->_bufferPos] = c;
        this_c->_increaseBufferPointer(this_c);
      }
      break;
    case STATE_IN_ARRAY:
      if (c == ']') {
        this_c->_endArray(this_c);
      } else {
        this_c->_startValue(this_c, c);
      }
      break;
    case STATE_IN_OBJECT:
      if (c == '}') {
        this_c->_endObject(this_c);
      } else if (c == '"') {
        this_c->_startKey(this_c);
      } else {
        //throw new RuntimeException("Start of string expected for object key. Instead got: " + c + " at position" + characterCounter);
      }
      break;
    case STATE_END_KEY:
      if (c != ':') {
        //throw new RuntimeException("Expected ':' after key. Instead got " + c + " at position" + characterCounter);
      }
      this_c->_state = STATE_AFTER_KEY;
      break;
    case STATE_AFTER_KEY:
      this_c->_startValue(this_c, c);
      break;
    case STATE_START_ESCAPE:
      this_c->_processEscapeCharacters(this_c, c);
      break;
    case STATE_UNICODE:
      this_c->_processUnicodeCharacter(this_c, c);
      break;
    case STATE_UNICODE_SURROGATE:
      this_c->_unicodeEscapeBuffer[this_c->_unicodeEscapeBufferPos] = c;
      this_c->_unicodeEscapeBufferPos++;
      if (this_c->_unicodeEscapeBufferPos == 2) {
        this_c->_endUnicodeSurrogateInterstitial(this_c);
      }
      break;
    case STATE_AFTER_VALUE: {
      // not safe for size == 0!!!
      int within = this_c->_stack[this_c->_stackPos - 1];
      if (within == STACK_OBJECT) {
        if (c == '}') {
          this_c->_endObject(this_c);
        } else if (c == ',') {
          this_c->_state = STATE_IN_OBJECT;
        } else {
          //throw new RuntimeException("Expected ',' or '}' while parsing object. Got: " + c + ". " + characterCounter);
        }
      } else if (within == STACK_ARRAY) {
        if (c == ']') {
          this_c->_endArray(this_c);
        } else if (c == ',') {
          this_c->_state = STATE_IN_ARRAY;
        } else {
          //throw new RuntimeException("Expected ',' or ']' while parsing array. Got: " + c + ". " + characterCounter);

        }
      } else {
        //throw new RuntimeException("Finished a literal, but unclear what state to move to. Last state: " + characterCounter);
      }
    }
      break;
    case STATE_IN_NUMBER:
      if (c >= '0' && c <= '9') {
        this_c->_buffer[this_c->_bufferPos] = c;
        this_c->_increaseBufferPointer(this_c);
      } else if (c == '.') {
        if (this_c->_doesCharArrayContain(this_c, this_c->_buffer, this_c->_bufferPos, '.')) {
          //throw new RuntimeException("Cannot have multiple decimal points in a number. " + characterCounter);
        } else if (this_c->_doesCharArrayContain(this_c, this_c->_buffer, this_c->_bufferPos, 'e')) {
          //throw new RuntimeException("Cannot have a decimal point in an exponent." + characterCounter);
        }
        this_c->_buffer[this_c->_bufferPos] = c;
        this_c->_increaseBufferPointer(this_c);
      } else if (c == 'e' || c == 'E') {
        if (this_c->_doesCharArrayContain(this_c, this_c->_buffer, this_c->_bufferPos, 'e')) {
          //throw new RuntimeException("Cannot have multiple exponents in a number. " + characterCounter);
        }
        this_c->_buffer[this_c->_bufferPos] = c;
        this_c->_increaseBufferPointer(this_c);
      } else if (c == '+' || c == '-') {
        char last = this_c->_buffer[this_c->_bufferPos - 1];
        if (!(last == 'e' || last == 'E')) {
          //throw new RuntimeException("Can only have '+' or '-' after the 'e' or 'E' in a number." + characterCounter);
        }
        this_c->_buffer[this_c->_bufferPos] = c;
        this_c->_increaseBufferPointer(this_c);
      } else {
        this_c->_endNumber(this_c);
        // we have consumed one beyond the end of the number
        this_c->parse(this_c, c);
      }
      break;
    case STATE_IN_TRUE:
      this_c->_buffer[this_c->_bufferPos] = c;
      this_c->_increaseBufferPointer(this_c);
      if (this_c->_bufferPos == 4) {
        this_c->_endTrue(this_c);
      }
      break;
    case STATE_IN_FALSE:
      this_c->_buffer[this_c->_bufferPos] = c;
      this_c->_increaseBufferPointer(this_c);
      if (this_c->_bufferPos == 5) {
        this_c->_endFalse(this_c);
      }
      break;
    case STATE_IN_NULL:
      this_c->_buffer[this_c->_bufferPos] = c;
      this_c->_increaseBufferPointer(this_c);
      if (this_c->_bufferPos == 4) {
        this_c->_endNull(this_c);
      }
      break;
    case STATE_START_DOCUMENT:
      //myListener->startDocument(); !!!
      this_c->myListener->startDocument(this_c->myListener); //che in realtà fa nulla
      if (c == '[') {
        this_c->_startArray(this_c);
      } else if (c == '{') {
        this_c->_startObject(this_c);
      } else {
        // throw new ParsingError($this->_line_number,
        // $this->_char_number,
        // "Document must start with object or array.");
      }
      break;
      //case STATE_DONE:
      // throw new ParsingError($this->_line_number, $this->_char_number,
      // "Expected end of document.");
      //default:
      // throw new ParsingError($this->_line_number, $this->_char_number,
      // "Internal error. Reached an unknown state: ".$this->_state);
  }
  this_c->_characterCounter++;
}

void increaseBufferPointer(JsonStreamingParser_c *this_c) {
  this_c->_bufferPos = _min(this_c->_bufferPos + 1, BUFFER_MAX_LENGTH - 1);
}

void endString(JsonStreamingParser_c *this_c) {
  int popped = this_c->_stack[this_c->_stackPos - 1];
  this_c->_stackPos--;
  if (popped == STACK_KEY) {
    this_c->_buffer[this_c->_bufferPos] = '\0';
    //myListener->key(String(buffer)); LISTENER
    this_c->myListener->key(this_c->myListener, this_c->_buffer);
    this_c->_state = STATE_END_KEY;
  } else if (popped == STACK_STRING) {
    this_c->_buffer[this_c->_bufferPos] = '\0';
    // myListener->value(String(buffer)); LISTENER
    this_c->myListener->value(this_c->myListener, this_c->_buffer);
    this_c->_state = STATE_AFTER_VALUE;
  } else {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Unexpected end of string.");
  }
  this_c->_bufferPos = 0;
}

void startValue(JsonStreamingParser_c *this_c, char c) {
  if (c == '[') {
    this_c->_startArray(this_c);
  } else if (c == '{') {
    this_c->_startObject(this_c);
  } else if (c == '"') {
    this_c->_startString(this_c);
  } else if (this_c->_isDigit(this_c, c)) {
    this_c->_startNumber(this_c, c);
  } else if (c == 't') {
    this_c->_state = STATE_IN_TRUE;
    this_c->_buffer[this_c->_bufferPos] = c;
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'f') {
    this_c->_state = STATE_IN_FALSE;
    this_c->_buffer[this_c->_bufferPos] = c;
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'n') {
    this_c->_state = STATE_IN_NULL;
    this_c->_buffer[this_c->_bufferPos] = c;
    this_c->_increaseBufferPointer(this_c);
  } else {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Unexpected character for value: ".$c);
  }
}

bool isDigit(JsonStreamingParser_c *this_c, char c) {
  // Only concerned with the first character in a number.
  return (c >= '0' && c <= '9') || c == '-';
}

void endArray(JsonStreamingParser_c *this_c) {
  int popped = this_c->_stack[this_c->_stackPos - 1];
  this_c->_stackPos--;
  if (popped != STACK_ARRAY) {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Unexpected end of array encountered.");
  }
  //myListener->endArray(); ///LISTENER
  this_c->myListener->endArray(this_c->myListener);
  this_c->_state = STATE_AFTER_VALUE;
  if (this_c->_stackPos == 0) {
    this_c->_endDocument(this_c);
  }
}

void startKey(JsonStreamingParser_c *this_c) {
  this_c->_stack[this_c->_stackPos] = STACK_KEY;
  this_c->_stackPos++;
  this_c->_state = STATE_IN_STRING;
}

void endObject(JsonStreamingParser_c *this_c) {
  int popped = this_c->_stack[this_c->_stackPos];
  this_c->_stackPos--;
  if (popped != STACK_OBJECT) {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Unexpected end of object encountered.");
  }
  // myListener->endObject(); ///LISTENER
  this_c->myListener->endObject(this_c->myListener);
  this_c->_state = STATE_AFTER_VALUE;
  if (this_c->_stackPos == 0) {
    this_c->_endDocument(this_c);
  }
}

void processEscapeCharacters(JsonStreamingParser_c *this_c, char c) {
  if (c == '"') {
    this_c->_buffer[this_c->_bufferPos] = '"';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == '\\') {
    this_c->_buffer[this_c->_bufferPos] = '\\';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == '/') {
    this_c->_buffer[this_c->_bufferPos] = '/';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'b') {
    this_c->_buffer[this_c->_bufferPos] = 0x08;
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'f') {
    this_c->_buffer[this_c->_bufferPos] = '\f';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'n') {
    this_c->_buffer[this_c->_bufferPos] = '\n';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'r') {
    this_c->_buffer[this_c->_bufferPos] = '\r';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 't') {
    this_c->_buffer[this_c->_bufferPos] = '\t';
    this_c->_increaseBufferPointer(this_c);
  } else if (c == 'u') {
    this_c->_state = STATE_UNICODE;
  } else {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Expected escaped character after backslash. Got: ".$c);
  }
  if (this_c->_state != STATE_UNICODE) {
    this_c->_state = STATE_IN_STRING;
  }
}

void processUnicodeCharacter(JsonStreamingParser_c *this_c, char c) {
  if (!this_c->_isHexCharacter(this_c, c)) {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Expected hex character for escaped Unicode character. Unicode parsed: "
    // . implode($this->_unicode_buffer) . " and got: ".$c);
  }

  this_c->_unicodeBuffer[this_c->_unicodeBufferPos] = c;
  this_c->_unicodeBufferPos++;

  if (this_c->_unicodeBufferPos == 4) {
    int codepoint = this_c->_getHexArrayAsDecimal(this_c, this_c->_unicodeBuffer, this_c->_unicodeBufferPos);
    this_c->_endUnicodeCharacter(this_c, codepoint);
    return;
    /*if (codepoint >= 0xD800 && codepoint < 0xDC00) {
      unicodeHighSurrogate = codepoint;
      unicodeBufferPos = 0;
      state = STATE_UNICODE_SURROGATE;
    } else if (codepoint >= 0xDC00 && codepoint <= 0xDFFF) {
      if (unicodeHighSurrogate == -1) {
        // throw new ParsingError($this->_line_number,
        // $this->_char_number,
        // "Missing high surrogate for Unicode low surrogate.");
      }
      int combinedCodePoint = ((unicodeHighSurrogate - 0xD800) * 0x400) + (codepoint - 0xDC00) + 0x10000;
      endUnicodeCharacter(combinedCodePoint);
    } else if (unicodeHighSurrogate != -1) {
      // throw new ParsingError($this->_line_number,
      // $this->_char_number,
      // "Invalid low surrogate following Unicode high surrogate.");
      endUnicodeCharacter(codepoint);
    } else {
      endUnicodeCharacter(codepoint);
    }*/
  }
}

bool isHexCharacter(JsonStreamingParser_c *this_c, char c) {
  return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

int getHexArrayAsDecimal(JsonStreamingParser_c *this_c, char hexArray[], int length) {
  int result = 0;
  for (int i = 0; i < length; i++) {
    char current = hexArray[length - i - 1];
    int value = 0;
    if (current >= 'a' && current <= 'f') {
      value = current - 'a' + 10;
    } else if (current >= 'A' && current <= 'F') {
      value = current - 'A' + 10;
    } else if (current >= '0' && current <= '9') {
      value = current - '0';
    }
    result += value * 16 ^ i;
  }
  return result;
}

bool doesCharArrayContain(JsonStreamingParser_c *this_c, char myArray[], int length, char c) {
  for (int i = 0; i < length; i++) {
    if (myArray[i] == c) {
      return true;
    }
  }
  return false;
}

void endUnicodeSurrogateInterstitial(JsonStreamingParser_c *this_c) {
  char unicodeEscape = this_c->_unicodeEscapeBuffer[this_c->_unicodeEscapeBufferPos - 1];
  if (unicodeEscape != 'u') {
    // throw new ParsingError($this->_line_number, $this->_char_number,
    // "Expected '\\u' following a Unicode high surrogate. Got: " .
    // $unicode_escape);
  }
  this_c->_unicodeBufferPos = 0;
  this_c->_unicodeEscapeBufferPos = 0;
  this_c->_state = STATE_UNICODE;
}

void endNumber(JsonStreamingParser_c *this_c) {
  this_c->_buffer[this_c->_bufferPos] = '\0';
  // String value = String(buffer);
  //float result = 0.0;
  //if (doesCharArrayContain(buffer, bufferPos, '.')) {
  //  result = value.toFloat();
  //} else {
  // needed special treatment in php, maybe not in Java and c
  //  result = value.toFloat();
  //}
  //myListener->value(value.c_str());
  this_c->myListener->value(this_c->myListener, this_c->_buffer);
  this_c->_bufferPos = 0;
  this_c->_state = STATE_AFTER_VALUE;
}

int convertDecimalBufferToInt(JsonStreamingParser_c *this_c, char myArray[], int length) {
  int result = 0;
  for (int i = 0; i < length; i++) {
    char current = myArray[length - i - 1];
    result += (current - '0') * 10;
  }
  return result;
}

void endDocument(JsonStreamingParser_c *this_c) {
  //myListener->endDocument();
  this_c->myListener->endDocument(this_c->myListener);
  this_c->_state = STATE_DONE;
}

void endTrue(JsonStreamingParser_c *this_c) {
  this_c->_buffer[this_c->_bufferPos] = '\0';
  // String value = String(this_c->_buffer); LISTENER
  //if (value.equals("true")) {
  //  myListener->value("true");
  // } else {
  // throw new ParsingError($this->_line_number, $this->_char_number,
  // "Expected 'true'. Got: ".$true);
  // }
  if (strncmp(this_c->_buffer, "true", MAX_KEY_LENGTH) == 0) {
    this_c->myListener->value(this_c->myListener, "true");
  }
  this_c->_bufferPos = 0;
  this_c->_state = STATE_AFTER_VALUE;
}

void endFalse(JsonStreamingParser_c *this_c) {
  this_c->_buffer[this_c->_bufferPos] = '\0';
  //String value = String(buffer); LISTENER
  //if (value.equals("false")) {
  // myListener->value("false");
  //  } else {
  // throw new ParsingError($this->_line_number, $this->_char_number,
  // "Expected 'true'. Got: ".$true);
  // }
  if (strncmp(this_c->_buffer, "false", MAX_KEY_LENGTH) == 0) {
    this_c->myListener->value(this_c->myListener, "false");
  }
  this_c->_bufferPos = 0;
  this_c->_state = STATE_AFTER_VALUE;
}

void endNull(JsonStreamingParser_c *this_c) {
  this_c->_buffer[this_c->_bufferPos] = '\0';
  //String value = String(buffer);
  //if (value.equals("null")) { LISTENER
  //myListener->value("null");
  //} else {
  // throw new ParsingError($this->_line_number, $this->_char_number,
  // "Expected 'true'. Got: ".$true);
  // }
  if (strncmp(this_c->_buffer, "null", MAX_KEY_LENGTH) == 0) {
    this_c->myListener->value(this_c->myListener, "null");
  }
  this_c->_bufferPos = 0;
  this_c->_state = STATE_AFTER_VALUE;
}

void startArray(JsonStreamingParser_c *this_c) {
  //myListener->startArray(); LISTENER
  this_c->myListener->startArray(this_c->myListener);
  this_c->_state = STATE_IN_ARRAY;
  this_c->_stack[this_c->_stackPos] = STACK_ARRAY;
  this_c->_stackPos++;
}

void startObject(JsonStreamingParser_c *this_c) {
  // myListener->startObject(); listener
  this_c->myListener->startObject(this_c->myListener);
  this_c->_state = STATE_IN_OBJECT;
  this_c->_stack[this_c->_stackPos] = STACK_OBJECT;
  this_c->_stackPos++;
}

void startString(JsonStreamingParser_c *this_c) {
  this_c->_stack[this_c->_stackPos] = STACK_STRING;
  this_c->_stackPos++;
  this_c->_state = STATE_IN_STRING;
}

void startNumber(JsonStreamingParser_c *this_c, char c) {
  this_c->_state = STATE_IN_NUMBER;
  this_c->_buffer[this_c->_bufferPos] = c;
  this_c->_increaseBufferPointer(this_c);
}

void endUnicodeCharacter(JsonStreamingParser_c *this_c, int codepoint) {
  this_c->_buffer[this_c->_bufferPos] = this_c->_convertCodepointToCharacter(this_c, codepoint);
  this_c->_increaseBufferPointer(this_c);
  this_c->_unicodeBufferPos = 0;
  this_c->_unicodeHighSurrogate = -1;
  this_c->_state = STATE_IN_STRING;
}

char convertCodepointToCharacter(JsonStreamingParser_c *this_c, int num) {
  if (num <= 0x7F)
    return (char) (num);
  // if(num<=0x7FF) return (char)((num>>6)+192) + (char)((num&63)+128);
  // if(num<=0xFFFF) return
  // chr((num>>12)+224).chr(((num>>6)&63)+128).chr((num&63)+128);
  // if(num<=0x1FFFFF) return
  // chr((num>>18)+240).chr(((num>>12)&63)+128).chr(((num>>6)&63)+128).chr((num&63)+128);
  return ' ';
}

void setListener(JsonStreamingParser_c *this_c, JsonListener_c *listener) {
  this_c->myListener = listener;
}

void initJsonStreamingParser(JsonStreamingParser_c *this_c) {
  this_c->setListener = &setListener;
  this_c->_convertCodepointToCharacter = &convertCodepointToCharacter;
  this_c->_endUnicodeCharacter = &endUnicodeCharacter;
  this_c->_startNumber = &startNumber;
  this_c->_startString = &startString;
  this_c->_startObject = &startObject;
  this_c->_startArray = &startArray;
  this_c->_endNull = &endNull;
  this_c->_endFalse = &endFalse;
  this_c->_endTrue = &endTrue;
  this_c->_endDocument = &endDocument;
  this_c->_convertDecimalBufferToInt = &convertDecimalBufferToInt;
  this_c->_endNumber = &endNumber;
  this_c->_endUnicodeSurrogateInterstitial = &endUnicodeSurrogateInterstitial;
  this_c->_doesCharArrayContain = &doesCharArrayContain;
  this_c->_getHexArrayAsDecimal = &getHexArrayAsDecimal;
  this_c->_isHexCharacter = &isHexCharacter;
  this_c->_processUnicodeCharacter = &processUnicodeCharacter;
  this_c->_processEscapeCharacters = &processEscapeCharacters;
  this_c->_endObject = &endObject;
  this_c->_startKey = &startKey;
  this_c->_endArray = &endArray;
  this_c->_isDigit = &isDigit;
  this_c->_startValue = &startValue;
  this_c->_endString = &endString;
  this_c->parse = &parse;
  this_c->_increaseBufferPointer = &increaseBufferPointer;
  this_c->reset = &reset;

  this_c->_stackPos = 0;
  this_c->_unicodeHighSurrogate = 0;
  this_c->reset(this_c);
}


