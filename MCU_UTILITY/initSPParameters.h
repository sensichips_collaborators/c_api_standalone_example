/*
 * initSPParameters.h
 *
 *  Created on: 31 mar 2019
 *      Author: Luca
 */

#ifndef INITSPPARAMETERS_H_
#define INITSPPARAMETERS_H_

#include "../API/config/init.h"

#include "../API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"

void parseSPConfiguration(const char* key, const char* value);
void parseSPCluster(const char* key, const char* value);
void parseSPChip(const char* key, const char* value, int chipCount);
void parseSPFamily(const char* key, const char* value);
void parseSPSensingElement(const char* key, const char* value, int count);
void parseSPSensingElementOnFamily(const char* key, const char* value, int count);
void parseSPSensingElementOnChip(const char* key, const char* value,  int chipCount, int seOnChipCount);
void parseSPCalibrationParameters(const char* key, const char* value,  int chipCount, int seOnChipCount);


#endif /* INITSPPARAMETERS_H_ */
