
/*
 * printHandler.c
 *
 *  Created on: 06 mag 2018
 *      Author: Luca Gerevini
 */

//#include "printPC.h"
extern "C"
{
#include "../API/util/printMCU.h"
}

#if defined(_LINUX_) || (_WIN32)
#include <stdio.h>

void print(const char *toPrint, ...)
{
	char buff[DIM_BUFFER];
	va_list list;
	va_start(list, toPrint);
	size_t len = vsnprintf(NULL, 0, toPrint, list);
	len = vsnprintf(buff, len + 1, toPrint, list);
	printf("%s", buff);
	fflush(stdout);
	va_end(list);
}

#elif defined(ARDUINO)

#include "Arduino.h"
//#include "../../ardPrint.h"
//#define REDIR_DEBUG
#ifdef REDIR_DEBUG
extern HardwareSerial RedirSerial;
#endif

void print(const char *toPrint, ...)
{
	if (PRINT)
	{
		char buff[DIM_BUFFER];
		va_list list;
		va_start(list, toPrint);
		size_t len = vsnprintf(NULL, 0, toPrint, list);
		len = vsnprintf(buff, len + 1, toPrint, list);
		Serial.printf("%s", buff);
		Serial.flush();
		va_end(list);
	}
}

void print_deb(const char *toPrint, ...)
{
	char buff[DIM_BUFFER];
	va_list list;
	va_start(list, toPrint);
	size_t len = vsnprintf(NULL, 0, toPrint, list);
	len = vsnprintf(buff, len + 1, toPrint, list);

#ifdef REDIR_DEBUG
	RedirSerial.printf("%s", buff);
	RedirSerial.flush();
#else
	Serial.printf("%s", buff);
	Serial.flush();
#endif

	va_end(list);
}

void print_f(const char *toPrint, ...)
{
	if (PRINT)
	{
		char buff[DIM_BUFFER];
		va_list list;
		va_start(list, toPrint);
		size_t len = vsnprintf(NULL, 0, toPrint, list);
		len = vsnprintf(buff, len + 1, toPrint, list);
		Serial.printf("%s", buff);
		Serial.flush();
		va_end(list);
	}
}

// void arddelay(int milliseconds){
//	delay(milliseconds);
// }

void _DEBUG_(const char *msg_type, char *toPrint, int debLvl)
{
	if (debLvl == 0 && DEBUG_LEVEL_0)
	{
		print_deb("--\t\tL0: %s %s \n", msg_type, toPrint);
	}
	else if (debLvl == 1 && DEBUG_LEVEL_1)
	{
		print_deb("--\tL1: %s %s", msg_type, toPrint);
	}
	else if (debLvl == 2 && DEBUG_LEVEL_2)
	{
		char *tk;
		tk = strtok(toPrint, "\n");
		while (tk != NULL)
		{
			print_deb("--L2: %s %s \n", msg_type, tk);
			tk = strtok(NULL, "\n");
		}
	}
}

void _DEBUG_BYTE_(const char *msg_type, char *toPrint, int debLvl) // da sistemare in base a come viene richiamata
{
	char buff[5];
	for (short i = 0; i < strlen(toPrint); i++)
	{
		sprintf(buff, "0x%02X", toPrint[i]);
		print_deb("--\t\tL0: %s %s \n", msg_type, buff);
	}
}

unsigned long millis_c()
{

	return millis();
}

#endif
