/*
 * saveToFile.h
 *
 *  Created on: 16 mar 2019
 *      Author: Luca
 */

#ifndef FILEHANDLERMCU_H_
#define FILEHANDLERMCU_H_

int writeFile(unsigned char value);

void readFile();

void deleteFile();

void formatFile();


#endif /* FILEHANDLERMCU_H_ */
