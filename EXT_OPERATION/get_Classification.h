/*
 * get_Classification.h
 *
 */

#ifndef EXT_OPERATION_GET_CLASSIFICATION_H_
#define EXT_OPERATION_GET_CLASSIFICATION_H_

#include "../TinyML/classificators/MLP/classifier_config.h"
#include "../EXT_OPERATION/set_Sensor.h"
#include "../API/level2/Items/SPParamItemMeasures.h"


#define CLASSIFIER_AIR_ID 0x00
extern bool classificationInitialState;

void get_Classification(const uint8_t *id);

#endif /* EXT_OPERATION_GET_CLASSIFICATION_H_ */
