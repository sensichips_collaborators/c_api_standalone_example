/*
 * set_sensingElement.h
 *
 *  Created on: 03 ago 2018
 *      Author: Luca
 */

#ifndef SET_SPMEASURAMENTPARAMETEREIS_H_
#define SET_SPMEASURAMENTPARAMETEREIS_H_

#include "../API/util/types.h"
#include "API/config/init.h"

#define PORT_NUMBER 32 /* Max number of ports supported for now with 5bit representation */

extern void set_SPMeasurementParameterEIS(uint8_t *parameters);


#endif /* SET_SPMEASURAMENTPARAMETEREIS_H_ */
