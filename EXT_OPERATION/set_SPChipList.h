/*
 * set_SPChipList.h
 *
 *  Created on: 05 ott 2018
 *      Author: Luca
 */

#ifndef SET_SPCHIPLIST_H_
#define SET_SPCHIPLIST_H_

#include "../API/util/types.h"
#include "../API/config/init.h"

void set_SPChipList(uint8_t *chipList, int size);

#endif /* SET_SPCHIPLIST_H_ */
