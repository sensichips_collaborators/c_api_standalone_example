/*
 * set_sensingElement.cpp
 *
 *  Created on: 03 ago 2018
 *      Author: Luca
 */

#include "../EXT_OPERATION/set_SPMeasurementParameterEIS.h"


//#include "Arduino.h"

#define PHASESHIFT_MIN    0
#define PHASESHIFT_MAX    31


static void set_Rsense(uint8_t rSense);

static void set_InGain(uint8_t inGain);

static void set_Harmonic(uint8_t harmonic);

static void set_ModeVI(uint8_t modeVI);

static void set_OutGain(uint8_t outGain);

static void set_InPort(uint8_t inPort);

static void set_QI(uint8_t qi);

static void set_Contacts(uint8_t contacts);

static void set_OutPort(uint8_t outPort);

static void set_DCBiasN(uint8_t dcBiasN);

static void set_PhaseShiftMode(uint8_t phaseShiftMode);

static void set_Measure(uint8_t measure);

static void set_PhaseShift(uint8_t phaseShift);

static void set_Filter(uint8_t filter);

static void set_DCBiasP(uint8_t *dcBiasP);

static void set_Frequency(uint8_t *frequency);

static void get_RenewBuffer(uint8_t renewBuffer);

static void set_InstanceID(uint8_t ID);


void set_SPMeasurementParameterEIS(uint8_t *parameters) {

  //leggo il 1 byte
  set_Rsense(parameters[0]);
  set_InGain(parameters[0]);
  set_Harmonic(parameters[0]);
  set_ModeVI(parameters[0]);

  //leggo il 2 byte
  set_OutGain(parameters[1]);
  set_InPort(parameters[1]);

  //leggo il 3 byte
  set_QI(parameters[2]);
  set_Contacts(parameters[2]);
  set_OutPort(parameters[2]);

  //leggo il 4 byte
  set_DCBiasN(parameters[3]);

  //leggo il 5 byte
  set_Measure(parameters[4]);
  set_Filter(parameters[4]);
  get_RenewBuffer(parameters[4]);

  //leggo il 6 byte
  set_PhaseShiftMode(parameters[5]);
  set_PhaseShift(parameters[5]);

  //leggo il 7-8 byte
  set_DCBiasP(&parameters[6]);

  //leggo il 9-10-11-12 byte
  set_Frequency(&parameters[8]);

  //leggo 13 byte
  set_InstanceID(parameters[12]);

}

void set_Rsense(uint8_t rSense) {
  uint8_t RSENSE_MASK = 0xC0;
  int value = (rSense & RSENSE_MASK) >> 6;

  if (value >= 0 && value <= 3) {
    ///spPara
    paramADC.rSense.value = paramADC.rSense.items[value];
  }

//	if(value >= 0 && value <= 3){
//		//setRSense(spParamEIS.QIparam, rSenseLabel[value]);
//		strcpy(spParamEIS.QIparam->rsense, RSenseValues[value]);
//		spParamADC.spParamItemRSense.setRsense(&spParamADC.spParamItemRSense, RSenseLabels[value]);
//	} else {
//		//Errore
//	}
}

void
set_InGain(uint8_t inGain) {   // c'� un problema qua??? sembra che il vettore ingainValuesRUN5 venga letto al contrario
  uint8_t INGAIN_MASK = 0x30;
  int value = (inGain & INGAIN_MASK) >> 4;
  if (value >= 0 && value <= 3) {
    paramADC.inGain.value = paramADC.inGain.items[value];
  }

//
//	if(value >= 0 && value <= 3){
//		//setInGain(spParamEIS.QIparam, ingainLabels[value]);
//		strcpy(spParamEIS.QIparam->ingain, ingainValuesRUN5[value]);
//		spParamADC.spParamItemInGain.setInGain(&spParamADC.spParamItemInGain, ingainLabelsRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_Harmonic(uint8_t harmonic) {
  uint8_t HARMONIC_MASK = 0x0C;
  int value = (harmonic & HARMONIC_MASK) >> 2;
  if (value >= 0 && value <= 2) {
    paramQI.harmonic.value = paramQI.harmonic.items[value];
  }

//
//	if(value >= 0 && value <= 2){
//		//setHarmonic(spParamEIS.QIparam, harmonicLabels[value-1]);
//		strcpy(spParamEIS.QIparam->harmonic, harmonicValuesRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_ModeVI(uint8_t modeVI) {
  uint8_t MODEVI_MASK = 0x03;
  int value = (modeVI & MODEVI_MASK);
  if (value >= 0 && value <= 3) {
    paramQI.modeVI.value = paramQI.modeVI.items[value];
  }

//
//	if(value >= 0 && value <= 3){
//		//setModeVI(spParamEIS.QIparam, modeVILabels[value]);
//		strcpy(spParamEIS.QIparam->modeVI, ModeVIValuesRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_OutGain(uint8_t outGain) {
  uint8_t OUTGAIN_MASK = 0xE0;
  int value = (outGain & OUTGAIN_MASK) >> 5;
  if (value >= 0 && value <= 7) {
    paramQI.outGain.value = paramQI.outGain.items[value];
  }
//
//	if(value >= 0 && value <= 7){
//		//setOutGain(spParamEIS.QIparam, outgainLabels[value]);
//		strcpy(spParamEIS.QIparam->outgain, outgainValuesRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_InPort(uint8_t inPort) {
  uint8_t INPORT_MASK = 0x1F;      // 00011111
  int value = (inPort & INPORT_MASK);  //restituisce 12 al posto di 11
  if (value >= 0 && value < PORT_NUMBER) {
    paramQI.inport.portIndex = value;
  }
//
//	if(value >= 0 && value < PORT_NUMBER){
////		print_f("\nPORT: %s\n", portLabels[value]);
//		setInPort(spParamEIS.QIparam, portLabels[value]);
//		//stampa debug
//	//	print_f("%s\n", spParamEIS.QIparam->inport->port->portLabel);
//	//	print_f("%s\n", spParamEIS.QIparam->inport->port->isInternal);
//		//strcpy(spParamEIS.QIparam->inport->port->portValue, portValues[value]);
//		//strcpy(spParamEIS.QIparam->inport->port->portLabel, portLabels[value]);
//	} else {
//		//Errore
//	}
}

void set_QI(uint8_t qi) {
  uint8_t QI_MASK = 0xC0;
  int value = (qi & QI_MASK) >> 6;

  if (value >= 0 && value <= 3) {
    paramQI.qi.value = paramQI.qi.items[value];
  }
//
//	if(value >= 0 && value <= 3){
//		strcpy(spParamEIS.QIparam->q_i, I_QValuesRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_Contacts(uint8_t contacts) {
  uint8_t CONTACTS_MASK = 0x20;
  int value = (contacts & CONTACTS_MASK) >> 5;
  if (value >= 0 && value <= 1) {
    paramEIS.contacts.value = paramEIS.contacts.items[value];
  }

//
//	if(value >= 0 && value <= 1){
//		//setContacts(&spParamEIS, contactsLabels[value]);
//		spParamEIS.Contacts.setContacts(&spParamEIS.Contacts, contactsLabels[value]);
//		//strcpy(spParamEIS.contacts, contactsValues[value]);
//	} else {
//		//Errore
//	}
}

void set_OutPort(uint8_t outPort) {
  uint8_t INPORT_MASK = 0x1F;
  int value = (outPort & INPORT_MASK);
  if (value >= 0 && value < PORT_NUMBER) {
    paramQI.outport.portIndex = value;
  }
//
//	if(value >= 0 && value < PORT_NUMBER){
//		setOutPort(spParamEIS.QIparam, portLabels[value]);
//		//stampa debug
//	//	print_f("%s\n", spParamEIS.QIparam->outport->port->portLabel);
//	//	print_f("%s\n", spParamEIS.QIparam->outport->port->isInternal);
//		//strcpy(spParamEIS.QIparam->outport->port->portValue, portValues[value]);
//		//strcpy(spParamEIS.QIparam->outport->port->portLabel, portLabels[value]);
//	} else {
//		//Errore
//	}
}

void set_DCBiasN(uint8_t dcBiasN) {
  short value = dcBiasN;
  value = value - 32;
  paramEIS.dcBiasN.value = value;

  //setDCBiasN(&spParamEIS, value);
}

void set_PhaseShiftMode(uint8_t phaseShiftMode) {
  uint8_t PHASESHIFTMODE_MASK = 0x60;
  int value = (phaseShiftMode & PHASESHIFTMODE_MASK) >> 5;
  if (value >= 0 && value <= 2) {
    paramQI.phaseShiftMode.value = paramQI.phaseShiftMode.items[value];
  }
//
//	if(value >= 0 && value <= 2){
//		spParamEIS.QIparam->phaseShiftMode = PhaseShiftModeValuesRUN5[value];
//	}
}

void set_Measure(uint8_t measure) {
  uint8_t MEASURE_MASK = 0x78;
  int value = (measure & MEASURE_MASK) >> 3;
  paramEIS.measure.value = paramEIS.measure.items[value];
//	spParamEIS.measure = measureValuesRUN5[value];
//	spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = measureValuesRUN5[value];
}

void get_RenewBuffer(uint8_t renewBuffer) {
  uint8_t RENEW_BUFFER_MASK = 0x80; //10000000
  int value = (renewBuffer & RENEW_BUFFER_MASK) >> 7;
  if (value) {
    for (int i = 0; i < NUM_OF_MEASUREMENT; i++) {
      status[i].firstMeasure = true;
    }
    spCluster.cache.methods->clearCache(&spCluster.cache);
  }

//
////	if(value){
//		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
//			qistatus[i].status.firstMeasure = true;
////		}
//		//reset cache
//		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
//			strcpy(cache[i], "\0");
//
//		}
//	}
}

void set_PhaseShift(uint8_t phaseShift) {
  uint8_t PHASESHIFT_MASK = 0x0F;
  int value = phaseShift & PHASESHIFT_MASK;

  if (value >= paramQI.phaseShift.min && value <= paramQI.phaseShift.max) {
    paramQI.phaseShift.value = value;
  }
//
//	if( value >= PHASESHIFT_MIN && value <= PHASESHIFT_MAX){
//		spParamEIS.QIparam->phaseShift = value;
//	} else {
//		//Errore
//	}
}

void set_Filter(uint8_t filter) {
  uint8_t FILTER_MASK = 0x07;
  int value = filter & FILTER_MASK;
  paramADC.filter.value = paramADC.filter.items[value];
//
//	if(value >= 0 && value <= 7){
//		strcpy(spParamEIS.QIparam->filter, filterValuesRUN5[value]);
//
//	} else {
//		//Errore
//	}
}

void set_DCBiasP(uint8_t *dcBiasP) {
  int value = dcBiasP[0] << 8;
  value = value | dcBiasP[1];
  value = value - 2048;
  paramEIS.dcBiasP.value = (short) value;
//	setDCBiasP(&spParamEIS, value);
}

void set_Frequency(uint8_t *frequency) {
  union freq {
      float value;
      uint8_t appo[4];
  } number;

  number.appo[3] = frequency[0];
  number.appo[2] = frequency[1];
  number.appo[1] = frequency[2];
  number.appo[0] = frequency[3];

  paramQI.frequency = number.value;

//
//	setFrequency(spParamEIS.QIparam, number.value);
}

void set_InstanceID(uint8_t ID) {
  instanceID = ID;
}

