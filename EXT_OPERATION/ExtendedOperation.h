/*
 * ExtendedOperation.h
 *
 *  Created on: 01 mar 2019
 *      Author: Luca
 */

#ifndef EXTENDEDOPERATION_H_
#define EXTENDEDOPERATION_H_

#include "set_SPChipList.h"
#include "set_SPMeasurementParameterADC.h"
#include "set_SPMeasurementParameterEIS.h"
#include "set_SPMeasurementParameterPOT.h"
#include "set_SPMeasurementParameterAUX.h"
#include "set_Sensor.h"
#include "softResetMCU.h"
// EXT_INSTR section
#define EXT_INSTR                    0x80   // 10000000; all extended instruction contains 0x80
#define EXT_INSTR_TEST              0x81   // 10000001
//#define EXT_INSTR_NET          		  0x82   // 10000010
#define EXT_INSTR_NUM_OF_CHIP        0x82   // 10000010
#define EXT_INSTR_DETECT_CHIP      0x83   // 10000011
#define EXT_INSTR_RESET            0x84   // 10000100
#define EXT_INSTR_SENSIBUSSPEED      0x85   // 10000101
#define EXT_INSTR_CHIPLIST          0x87   // 10000111
#define EXT_INSTR_USBSPEED          0x88   // 10001000
#define EXT_INSTR_CLEAR_CACHE        0x89   // 10001001

#define EXT_INSTR_ELAPSED_START_COM  0xA0   // 10100000
#define EXT_INSTR_TEST_SPEED32X_COM  0xC0   // 11000000

#define EXT_INSTR_SETEIS        0x90   // 10010000
#define EXT_INSTR_SETADC        0x91   // 10010001
#define EXT_INSTR_GETEIS        0x92   // 10010010
#define EXT_INSTR_SET_SENSOR      0x93   // 10010011
#define EXT_INSTR_GET_SENSOR      0x94   // 10010100
#define EXT_INSTR_SET_POT        0x95   // 10010101
#define EXT_INSTR_GET_POT        0x96   // 10010110
#define EXT_INSTR_SAVE_FILE        0x97   // 10010111
#define EXT_INSTR_READ_FILE        0x98   // 10011000
#define EXT_INSTR_DELETE_FILE      0x99   // 10011001
#define EXT_INSTR_FORMAT_FILE      0x9A   // 10011010
#define EXT_INSTR_GET_CLASSIFICATION     0x9B   //1001 1011
#define EXT_INSTR_SET_AUX     0x9C   //10011100

#endif /* EXTENDEDOPERATION_H_ */
