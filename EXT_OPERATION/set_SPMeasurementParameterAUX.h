//
// Created on 5/8/23.
//

#ifndef SB_EXT_OP_SET_SPMEASUREMENTPARAMETERAUX_H
#define SB_EXT_OP_SET_SPMEASUREMENTPARAMETERAUX_H

#include "API/config/init.h"
#include "./util/util.h"
#include "../API/level2/Parameters/SPMeasurementParameterAUX.h"

#define EXOP_AUX_OUT_MODE_MASK  0b11000000
#define EXOP_AUX_OUT_PORT_MASK  0b00111000
#define EXOP_AUX_IN_PORT_MASK   0b00000111
#define EXOP_AUX_BIAS_MASK

#define EXTOP_AUX_PAYLOAD_SIZE 3

extern void set_SPMeasurementParameterAUX(uint8_t *payload_parameters, SPMeasurementParameterAUX *paramAUX);


#endif //SB_EXT_OP_SET_SPMEASUREMENTPARAMETERAUX_H
