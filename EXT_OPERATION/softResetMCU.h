/*
 * softResetMCU.h
 *
 *  Created on: 08 ott 2018
 *      Author: Luca
 */

#ifndef SOFTRESETMCU_H_
#define SOFTRESETMCU_H_

#include "../API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
#include "get_Classification.h"
//#include "../task.h"

void softResetMCU();


#endif /* SOFTRESETMCU_H_ */
