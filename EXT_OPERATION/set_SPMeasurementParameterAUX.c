//
// Created on 5/8/23.
//
#include "set_SPMeasurementParameterAUX.h"

void set_SPMeasurementParameterAUX(uint8_t *payload_parameters, SPMeasurementParameterAUX *paramAUX) {

  paramAUX->outMode.value =
          extractField(payload_parameters[0], EXOP_AUX_OUT_MODE_MASK);

  paramAUX->inPort.portValue =
          extractField(payload_parameters[0], EXOP_AUX_IN_PORT_MASK);

  paramAUX->outPort.portValue =
          extractField(payload_parameters[0], EXOP_AUX_OUT_PORT_MASK);

  /* Extract Bias */
  int value = payload_parameters[1] << 8;
  value = value | payload_parameters[2];
  value = value - 2048;
  paramAUX->bias.value = (short) value;

  /* isntanceID */
  instanceID = payload_parameters[3];
}