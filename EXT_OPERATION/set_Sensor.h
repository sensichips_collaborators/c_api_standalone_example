/*
 * set_Sensor.h
 *
 *  Created on: 02 dic 2018
 *      Author: Luca
 */

#ifndef SET_SENSOR_H_
#define SET_SENSOR_H_

#include "../API/level2/Parameters/SPMeasurementParameterSENSOR.h"
#include "../API/level2/Parameters/SPMeasurementParameterSENSOR.h"
#include "../API/level1/chip/SPSensingElement.h"
#include "../API/level1/chip/SPSensingElementOnChip.h"
#include "../API/config/init.h"

void set_Sensor(uint8_t *param);


#endif /* SET_SENSOR_H_ */
