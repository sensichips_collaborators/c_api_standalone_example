/*
 * set_SPMeasuramentParameterPOT.h
 *
 *  Created on: 01 mar 2019
 *      Author: Luca
 */

#ifndef SET_SPMEASURAMENTPARAMETERPOT_H_
#define SET_SPMEASURAMENTPARAMETERPOT_H_


#include "../API/util/types.h"
#include "../API/level2/Parameters/SPMeasurementParameterPOT.h"

extern void set_SPMeasuramentParamenterPOT(uint8_t *parameters);


#endif /* SET_SPMEASURAMENTPARAMETERPOT_H_ */
