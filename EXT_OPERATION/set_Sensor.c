/*
 * set_Sensor.cpp
 *
 *  Created on: 02 dic 2018
 *      Author: Luca
 */

#include "set_Sensor.h"


#define MAX_SENSORS_N 32 /* numero di sensori massimo dovuto all'utilizzo di 5 bit per identificarne la posizione */
#define ACTUAL_SENSOR_N  31

//char sensorNAME[MAX_SENSORS_N][SENSINGELEMENTNAME_MAXLEN] = {
//		"ONCHIP_DARK", "ONCHIP_LIGHT", "ONCHIP_TEMPERATURE", "ONCHIP_VOLTAGE", "OFFCHIP_PLATINUM_78kHz",
//		"OFFCHIP_GOLD_78kHz", "OFFCHIP_PLATINUM_200Hz", "OFFCHIP_GOLD_200Hz", "OFFCHIP_SILVER_200Hz", "OFFCHIP_NICKEL_200Hz",
//		"ONCHIP_ALUMINUM_OXIDE", "ONCHIP_UREA", "ONCHIP_VOC_PORT8", "ONCHIP_VOC_PORT9", "ONCHIP_VOC_PORT10", "ONCHIP_POLYMIDE",
//		"OFFCHIP_HP_DC", "OFFCHIP_HP_AC", "OFFCHIP_AU_GRAPHENE_EXT3", "OFFCHIP_HUMIDITY", "ENVIRONMENTAL_TEMPERATURE",
//		"ENVIRONMENTAL_RELATIVE_HUMIDITY", "CELL_GASSING", "OFFCHIP_ANODE_TEMPERATURE", "OFFCHIP_CATHODE_TEMPERATURE",
//		"OFFCHIP_BODY_TEMPERATURE", "BODY_WETNESS", "BODY_STRAIN","ANODE_TEMPERATURE","CATHODE_TEMPERATURE","BODY_TEMPERATURE"
//};

char sensorNAME[MAX_SENSORS_N][SENSINGELEMENTNAME_MAXLEN] = {
        "ONCHIP_DARK", "ONCHIP_LIGHT", "ONCHIP_TEMPERATURE", "ONCHIP_VOLTAGE", "OFFCHIP_PLATINUM_78kHz",
        "OFFCHIP_GOLD_78kHz", "OFFCHIP_PLATINUM_200Hz", "OFFCHIP_GOLD_200Hz", "OFFCHIP_SILVER_200Hz",
        "OFFCHIP_NICKEL_200Hz",
        "ONCHIP_ALUMINUM_OXIDE", "ONCHIP_ALUMINUM_OXIDE_OUT4", "ONCHIP_ALUMINUM_OXIDE_OUT7",
        "OFFCHIP_SENSIMOX_SnAu",
        "OFFCHIP_SENSIMOX_SO2", "OFFCHIP_TGS8100_FIGARO_T200", "ONCHIP_POLYMIDE", "OFFCHIP_HP_AC",
        "OFFCHIP_AU_GRAPHENE_EXT3",
        "OFFCHIP_HUMIDITY", "ENVIRONMENTAL_TEMPERATURE",
        "ENVIRONMENTAL_RELATIVE_HUMIDITY", "CELL_GASSING", "OFFCHIP_ANODE_TEMPERATURE", "OFFCHIP_CATHODE_TEMPERATURE",
        "OFFCHIP_BODY_TEMPERATURE", "BODY_WETNESS", "BODY_STRAIN", "ANODE_TEMPERATURE", "CATHODE_TEMPERATURE",
        "BODY_TEMPERATURE"
};

static void setPortValue(char *param, char *port);

static void setSensorEIS(SPMeasurementParameterSENSOR *param, SPSensingElement *se, SPSensingElementOnChip *seOnChip);

void set_Sensor1(uint8_t *param, SPMeasurementParameterSENSOR *sensor) {
//	uint8_t FIRST_MEASURE_MASK = 0x01;
//
//	int sensorNameMask = 31; //00011111
//	int sensorCode = param[0] & sensorNameMask;
//	char sensorName[24];
//	strcpy(sensorName, sensorNAME[sensorCode]);
//	strcpy(sensor->sensorName, sensorName);
//
//	instanceID = (param[1] >> 1);
//
//	SPSensingElementOnChip* seOnChip = spConfiguration.searchSPSensingElementOnChip(&spConfiguration, sensorName);
//	SPSensingElement* sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);
//
//	if(strcmp(sensingElement->measureTecnique, "DIRECT") == 0){
//		strcpy(sensor->port, seOnChip->spSensingElementOnFamily->port->portValue);
//		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = -1;
//		strcpy(sensor->filter, sensingElement->filter);
//		sensor->paramInternalEIS = NULL;
//		qistatus[instanceID].status.dimBuffer = atoi(sensor->filter);
//		//		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
//		//			cache[i][0] = '\0';
//		//		}
//
//
//	} else if (strcmp(sensingElement->measureTecnique, "EIS") == 0){
//		sensor->paramInternalEIS = &spParamEIS;
//		setSensorEIS(sensor, sensingElement, seOnChip);
//		int measureIndextToSave = param[0] >> 5;
//		spMeasurementRUNX[instanceID].spMeasurement->measureIndexToSave = measureIndextToSave;
//		strcpy(sensor->filter, sensingElement->filter);
//	}
//
//	if(param[1] & FIRST_MEASURE_MASK){//renewBuffer
//		for(int i = 0; i < NUM_OF_MEASUREMENT; i++){
//			qistatus[instanceID].status.firstMeasure = true;
//		}
//		//reset cache
//		for(int i = 0; i < NUM_OF_INSTRUCTIONS; i++){
//			strcpy(cache[i], "\0");
//
//		}
//	}

}

void set_Sensor(uint8_t *param) {

  /* Per avere le misure ripetute di diversi sensorEIS sempre consistenti,
   * dato che si riutilizzano sempre le stesse strutture dati per instanceID */
  paramSENSOR.paramInternalEIS = NULL;
  paramSENSOR.spOffChipTemperature = NULL;
  paramSENSOR.spOffChipSENSIMOX = NULL;

  uint8_t FIRST_MEASURE_MASK = 0x01;
  int sensorNameMask = 31; //00011111
  int sensorCode = param[0] & sensorNameMask;  //2 & 31 = 2
  char sensorName[SENSINGELEMENTNAME_MAXLEN];
  strcpy(sensorName, sensorNAME[sensorCode]);
  strcpy(paramSENSOR.sensorName, sensorName);
////	print_f("\nsensorName: %s\n", spParamSENSOR.sensorName);
  instanceID = (param[1] >> 1); //shift di 1 bit a dx

  SPSensingElementOnChip *seOnChip = spConfiguration.searchSensingElementOnChip(&spConfiguration, sensorName);
  SPSensingElement *sensingElement = (seOnChip->spSensingElementOnFamily->spSensingElement);

  paramSENSOR.port.portIndex = seOnChip->spSensingElementOnFamily->port.portIndex;
  spMeasurement[instanceID].measureIndexToSave = -1;
  paramSENSOR.filter.value = sensingElement->filter.value; /* TODO: CHECK COMPATIBILITY*/

  /* Sensori pseudo-diretti TODO: normalizzare quelli water */
  if (strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_ANODE_TEMPERATURE") == 0 ||
      strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_CATHODE_TEMPERATURE") == 0 ||
      strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_BODY_TEMPERATURE") == 0) {

    paramSENSOR.spOffChipTemperature = &spOffChipTemperature;
    paramSENSOR.spOffChipTemperature->paramInternalEIS = &paramEIS;
    initSPOffChipTemperature(&spOffChipTemperature);
    /* Conversion rate from sensing element*/
    paramSENSOR.spOffChipTemperature->paramInternalEIS->QIparam->spMeasurementParameterADC->conversionRate.value =
            seOnChip->spSensingElementOnFamily->spSensingElement->ADCConversionRate.value;
    /* Select ports from sensingElement */
    paramSENSOR.spOffChipTemperature->paramInternalEIS->QIparam->inport.portIndex =
            seOnChip->spSensingElementOnFamily->port.portIndex;
    paramSENSOR.spOffChipTemperature->paramInternalEIS->QIparam->outport.portIndex =
            seOnChip->spSensingElementOnFamily->port.portIndex;

  } else if (strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_SENSIMOX_SnAu") == 0 ||
             strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_SENSIMOX_SnO2") == 0) {
    paramSENSOR.spOffChipSENSIMOX = &spOffChipSENSIMOX;
    spMeasurement[instanceID].measureIndexToSave = MEASURES_IN_PHASE;
    /* frequency */
    union freq {
        float value;
        uint8_t f[4];
    } number;
    number.f[3] = param[2];
    number.f[2] = param[3];
    number.f[1] = param[4];
    number.f[0] = param[5];
    /* RSense */
    uint8_t RSENSE_MASK = 0xC0;
    int rSenseIdx = (param[6] & RSENSE_MASK) >> 6;
    /* Temeprature*/
    short heaterTemp = (param[6] & 0b00111111) << 8;
    heaterTemp = heaterTemp | param[7];


    if (strcmp(seOnChip->spSensingElementOnFamily->id, "OFFCHIP_SENSIMOX_SnAu") == 0) {
      initSPOffChipSENSIMOX_parameters(paramSENSOR.spOffChipSENSIMOX, &paramEIS, &paramAUX,
                                       0, 0, heaterTemp, 0, number.value,
                                       AUX_OUT_PORT_SHA, &spConfiguration);
    } else {
      initSPOffChipSENSIMOX_parameters(paramSENSOR.spOffChipSENSIMOX, &paramEIS, &paramAUX,
                                       0, 0, heaterTemp, 0, number.value,
                                       AUX_OUT_PORT_AUX, &spConfiguration);
    }
    paramSENSOR.spOffChipSENSIMOX->paramEISResOutput->QIparam->spMeasurementParameterADC->rSense.value =
            paramSENSOR.spOffChipSENSIMOX->paramEISResOutput->QIparam->spMeasurementParameterADC->rSense.items[rSenseIdx];


  } else if (sensorCode > 3) { /* EIS Measurements types */
    paramSENSOR.paramInternalEIS = &paramEIS;
    setSensorEIS(&paramSENSOR, sensingElement, seOnChip);
    int measureIndexToSave = param[0] >> 5;
    spMeasurement[instanceID].measureIndexToSave = (int8_t) measureIndexToSave;
    /* Filter already configured */
  }

  if (param[1] & FIRST_MEASURE_MASK) {
    for (int i = 0; i < NUM_OF_MEASUREMENT; i++) {
      status[instanceID].firstMeasure = true;
    }
    spCluster.cache.methods->clearCache(&spCluster.cache);
  }

  paramSENSOR.spMeasurementParameterADC->FIFO_READ.value = paramSENSOR.spMeasurementParameterADC->FIFO_DEEP.value = 1;

}

void setSensorEIS(SPMeasurementParameterSENSOR *param, SPSensingElement *se, SPSensingElementOnChip *seOnChip) {
  //setContacts(param->paramInternalEIS, se->contacts);
  param->paramInternalEIS->contacts.value = se->contacts.value;

  //setRSense(param->paramInternalEIS->QIparam, se->rsense);
  param->paramInternalEIS->QIparam->spMeasurementParameterADC->rSense.value = se->rSense.value;

  //setDCBiasP(param->paramInternalEIS, se->dcBiasP);
  param->paramInternalEIS->dcBiasP.value = se->dcBiasP.value;

  //setDCBiasN(param->paramInternalEIS, se->dcBiasN);
  param->paramInternalEIS->dcBiasN.value = se->dcBiasN.value;

  //setFrequency(param->paramInternalEIS->QIparam, se->frequency);
  param->paramInternalEIS->QIparam->frequency = se->frequency;

  //setHarmonic(param->paramInternalEIS->QIparam, se->harmonic);
  param->paramInternalEIS->QIparam->harmonic.value = se->harmonic.value;

  //setOutPort(param->paramInternalEIS->QIparam, seOnChip->spSensingElementOnFamily->port->portLabel);//port11 ==> ONCHIP_POLYMIDE
  param->paramInternalEIS->QIparam->outport.portIndex = seOnChip->spSensingElementOnFamily->port.portIndex;

  //setInPort(param->paramInternalEIS->QIparam, seOnChip->spSensingElementOnFamily->port->portLabel);
  param->paramInternalEIS->QIparam->inport.portIndex = seOnChip->spSensingElementOnFamily->port.portIndex;

  //setInGain(param->paramInternalEIS->QIparam, se->ingain);
  param->paramInternalEIS->QIparam->spMeasurementParameterADC->inGain.value = se->inGain.value;

  //setOutGain(param->paramInternalEIS->QIparam, se->outgain);
  param->paramInternalEIS->QIparam->outGain.value = se->outGain.value;

  //setModeVI(param->paramInternalEIS->QIparam, se->modeVI);
  param->paramInternalEIS->QIparam->modeVI.value = se->modeVI.value;

  //setMeasure(param->paramInternalEIS, se->measureType);
  param->paramInternalEIS->measure.value = se->measure.value;

  //setPhaseShiftQuadrants(param->paramInternalEIS->QIparam, se->phaseShiftMode, se->phaseShift, se->iq);
  param->paramInternalEIS->QIparam->phaseShiftMode.value = se->phaseShiftMode.value;
  param->paramInternalEIS->QIparam->phaseShift.value = se->phaseShift.value;
  param->paramInternalEIS->QIparam->qi.value = se->qi.value;

  //setFilter(param->paramInternalEIS->QIparam, se->filter);
  param->paramInternalEIS->QIparam->spMeasurementParameterADC->filter.value = se->filter.value; /* TODO: CHECK compatibility*/
}

void setPortValue(char *param, char *port) {

//	sp_bool portFound = false;
//
//	if(strcmp(port, NOPORT) != 0){
//		int i=0;
//		while(i<PORT_NUMBER && !portFound){
//			if(strcmp(portLabels[i], port) != 0){
//				i++;
//			}else{
//				portFound = true;
//			}
//		}
//		if(portFound){
//			strcpy(param, portValues[i]);
//		}
//	}

  return;
}
