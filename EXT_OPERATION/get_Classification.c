/*
 * get_Classifier.c
 *
 */

#include "get_Classification.h"

bool classificationInitialState = true;

void get_Classification(const uint8_t *id) {

  //TODO: update new API
  if (classificationInitialState == true) {
    // init Classifier
    initSPDataProcessing(&dataProcessing, C_N, C_M, RIENTRO, &lastClassification, &predicted);
    initSPClassificatorMLP(&spClassificatorMLP,
                           EWA_COEFFICIENT,
                           EWA_START_COEFFICIENT,
                           EWANormalizer,
                           wrongMediumName,
                           classNames,
                           relaiabilityIndex,
                           vect_dist,
                           CLASS_VAL_SIZE,
                           &dataProcessing,
                           FEATURE_SIZE,
                           BACKGROUND_INDEX,
                           DISTANCE_THRESHOLD,
                           WRONG_MEDIUM_THRESHOLD);

    classificationInitialState = false;
    globalIndex = 0;
  }

  short int featureToClassify[FEATURE_SIZE] = {0};
  unsigned int featureIndex = 0;

  if (*id == CLASSIFIER_AIR_ID) {

/*
    for (int i = 0; i < BATCH_SIZE; ++i) {
      set_Sensor(&setSensorPayloads[i][2]);
      spMeasurement[instanceID].methods->setSENSOR(&spMeasurement[instanceID], &paramSENSOR);
      spMeasurement[instanceID].methods->getSENSOR(&spMeasurement[instanceID], extOpOutput, &paramSENSOR);
      //TODO:  chip mulltipli?
      for (int j = 0; j < spCluster.dimChipList; ++j) {
        if (i < 2) {
          featureToClassify[featureIndex] = (short int) extOpOutput[j][MEASURES_IN_PHASE];
        } else {
          featureToClassify[featureIndex] = (short int) extOpOutput[j][MEASURES_IN_PHASE];
          featureIndex++;
          featureToClassify[featureIndex] = (short int) extOpOutput[j][MEASURES_QUADRATURE];
        }
        featureIndex++;
      }

    }
*/

    /* SENSIMOX SECTION */
    paramSENSOR.paramInternalEIS = NULL;
    strcpy(paramSENSOR.sensorName, "OFFCHIP_SENSIMOX_SnAu");
    paramSENSOR.spOffChipSENSIMOX = &spOffChipSENSIMOX;
    initSPMeasurementParameterAUX(&paramAUX);
    initSPOffChipSENSIMOX_parameters(paramSENSOR.spOffChipSENSIMOX, &paramEIS, &paramAUX,
                                     0, 0, 150, 0, 78125.0f,
                                     AUX_OUT_PORT_SHA, &spConfiguration);

    paramSENSOR.spOffChipSENSIMOX->paramEISResOutput->QIparam->spMeasurementParameterADC->rSense.value =
            paramSENSOR.spOffChipSENSIMOX->paramEISResOutput->QIparam->spMeasurementParameterADC->rSense.items[RSENSE_5000];

    float frequencies[2] = {200.0f, 312000.0f};
    float temperatures[6] = {150.0f, 200.0f, 250.0f, 300.0f, 350.0f, 400.0f};
    for (int f = 0; f < 2; ++f) {
      paramSENSOR.spOffChipSENSIMOX->paramEISResOutput->QIparam->frequency = frequencies[f];
      spMeasurement[0].measureIndexToSave = MEASURES_IN_PHASE;
      for (int t = 0; t < 6; ++t) {
        initSPOffChipSENSIMOX_parameters(paramSENSOR.spOffChipSENSIMOX, &paramEIS, &paramAUX,
                                         0, 0, temperatures[t], 0, frequencies[f],
                                         AUX_OUT_PORT_SHA, &spConfiguration);
        spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
        spMeasurement[0].methods->getSENSOR(&spMeasurement[0], extOpOutput, &paramSENSOR);
        featureToClassify[featureIndex] = (short int) extOpOutput[0][MEASURES_IN_PHASE];
        featureIndex++;
      }
    }

    /* OXIDE SECTION */
    paramEIS.contacts.value = paramEIS.contacts.items[CONTACTS_TWO];
    paramEIS.measure.value = paramEIS.measure.items[MEASURES_CAPACITANCE];
    paramEIS.QIparam->spMeasurementParameterADC->filter.value = paramEIS.QIparam->spMeasurementParameterADC->filter.items[FILTER_1];
    paramEIS.dcBiasN.value = 0;
    paramEIS.dcBiasP.value = 0;
    paramEIS.QIparam->harmonic.value = paramEIS.QIparam->harmonic.items[FIRST_HARMONIC];
    paramEIS.QIparam->modeVI.value = paramEIS.QIparam->modeVI.items[VOUT_IIN];
    paramEIS.QIparam->frequency = 78125.0;
    paramEIS.QIparam->qi.value = paramEIS.QIparam->qi.items[QI_IN_PHASE];
    paramEIS.QIparam->inport.portIndex = PORT5;
    paramEIS.QIparam->outport.portIndex = PORT5;
    paramEIS.QIparam->phaseShift.value = 0;
    paramEIS.QIparam->phaseShiftMode.value = paramEIS.QIparam->phaseShiftMode.items[PHASESHIFTMODE_QUADRANTS];
    paramEIS.QIparam->spMeasurementParameterADC->inGain.value = paramEIS.QIparam->spMeasurementParameterADC->inGain.items[INGAIN_50];
    paramEIS.QIparam->spMeasurementParameterADC->mux._DF = 0;
    paramEIS.QIparam->spMeasurementParameterADC->mux._VF = 1;
    paramEIS.QIparam->spMeasurementParameterADC->mux._DS = 2;
    paramEIS.QIparam->spMeasurementParameterADC->mux._VS = 3;
    paramEIS.QIparam->spMeasurementParameterADC->conversionRate.value = 500.0f;
    paramEIS.QIparam->spMeasurementParameterADC->rSense.value = paramEIS.QIparam->spMeasurementParameterADC->rSense.items[RSENSE_5000];

    paramSENSOR.paramInternalEIS = &paramEIS;
    paramSENSOR.spOffChipSENSIMOX = NULL;
    strcpy(paramSENSOR.sensorName, "ONCHIP_ALUMINUM_OXIDE_OUT4");
    paramEIS.QIparam->outGain.value = paramEIS.QIparam->outGain.items[OUTGAIN_4];
    paramSENSOR.filter.value = paramSENSOR.filter.items[FILTER_1];

    paramSENSOR.spMeasurementParameterADC->conversionRate.value = 500.0f;
    paramSENSOR.port.portIndex = PORT5;
    spMeasurement[0].measureIndexToSave = MEASURES_CAPACITANCE;
    spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
    spMeasurement[0].methods->getSENSOR(&spMeasurement[0], extOpOutput, &paramSENSOR);
    featureToClassify[featureIndex] = (short int) extOpOutput[0][MEASURES_IN_PHASE];
    featureIndex++;
    featureToClassify[featureIndex] = (short int) extOpOutput[0][MEASURES_QUADRATURE];
    featureIndex++;

    strcpy(paramSENSOR.sensorName, "ONCHIP_ALUMINUM_OXIDE_OUT7");
    paramEIS.QIparam->outGain.value = paramEIS.QIparam->outGain.items[OUTGAIN_7];
    paramSENSOR.filter.value = paramSENSOR.filter.items[FILTER_1];
    paramSENSOR.spMeasurementParameterADC->conversionRate.value = 500.0f;
    paramSENSOR.port.portIndex = PORT5;
    spMeasurement[0].measureIndexToSave = MEASURES_CAPACITANCE;
    spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
    spMeasurement[0].methods->getSENSOR(&spMeasurement[0], extOpOutput, &paramSENSOR);
    featureToClassify[featureIndex] = (short int) extOpOutput[0][MEASURES_IN_PHASE];
    featureIndex++;
    featureToClassify[featureIndex] = (short int) extOpOutput[0][MEASURES_QUADRATURE];

    /* DUMMY DATASET */
    for (int i = 0; i < FEATURE_SIZE; ++i) {
      featureToClassify[i] = dummyDataset[globalIndex][i];
    }
    globalIndex++;



    //	TODO: normalizzare i tipi delle features in base al classificatore che si vuole implementare
    spClassificatorMLP.classify(&spClassificatorMLP, featureToClassify, classificationOutput);

  }
}
