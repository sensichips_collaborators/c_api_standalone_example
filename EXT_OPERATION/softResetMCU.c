/*
 * softResetMCU.cpp
 *
 *  Created on: 08 ott 2018
 *      Author: Luca
 */


#include "../EXT_OPERATION/softResetMCU.h"

extern SPCluster dfltCluster;

void softResetMCU() {
  instanceID = 0;
  setAddressingType(&spProtocol, FULL_ADDRESS);
  for (int i = 0; i < NUM_OF_MEASUREMENT; i++) {
    spMeasurement[i].status->firstMeasure = 0;
    spMeasurement[i].status->counter = 0;
    spMeasurement[i].measureIndexToSave = -1;
  }

  spCluster.cache.methods->clearCache(&spCluster.cache);
  dfltCluster.cache.methods->clearCache(&dfltCluster.cache);

  classificationInitialState = true;

  setINIT(&spConfiguration);
}



