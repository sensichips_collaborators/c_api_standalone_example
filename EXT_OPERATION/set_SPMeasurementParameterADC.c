/*
 * set_SPMeasuramentParameterADC.cpp
 *
 *  Created on: 05 ago 2018
 *      Author: Luca
 */
#include "../EXT_OPERATION/set_SPMeasurementParameterADC.h"


static void set_spParamItemFIFO_DEEP(uint8_t spParamItemFIFO_DEEP);

static void set_InPortADC(uint8_t inPort);

static void set_spParamItemFIFO_READ(uint8_t spParamItemFIFO_READ);

static void set_spParamItemInGain(uint8_t spParamItemInGain);

void set_SPMeasurementParameterADC(uint8_t *parameter) {

//	//leggo 1 byte
  set_InPortADC(parameter[0]);
  set_spParamItemFIFO_DEEP(parameter[0]);

//	//leggo 2 byte
  set_spParamItemFIFO_READ(parameter[1]);
  set_spParamItemInGain(parameter[1]);
//
//	//print_f("set_SPMeasuramentParameterADC");
//
/* TODO: AGGIORNARE SELEZIONE CONVERION RATE */
  if (paramQI.frequency < 50.0f && paramQI.frequency != 0) {
    paramADC.conversionRate.value = paramQI.frequency;
  } else {
    paramADC.conversionRate.value = 50.0f;
  }

//	if(spParamEIS.QIparam->frequency < 50.0 && spParamEIS.QIparam->frequency != 0){
//		setConversionRate(&spParamADC,spParamEIS.QIparam->frequency);
//	}else{
//		sp_double convRate = 50.0;
//		setConversionRate(&spParamADC,convRate);
//	}

}

void set_spParamItemFIFO_DEEP(uint8_t spParamItemFIFO_DEEP) {
  uint8_t FIFO_DEEP_MASK = 0x0F; //00001111
  int value = spParamItemFIFO_DEEP & FIFO_DEEP_MASK;
  paramADC.FIFO_DEEP.value = (short) value;
//
//	setFIFO_DEEP(&spParamADC, value);
}

void set_InPortADC(uint8_t inPort) {
  uint8_t INPORT_MASK = 0x30; //00110000
  int value = (inPort & INPORT_MASK) >> 4;
  if (value >= 0 && value < PORTADC_SIZE) {
    paramADC.inPort.value = paramADC.inPort.items[value];
  }
//
//	if(value >= 0 && value < INPORTADC_NUMBER){
//		setInPortADC(&spParamADC, inPortADCLabelsRUN5[value]);
//	} else {
//		//Errore
//	}
}

void set_spParamItemFIFO_READ(uint8_t spParamItemFIFO_READ) {
  uint8_t FIFO_READ_MASK = 0x0F; //00001111
  int value = spParamItemFIFO_READ & FIFO_READ_MASK;
  paramADC.FIFO_READ.value = (short) value;
//
//	setFIFO_READ(&spParamADC, value);
}

void set_spParamItemInGain(uint8_t spParamItemInGain) { /* TODO: Check*/
  uint8_t IN_GAIN_MASK = 0xF0; //11110000
  int value = (spParamItemInGain & IN_GAIN_MASK) >> 4;
  paramADC.inGain.value = paramADC.inGain.items[value];

//
//	char inGainLabels[20];
//	strcpy(inGainLabels, ingainLabelsRUN5[value]);
//	spParamADC.spParamItemInGain.setInGain(&spParamADC.spParamItemInGain, ingainLabelsRUN5[value]);

  //setInGainItem(&spParamADC.spParamItemInGain, inGainLabels);
}
