/*
 * set_SPMeasuramentParameterADC.h
 *
 *  Created on: 05 ago 2018
 *      Author: Luca
 */

#ifndef SET_SPMEASURAMENTPARAMETERADC_H_
#define SET_SPMEASURAMENTPARAMETERADC_H_

#include "../API/util/types.h"
#include "../API/config/init.h"

extern void set_SPMeasurementParameterADC(uint8_t *parameter);


#endif /* SET_SPMEASURAMENTPARAMETERADC_H_ */
