//
// Created on   5/9/23.
//

#ifndef SB_EXT_OP_UTIL_H
#define SB_EXT_OP_UTIL_H

#include "../../API/util/types.h"

extern byte extractField(byte payload, byte mask);

#endif //SB_EXT_OP_UTIL_H
