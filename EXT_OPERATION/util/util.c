//
// Created  on 5/9/23.
//
#include "util.h"

byte extractField(byte payload, byte mask) {
  byte tempMask = mask;
  uint8_t shift = 0;
  for (; shift < 8; shift++) {
    if ((tempMask & 0b00000001) == 0b00000001)
      break;
    tempMask = ((tempMask & 255) >> 1);
  }
  return ((payload & mask) >> shift);
}
