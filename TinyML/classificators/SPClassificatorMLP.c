/*
 * SPClassificatorMLP.c
 */
#include "SPClassificatorMLP.h"

SPClassificatorMLP spClassificatorMLP;

int cmpSPClassification(const void *a, const void *b) {
  const struct SPClassification *aa = a;
  const struct SPClassification *bb = b;
  return bb->confidenceDegree > aa->confidenceDegree ? 1 : -1;
}


float mean(SPClassificatorMLP *this_c, const float *vect_dist, unsigned int size) {
  float sum = 0;
  for (int i = 0; i < size; ++i) {
    sum += vect_dist[i];
  }
  return sum / size;
}


float std(SPClassificatorMLP *this_c, float mean, const float *vect_dist, unsigned int size) {
  float sum = 0;
  for (int i = 0; i < size; ++i) {
    sum += pow(abs(vect_dist[i]) - mean, 2);
  }
  return (float) sqrt(sum / size);
}


void normalize(SPClassificatorMLP *this_c, const short int *features, unsigned int size, float *normalized) {
  for (int i = 0; i < size; ++i) {
    /*TODO: gestire divisione o sottrazione*/
#ifdef DIFFERENZE
    normalized[i] = features[i] - this_c->EWANormalizer[i];
#elif
    normalized[i] = features[i] / this_c->EWANormalizer[i]; /* oppure sottrazione se con interi*/
#endif
  }
}

void updateBaseline(SPClassificatorMLP *this_c, short int *features, unsigned int size) {
  if (this_c->rep == 0) {
    for (int i = 0; i < size; ++i) {
      this_c->EWANormalizer[i] = features[i];
    }
  }
  /* TODO: come gestire la baseline per dataset interi?*/
  for (int i = 0; i < size; ++i) {
    this_c->EWANormalizer[i] = this_c->EWANormalizer[i] * (1 - 1 / this_c->EWAStartCoefficient) +
                               (1 / this_c->EWAStartCoefficient) * features[i];
  }

}


int checkBaselineTreshold(SPClassificatorMLP *this_c, float dist) {
  if (dist < this_c->backgroundThreshold) {
    return 0;
  } else if (dist > this_c->wrongMediumThreshold) {
    return -1;
  }
  return 1;
}


void getOrigin(SPClassificatorMLP *this_c, unsigned int size, float *origin_vect) {
  for (int i = 0; i < size; ++i) {
#ifdef DIFFERENZE
    origin_vect[i] = 0;
#elif
    origin_vect[i] = 1;
#endif
  }
}


float distance(SPClassificatorMLP *this_c, const float *features, unsigned int size) { /* presente in SPClassifier*/
  float origin[FEATURE_SIZE];
  this_c->getOrigin(this_c, size, origin);
  float sum = 0;
  for (int i = 0; i < size; ++i) {
    sum += (features[i] - origin[i]) * (features[i] - origin[i]);
  }
  this_c->distanceValue = sqrt(sum);
  return this_c->distanceValue;
}

void setState(SPClassificatorMLP *this_c, int state) {
  if (state == STATE_BS) {
    this_c->WAIT = false;
    this_c->BASELINE_ACQUISITION = false;
    this_c->BASELINE_TRACKING = false;
    this_c->BASELINE_SUSPENDEND = false;
    this_c->BASELINE_STOPPED = true;
  } else if (state == STATE_BA) {
    this_c->WAIT = false;
    this_c->BASELINE_ACQUISITION = true;
    this_c->BASELINE_TRACKING = false;
    this_c->BASELINE_SUSPENDEND = false;
    this_c->BASELINE_STOPPED = false;
  }
}

uint8_t getState(SPClassificatorMLP *this_c, char *state) {
  if (this_c->WAIT == true) {
    strcpy(state, "WAIT");
    return 0;
  } else if (this_c->BASELINE_ACQUISITION == true) {
    strcpy(state, "BASELINE_ACQUISITION");
    return 1;
  } else if (this_c->BASELINE_TRACKING == true) {
    strcpy(state, "BASELINE_TRACKING");
    return 2;
  } else if (this_c->BASELINE_SUSPENDEND == true) {
    strcpy(state, "BASELINE_SUSPENDEND");
    return 3;
  } else if (this_c->BASELINE_STOPPED == true) {
    strcpy(state, "BASELINE_STOPPED");
    return 4;
  }
  return 255;
}

uint8_t getStateID(SPClassificatorMLP *this_c) {
  if (this_c->WAIT == true) {
    return 0;
  } else if (this_c->BASELINE_ACQUISITION == true) {
    return 1;
  } else if (this_c->BASELINE_TRACKING == true) {
    return 2;
  } else if (this_c->BASELINE_SUSPENDEND == true) {
    return 3;
  } else if (this_c->BASELINE_STOPPED == true) {
    return 4;
  }
  return 255;
}

uint8_t getClassValID(SPClassificatorMLP *this_c, char *classValName) {
  for (int i = 0; i < this_c->classValSize; ++i) {
    if (strcmp(this_c->classVal[i], classValName) == 0)
      return i;
  }
  return 255;
}

void getSubstance(SPClassificatorMLP *this_c, float *normalizedFeatures,
                  SPClassification *output) { /* Chiama il classificatore*/

  if (this_c->checkBaselineTreshold(this_c,
                                    this_c->distance(this_c, normalizedFeatures, this_c->featureSize)) == -1) {

    int outDim = 0;
    initSPClassification(&output[outDim], this_c->wrongMediumName, -1, -1);
    outDim++;
    for (int i = 0; i < this_c->classValSize; ++i) {
      if (i != this_c->backgroundIndex) {
        initSPClassification(&output[outDim], this_c->classVal[i], -1, -1);
        outDim++;
      }
    }
  } else {

    /* TEST VALUES
    normalizedFeatures[0] =   -52.543167;
    normalizedFeatures[1] =   -116.106201;
    normalizedFeatures[2] =   -256.89502;
    normalizedFeatures[3] =   -372.158936;
    normalizedFeatures[4] =   -922.974121;
    normalizedFeatures[5] =   -1950.364258;
    normalizedFeatures[6] =   -52.342682;
    normalizedFeatures[7] =   -125.861389;
    normalizedFeatures[8] =   -289.549072;
    normalizedFeatures[9] =   -619.722656;
    normalizedFeatures[10] = -1190.798828;
    normalizedFeatures[11] = -2052.546875;
    normalizedFeatures[12] = 1.143787;
    normalizedFeatures[13] = -0.642189;
    normalizedFeatures[14] =3.307259;
    normalizedFeatures[15] =-1.999481;
     */

    distributionForInstance(&this_c->nn, normalizedFeatures, confdegree);

    /* TODO: COSTRUZIONE OUTPUT, BISOGNA RISPETTARE LE POSIZIONI
     * 	O RIORDINARLE*/

    for (int i = 0; i < this_c->classValSize; ++i) {
      initSPClassification(&output[i], this_c->classVal[i], (float) (confdegree[i] * 100.0f),
                           this_c->reliabilityIndex[i]);
    }
  }
}

void baseline_classify(SPClassificatorMLP *this_c, short int *features,
                       SPClassification *output) {/*manca valore di ritorno*/
  // TODO: GESTIRE OUTPUT
  unsigned int outDim = 0;
  //float normalized[FEATURE_SIZE]; /* [this_c->featureSize] Allocazione dinamica su stack, "staticizzare"*/


  if (!this_c->BASELINE_STOPPED) {
    if (this_c->BASELINE_ACQUISITION || this_c->WAIT) {
      //	TRACKING gestione output
      initSPClassification(&tempOutput[outDim], "TRACKING", -1, -1);
      outDim++;

      for (int i = 0; i < this_c->classValSize; i++) {
        if (i != this_c->backgroundIndex) {
          initSPClassification(&tempOutput[outDim], this_c->classVal[i], -1, -1);
          outDim++;
        }
        /*Initialize last element*/
        initSPClassification(&tempOutput[outDim], "", -1, -1);
      }
    } else {
      // WATER SWW
      initSPClassification(&tempOutput[outDim], this_c->classVal[this_c->backgroundIndex],
                           100.0f, this_c->reliabilityIndex[this_c->backgroundIndex]);
      outDim++;

      for (int i = 0; i < this_c->classValSize; ++i) {
        if (i != this_c->backgroundIndex) {
          initSPClassification(&tempOutput[outDim], this_c->classVal[i], 0.0f, this_c->reliabilityIndex[i]);
          outDim++;
        }
      }
    }
  }

  if (this_c->WAIT) {
    this_c->updateBaseline(this_c, features, this_c->featureSize);
    this_c->normalize(this_c, features, this_c->featureSize, normalized);
    this_c->vect_dist[this_c->rep - ((this_c->rep / this_c->classValSize) * this_c->classValSize)] = distance(this_c,
                                                                                                              normalized,
                                                                                                              this_c->featureSize);

    if (this_c->rep >= this_c->EWACoefficient) {
      this_c->WAIT = false;
      this_c->BASELINE_ACQUISITION = true;
    }

  } else if (this_c->BASELINE_ACQUISITION) {
    float mean = this_c->mean(this_c, this_c->vect_dist, this_c->classValSize);
    this_c->std_dist = mean + (this_c->std(this_c, mean, this_c->vect_dist, this_c->classValSize) * 3);
    this_c->updateBaseline(this_c, features, this_c->featureSize);
    this_c->normalize(this_c, features, this_c->featureSize, normalized);
    this_c->vect_dist[this_c->rep - ((this_c->rep / this_c->classValSize) * this_c->classValSize)] = distance(this_c,
                                                                                                              normalized,
                                                                                                              this_c->featureSize);

    /* Bypass baseline tracking logic...
    if (this_c->checkBaselineTreshold(this_c, this_c->std_dist) == 0) {
      this_c->BASELINE_ACQUISITION = false;
      this_c->BASELINE_TRACKING = true;
    }
    */
    this_c->BASELINE_ACQUISITION = false;
    this_c->BASELINE_TRACKING = true;

  } else if (this_c->BASELINE_TRACKING) {
    this_c->normalize(this_c, features, this_c->featureSize, normalized);
    this_c->vect_dist[this_c->rep - ((this_c->rep / this_c->classValSize) * this_c->classValSize)] = distance(this_c,
                                                                                                              normalized,
                                                                                                              this_c->featureSize);

    if (this_c->checkBaselineTreshold(this_c,
                                      this_c->distance(this_c, normalized, this_c->featureSize)) == 0) {

      this_c->updateBaseline(this_c, features, this_c->featureSize);
      /* Bypass baseline tracking logic... */
      this_c->BASELINE_TRACKING = false;
      this_c->BASELINE_SUSPENDEND = true;
      this_c->sigma_count = 0;
   } else {
     this_c->BASELINE_TRACKING = false;
     this_c->BASELINE_SUSPENDEND = true;
     this_c->sigma_count = 0;
   }

 } else if (this_c->BASELINE_SUSPENDEND) {
   this_c->normalize(this_c, features, this_c->featureSize, normalized);
   this_c->vect_dist[this_c->rep - ((this_c->rep / this_c->classValSize) * this_c->classValSize)] = distance(this_c,
                                                                                                             normalized,
                                                                                                             this_c->featureSize);

   if (this_c->checkBaselineTreshold(this_c,
                                     this_c->distance(this_c, normalized, this_c->featureSize)) != 0) {
     this_c->sigma_count++;
   } else {
     this_c->BASELINE_SUSPENDEND = false;
     this_c->BASELINE_TRACKING = true;
     this_c->sigma_count = 0;
   }

   if (this_c->sigma_count == 5) {
     this_c->BASELINE_SUSPENDEND = false;
     this_c->BASELINE_STOPPED = true;
   }

 } else if (this_c->BASELINE_STOPPED) {
   this_c->normalize(this_c, features, this_c->featureSize, normalized);
   this_c->vect_dist[this_c->rep - ((this_c->rep / this_c->classValSize) * this_c->classValSize)] = distance(this_c,
                                                                                                             normalized,
                                                                                                             this_c->featureSize);

   /* TODO:: da vedere come gestire l'anomaly*/
    /* Chiama il classificatore*/
    this_c->getSubstance(this_c, normalized, tempOutput);
    this_c->anly_count = 0;

  }

  this_c->rep++;

  //TODO: controllare ordinamento output per confidence degree
  if (strcmp(tempOutput[0].className, "TRACKING") != 0)
    qsort(tempOutput, CLASS_VAL_SIZE + 1, sizeof(struct SPClassification), cmpSPClassification);

  if (this_c->BASELINE_STOPPED && strcmp(tempOutput[0].className, this_c->classVal[this_c->backgroundIndex]) ==
                                  0 /* and background*/) { /* gestire le class_names*/
    this_c->sigma_count++;
    if (this_c->sigma_count - 5 == this_c->EWACoefficient * 10) {
      this_c->BASELINE_STOPPED = false;
      this_c->BASELINE_ACQUISITION = true;
    }


  } else if (this_c->BASELINE_STOPPED &&
             strcmp(tempOutput[0].className, this_c->classVal[this_c->backgroundIndex]) != 0 /* and no background*/) {
    this_c->sigma_count = 0;
  }


  if (this_c->BASELINE_STOPPED) {
    tempOutput[0].concentration = -1.0f; /* TODO: Concentration ?? */
  }

  //TODO: DATAPROCESSING
  this_c->dataProcessing->process(this_c->dataProcessing, tempOutput, normalized, output, (void *) this_c);

}


/*TODO: aggiungere parametri necessari -> features -> dimensioni*/
/*TODO: VALUTARE Se realizzare una struttura che contenga tutti i parametri del config*/
void initSPClassificatorMLP(SPClassificatorMLP *this_c,
                            float EWACoefficient,
                            float EWAStartCoefficient,
                            float *EWANormalizer,
                            char *wrongMediumName,
                            char classVal[][20],
                            float *reliabilityIndex,
                            float *vect_dist,
                            unsigned int classValSize,
                            SPDataProcessing *dataProcessing,
                            unsigned int featureSize,
                            unsigned int backgroundIndex,
                            float backgroundThreshold,  /* distance*/
                            float wrongMediumThreshold) {

  this_c->dataProcessing = dataProcessing;
  this_c->backgroundIndex = backgroundIndex;
  this_c->backgroundThreshold = backgroundThreshold; /* distance threshold*/
  this_c->wrongMediumThreshold = wrongMediumThreshold;

  /* EWA coefficients*/
  this_c->EWAStartCoefficient = EWAStartCoefficient;
  this_c->EWACoefficient = EWACoefficient;
  this_c->wrongMediumName = wrongMediumName;

  /* Arrays*/
  this_c->EWANormalizer = EWANormalizer;
  this_c->classVal = classVal;
  this_c->reliabilityIndex = reliabilityIndex;
  this_c->vect_dist = vect_dist;

  /*Sizes */
  this_c->classValSize = classValSize;
  this_c->featureSize = featureSize;

  /* Baseline initial status*/
  this_c->WAIT = true;
  this_c->BASELINE_ACQUISITION = false;
  this_c->BASELINE_TRACKING = false;
  this_c->BASELINE_SUSPENDEND = false;
  this_c->BASELINE_STOPPED = false;

  /*Functions*/
  this_c->mean = &mean;
  this_c->std = &std;
  this_c->normalize = &normalize;
  this_c->updateBaseline = &updateBaseline;
  this_c->getOrigin = &getOrigin;
  this_c->distance = &distance;
  this_c->getState = &getState;
  this_c->setState = &setState;
  this_c->getStateID = &getStateID;
  this_c->getClassValID = &getClassValID;


  this_c->classify = &baseline_classify;
  this_c->getSubstance = &getSubstance;
  this_c->checkBaselineTreshold = &checkBaselineTreshold;

  /*vanno inizializzate le varie dimensioni*/
  this_c->std_dist = this_c->backgroundThreshold;
  this_c->distanceValue = 0;
  this_c->rep = 0;
  this_c->sigma_count = 0;
  this_c->anly_count = 0;

  /* Inizializzazione rete neurale */
  init_neural_network(&this_c->nn);
}
