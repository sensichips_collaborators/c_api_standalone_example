/*
 * SPClassificatorMLP.h
 */

#ifndef TINYML_SPCLASSIFICATORMLP_H_
#define TINYML_SPCLASSIFICATORMLP_H_


#define STATE_BS 0
#define STATE_BA 1

#include "../SPClassification.h"
#include "../DataProcessing/SPDataProcessing.h"
#include "math.h"
#include "../../API/util/types.h"
#include "../classificators/MLP/nn.h"
#include "MLP/classifier_config.h"

//#include "KNN/classifier_config.h"
//#include "KNN/knn.h"
//#include "KNN/matrix.h"

typedef struct SPClassificatorMLP SPClassificatorMLP;

struct SPClassificatorMLP {

    neural_network nn;
    float EWACoefficient;
    float EWAStartCoefficient;
    float *EWANormalizer; /* definire singola size o unico riferimento?(feature size ecc)*/
    float std_dist;
    float distanceValue;
    char *wrongMediumName;
    char (*classVal)[20];
    float *reliabilityIndex; /* stessa dimensione di classValSize*/
    float *vect_dist; /* stessa dimensione di classValSize (da controllare)*/
    unsigned int classValSize;
    SPDataProcessing *dataProcessing;

    unsigned int featureSize;
    int sigma_count;
    int anly_count;
    unsigned int rep;

    unsigned int backgroundIndex;

    float backgroundThreshold;
    float wrongMediumThreshold;

    /* baseline states */
    bool WAIT;
    bool BASELINE_ACQUISITION;
    bool BASELINE_TRACKING;
    bool BASELINE_SUSPENDEND;
    bool BASELINE_STOPPED;


    void (*classify)(SPClassificatorMLP *this_c, short int *features, SPClassification *output);

    void (*getSubstance)(SPClassificatorMLP *this_c, float *normalizedFeatures,
                         SPClassification *output); /* Chiama il classificatore*/
    void (*updateBaseline)(SPClassificatorMLP *this_c, short int *features, unsigned int size);

    int (*checkBaselineTreshold)(SPClassificatorMLP *this_c, float dist);

    float (*std)(SPClassificatorMLP *this_c, float mean, const float *vect_dist, unsigned int size);

    float (*mean)(SPClassificatorMLP *this_c, const float *vect_dist, unsigned int size);

    void (*normalize)(SPClassificatorMLP *this_c, const short int *features, unsigned int size, float *normilized);

    float
    (*distance)(SPClassificatorMLP *this_c, const float *features, unsigned int size); /* presente in SPClassifier*/
    void (*getOrigin)(SPClassificatorMLP *this_c, unsigned int size, float *origin_vect);

    void (*setState)(SPClassificatorMLP *this_c, int state);

    uint8_t (*getState)(SPClassificatorMLP *this_c, char *state); /* DEBUG purpose*/
    uint8_t (*getStateID)(SPClassificatorMLP *this_c);

    uint8_t (*getClassValID)(SPClassificatorMLP *this_c, char *classValName);

};

void initSPClassificatorMLP(SPClassificatorMLP *this_c,
                            float EWACoefficient,
                            float EWAStartCoefficient,
                            float *EWANormalizer,
                            char *wrongMediumName,
                            char classVal[][20],
                            float *reliabilityIndex,
                            float *vect_dist,
                            unsigned int classValSize,
                            SPDataProcessing *dataProcessing,
                            unsigned int featureSize,
                            unsigned int backgroundIndex,
                            float backgroundThreshold,
                            float wrongMediumThreshold);

extern SPClassificatorMLP spClassificatorMLP;

#endif
