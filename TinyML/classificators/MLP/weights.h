#ifndef _WEIGHTS_H_
#define _WEIGHTS_H_

#define INPUT_SIZE  16
#define HIDDEN_SIZE  10
#define OUTPUT_SIZE  4

extern const double weights1[INPUT_SIZE][HIDDEN_SIZE];

extern const double weights2[HIDDEN_SIZE][OUTPUT_SIZE];

extern const double biases1[HIDDEN_SIZE];

extern const double biases2[OUTPUT_SIZE];

#endif
