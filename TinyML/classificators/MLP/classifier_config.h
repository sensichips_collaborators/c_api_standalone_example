/*
 * classifier_config.h
 *
 */

#ifndef TINYML_CLASSIFICATORS_KNN_CLASSIFIER_CONFIG_H_
#define TINYML_CLASSIFICATORS_KNN_CLASSIFIER_CONFIG_H_

//#include "../../SPClassification.h"
// #include "../../DataProcessing/SPDataProcessing.h"
//#include "stdint.h"

#include "../../../API/util/types.h"
//#include "TinyML/SPClassification.h"
//#include "TinyML/DataProcessing/SPDataProcessing.h"
#include "TinyML/classificators/SPClassificatorMLP.h"



/* Classifier config */
#define CLASS_VAL_SIZE 4   /*  */
#define DISTANCE_THRESHOLD  150//0.15
#define EWA_COEFFICIENT 25
#define EWA_START_COEFFICIENT 25
#define MEASURE_TO_DISCARD 0
#define BACKGROUND_INDEX 0
#define WRONG_MEDIUM_NAME "WRONG MEDIUM"
#define WRONG_MEDIUM_THRESHOLD 100000
#define C_N 3
#define C_M 10
#define RIENTRO 0.0
#define DIFFERENZE  // Normalization for difference
#define FEATURE_SIZE 16
#define K 100

extern char classNames[CLASS_VAL_SIZE][20];
extern float relaiabilityIndex[CLASS_VAL_SIZE];
extern float concentrationIndex[CLASS_VAL_SIZE];
extern float EWANormalizer[FEATURE_SIZE];
extern SPClassification classificationOutput[CLASS_VAL_SIZE + 1];
extern SPClassification tempOutput[CLASS_VAL_SIZE + 1];
extern float vect_dist[EWA_COEFFICIENT];
extern SPDataProcessing dataProcessing;
extern SPClassification lastClassification;
extern SPClassification predicted;
extern char wrongMediumName[20];

extern float normalized[FEATURE_SIZE];

extern float confdegree[CLASS_VAL_SIZE];

///* Dataset Section */


/* batch section*/
#define BATCH_SIZE 9
#define BATCH_PAYLOAD_SIZE 4
extern uint8_t setSensorPayloads[BATCH_SIZE][BATCH_PAYLOAD_SIZE];

extern int globalIndex;
extern short int dummyDataset[601][FEATURE_SIZE];

#endif /* TINYML_CLASSIFICATORS_KNN_CLASSIFIER_CONFIG_H_ */
