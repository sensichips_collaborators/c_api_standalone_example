#ifndef _NN_H_
#define _NN_H_

#include <time.h>
#include <math.h>
//#include "classifier_config.h"
#include "weights.h"

#define SOFTMAX  0      // Se vale 0 NON applico la softmax finale in predict, altrimenti la applico

typedef float nn_data;      // E' il tipo di dato utilizzato in tutto il file nn.h. Puoi mettere float o double

#define MAX_WEIGHT_VALUE 1.59  // I valori dei pesi vengono generati in modo random tra +/- MAX_WEIGHT_VALUE
#define MAX_INPUT_VALUE 0.97  // I valori di input vengono generati in modo random tra +/- MAX_INPUT_VALUE

/* MLP normalization parameters */
extern float m_attributeBases[INPUT_SIZE];
extern float m_attributeRanges[INPUT_SIZE];

typedef struct {

    /* MLP normalization parameters */
    float m_attributeBases[INPUT_SIZE];
    float m_attributeRanges[INPUT_SIZE];

    nn_data input_nodes[INPUT_SIZE];        // Nodi di input
    nn_data hidden_nodes[HIDDEN_SIZE];        // Nodi di hidden
    nn_data output_nodes[OUTPUT_SIZE];        // Nodi di output

    nn_data input_weights[INPUT_SIZE][HIDDEN_SIZE];      // Pesi tra input e hidden
    nn_data output_weights[HIDDEN_SIZE][OUTPUT_SIZE];    // Pesi tra hidden e output

    nn_data input_bias_node;          // Nodo bias di input
    nn_data output_bias_node;          // Nodo bias nello strato hidden

    nn_data input_bias_weights[HIDDEN_SIZE];      // Pesi di input nodo bias di input
    nn_data output_bias_weights[OUTPUT_SIZE];      // Pesi di output nodo bias di output

} neural_network;

// Funzione ReLU
nn_data relu(nn_data v);

// Funzione sigmoid
nn_data sigmoid(nn_data x);

// Setta i nodi di input con i valori dell'array measure
//void set_input(neural_network *nn, nn_data *measure);
void set_input(neural_network *nn, float *measure);

// Funzione di attivazione
nn_data activ_func(nn_data v);

// Propaga il nodo bias di input
void prop_input_bias(neural_network *nn);

// Propaga il nodo bias di output
void prop_output_bias(neural_network *nn);

// Propagazione input-hidden (nodo input i verso strato hidden)
void prop_input_hidden(neural_network *nn, int i);

// Propagazione hidden-output (nodo hidden i verso strato output)
void prop_hidden_output(neural_network *nn, int i);

// Applica la funzione di attivazione allo strato hidden
void activ_hidden_nodes(neural_network *nn);

// Applica la funzione di attivazione allo strato output
void activ_output_nodes(neural_network *nn);

// Azzera i valori sugli strati input-hidden-output (mantiene i pesi)
void reset_data_neural_network(neural_network *nn);

// Genera un numero in virgola mobile random tra -max_value e +max_value
nn_data get_random_value(nn_data max_value);

// Inizializza la rete neurale con dei pesi random
void init_random_neural_network(neural_network *nn);

// Inizializza la rete neurale con i pesi presenti nell'header "weights.h"
void init_neural_network(neural_network *nn);

// Estrae il valore massimo da un array di valori
nn_data extract_max(nn_data *input, int input_len);

// Estrae l'indice che punta al valore massimo di un array di valori
int extract_max_index(nn_data *input, int input_len);

// Normalizza il vettore di nn_data/double in numeri di probabilit� con somma totale pari a 1
void softmax(nn_data *input, int input_len);


// Propaga i valori di input nell'output
void predict(neural_network *nn);

void distributionForInstance(neural_network *nn, float *features, float *confDegree);


#endif
