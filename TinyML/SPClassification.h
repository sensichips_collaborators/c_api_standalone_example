/*
 * SPClassification.h
 *
 */

#ifndef TINYML_SPCLASSIFICATION_H_
#define TINYML_SPCLASSIFICATION_H_

#define CLASS_NAME_MAX_SIZE 20
#include "string.h"
#include "stdlib.h"
#include "../API/config/config.h"

typedef struct SPClassification SPClassification;

struct SPClassification{

	char className[CLASS_NAME_MAX_SIZE];
	float reliabilityIndex;
  float confidenceDegree;
  float concentration;

	int (*compareTo)(const SPClassification *this_c, const SPClassification *toCompare);
	void (*clone)(const SPClassification *this_c, SPClassification *cloned);  /* "copy"  */


};

void initSPClassification(SPClassification* this_c, char *className, float confidenceDegree,
		float reliabilityIndex);

#endif /* TINYML_SPCLASSIFICATION_H_ */
