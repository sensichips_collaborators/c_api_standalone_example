/*
 * SPClassification.c
 */
#include "SPClassification.h"


int compareTo(const SPClassification *this_c, const SPClassification *toCompare){
	if (this_c->confidenceDegree > toCompare->confidenceDegree) {
		return -1;
	}else 	if (this_c->confidenceDegree == toCompare->confidenceDegree) {
		return 0;
	}else
		return 0;
}

void clone(const SPClassification *this_c, SPClassification *cloned){  /* "copy"  */

	strncpy(cloned->className, this_c->className, CLASS_NAME_MAX_SIZE);
	cloned->confidenceDegree = this_c->confidenceDegree;
	cloned->concentration = this_c->concentration;
	cloned->reliabilityIndex = this_c->reliabilityIndex;

}

void initSPClassification(SPClassification* this_c, char *className, float confidenceDegree,
		float reliabilityIndex){

	strcpy(this_c->className, className);
	this_c->confidenceDegree = confidenceDegree;
	this_c->reliabilityIndex = reliabilityIndex;
	this_c->concentration = 0;

	this_c->compareTo = &compareTo;
	this_c->clone = &clone;
}
