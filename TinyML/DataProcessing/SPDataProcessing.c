/*
 * SPDataProcessing.c
 */

#include "SPDataProcessing.h"
#include "../classificators/SPClassificatorMLP.h"


void process(SPDataProcessing *this_c, SPClassification *currentClassification,
             short int *normalizedFeature, SPClassification *output, void *classifier) {

  SPClassificatorMLP *cls = (SPClassificatorMLP *) classifier;

  if (this_c->evaluate) {
    if (this_c->lastClassification->className[0] == '\0') {
      currentClassification[0].clone(&currentClassification[0], this_c->lastClassification);
      currentClassification[0].clone(&currentClassification[0], this_c->predicted);
    } else if (strncmp(this_c->lastClassification->className, currentClassification[0].className,
                       CLASS_NAME_MAX_SIZE) == 0) {
      this_c->count++;
    } else {
      this_c->count = 1;
    }

    currentClassification[0].clone(&currentClassification[0], this_c->lastClassification);

    if (this_c->count >= this_c->N) {
      currentClassification[0].clone(&currentClassification[0], this_c->predicted);
      if (strncmp(this_c->predicted->className, "SWW", CLASS_NAME_MAX_SIZE) != 0 &&
          strncmp(this_c->predicted->className, "AIR", CLASS_NAME_MAX_SIZE) != 0 &&
          strncmp(this_c->predicted->className, "TRACKING", CLASS_NAME_MAX_SIZE) != 0 &&
          strncmp(this_c->predicted->className, "TATP", CLASS_NAME_MAX_SIZE) != 0) {

        this_c->evaluate = false;
        this_c->count = 1;
      }
    }
  } else {
    for (int i = 0; i < cls->featureSize; ++i) {
      if (abs(normalizedFeature[i] - 1) < this_c->threshold) {
        this_c->evaluate = true;
        this_c->count = 1;
        cls->setState(cls, STATE_BA);
      }
    }
  }

  int outDim = 0;

  initSPClassification(&output[outDim], this_c->predicted->className,
                       this_c->predicted->confidenceDegree, this_c->predicted->reliabilityIndex);
  outDim++;

  for (int i = 0; i < cls->classValSize; ++i) {
    initSPClassification(&output[outDim], currentClassification[i].className,
                         currentClassification[i].confidenceDegree, currentClassification[i].reliabilityIndex);
    outDim++;
  }


}

void initSPDataProcessing(SPDataProcessing *this_c, unsigned int N,
                          unsigned int M, float threshold, SPClassification *lastClassification,
                          SPClassification *predicted) {

  this_c->evaluate = true;
  this_c->count = 1;
  this_c->N = N;
  this_c->M = M;
  this_c->threshold = threshold; /* RIENTRO */
  this_c->lastClassification = lastClassification;
  this_c->lastClassification->className[0] = '\0';
  this_c->predicted = predicted;

  this_c->process = &process;


}
