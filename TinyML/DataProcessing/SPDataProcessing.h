/*
 * SPDataProcessing.h
 */

#ifndef TINYML_DATAPROCESSING_SPDATAPROCESSING_H_
#define TINYML_DATAPROCESSING_SPDATAPROCESSING_H_

#include "../SPClassification.h"
#include "string.h"
#include "../../API/util/types.h"

typedef struct SPDataProcessing SPDataProcessing;

struct SPDataProcessing{

	SPClassification *lastClassification;
	SPClassification *predicted;

	bool evaluate;
	unsigned int count;
	unsigned int M;
	unsigned int N;
	float threshold;

	void (*process)(SPDataProcessing *this_c, SPClassification *currentClassification,
			short int *normalizedFeature, SPClassification *output, void *classifier);

};

void initSPDataProcessing(SPDataProcessing *this_c, unsigned int N,
		unsigned int M, float threshold, SPClassification *lastClassification,
		SPClassification *predicted);

#endif /* TINYML_DATAPROCESSING_SPDATAPROCESSING_H_ */
