#ifndef SENSIBUS_H
#define SENSIBUS_H


extern "C" {
// #include "../../API/config/init.h"
}

#include "MsgSENSIBUS.h"
#include "StatusSENSIBUS.h"

// Definisco le direttive e le Macro:
#define MODE_INPUT(pin)          pinMode(pin,INPUT)
#define MODE_OUTPUT(pin)         pinMode(pin,OUTPUT_OPEN_DRAIN)
#define WRITE_LOW(pin)           digitalWrite(pin, LOW)
#define WRITE_HIGH(pin)          digitalWrite(pin, HIGH)
#define READ(pin)                digitalRead(pin)


#define SENSIBUS_SPEED             0xDC
#define SENSIBUS_ADDRESS           0xD8

#define SENSIBUS_VALUE_NO_ADDRESS             0xDC
#define SENSIBUS_VALUE_SHORT_ADDRESS          0xEC
#define SENSIBUS_VALUE_FULL_ADDRESS           0xCC

#define SENSIBUS_SPEED       0xDC
#define SYS_CLOCK_RUN7       10000000
#define SYS_CLOCK_RUN8       6784000 // Caso Run8

// ******************** Inizio SensiBus.h *******************

// utilizzando le Direttive, individuo il microcontrollore e default pin: STM o Arduino o esp8266
#ifdef ESP8266
#define   DATI          D3
#define   SENSIBUS_VCC  D2

#elif defined(ARDUINO_XIAO_ESP32C3) || defined(ARDUINO_XIAO_ESP32S3)
#define   SENSIBUS_VCC  D6
#define   DATI       D10
#define   DONTCARE     D9
#define   LED_BUILTIN   D7

 #else // Suitable for ESP32
#define   SENSIBUS_VCC  32//21
#define   DATI        18//21//17//23
#define   DONTCARE  22//16 //26
#define   LED_BUILTIN  4//22//4
#endif

// COMMENT THIS DEFINITION FOR CLOCK_TICK
// REMEMBER TO COMPILE WITH 160MHz option for clock_tick
#define DELAYMICROSECONDS

#ifdef DELAYMICROSECONDS

class CommSENSIBUS {
private:
    uint8_t PIN_DATI;

    float TS;
    float TS_2_Write;
    float TS_2_Read;
    float TS_4;
    float TS_8;
    float TS_16;
    float DELAY_BETWEEN_BIT;
    float DELAY_BETWEEN_BYTE;
    float DELAY_AFTER_START;

    //Update RUN8 Calibration
    float SYS_CLOCK_DRIVER;
    float START_COM_DELAY;
    float START_COM_DELAY_2;
    byte LAST_SPEED;

    int TS_START_COM;
    int TS_START_COM_2;

    byte read();

    void write(byte);

public:
    CommSENSIBUS(uint8_t pin, byte newSPEED = 0xFF); // Value for SENSIBUS_SET register
    void setTS(byte newSPEED);

    bool startCom();         // Return TRUE if at least one chip is ALIVE
    void transaction(MsgSENSIBUS &SBFromUSB, StatusSENSIBUS &SBStatus);

    int getTS_START_COM();

    void onlyForTest();

    bool tryDetectRUN(); /* Test startCom() with different configs */
};




// ******************** Fine SensiBus.h *******************

#else

class CommSENSIBUS {
private:
  uint8_t PIN_DATI;

  int CkT;
  int CkT_3_4;
  int CkT_2;
  int CkT_2_read;
  int CkT_4;
  int CkT_RiseTime;

  int CkT_START_COM;
  int CkT_START_COM_2;
  int CkT_START_COM_4;
  int CkT_START_COM_8;

  int TkZero32x;
  
  byte read();      
  void write(byte);

public:
  CommSENSIBUS(uint8_t pin, byte newSPEED = 0xFF); // Value for SENSIBUS_SET register
  void  setSPEED(byte newSPEED);
  byte  adaptSPEED(byte newSPEED);
  bool  startCom();         // Return TRUE if at least one chip is ALIVE
  void  transaction(MsgSENSIBUS &SBFromUSB, StatusSENSIBUS &SBStatus);
  float elapsedStartComResponse();
  void  setTkZero32x(int b);
};

#endif

#endif

// ******************** Inizio Appunti **********************
// Dalla Doc.: If the pin is configured as an INPUT, writing a HIGH value with digitalWrite() 
// will enable an internal 20K pullup resistor.

// Per inviare byte su singolo PIN: https://www.arduino.cc/en/Tutorial/BitMask

// Per leggere lo stato di un PIN: https://www.arduino.cc/en/Reference.PulseInLong
// Oppure da qui: https://github.com/adafruit/DHT-sensor-library/blob/master/DHT.cpp

// OPEN-DRAIN, PULL-UP STM32: http://embeddedsystemengineering.blogspot.it/2016/01/arm-cortex-m3-stm32f103-tutorial-gpio.html
// ******************** Fine Appunti ************************






