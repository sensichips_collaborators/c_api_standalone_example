#include "CommTCP.h"
#include "SPIFFS.h"
// extern "C"{
#include "../../EXT_OPERATION/ExtendedOperation.h"
// }

CommTCP::CommTCP() {
  //this->status = s;
}

void CommTCP::init(WiFiClient *client, WiFiServer *server) {
  this->client = client;
  this->server = server;
}

boolean CommTCP::rcvByte(byte &rcvdByte) {
  // Wait for rcvdByte
  while (client->available() <= 0 && client->connected());
  if (client->connected()) {
    rcvdByte = client->read();
    return true;
  } else {
    return false;
  }
}


void CommTCP::receiveHeader(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status) {

  if (rcvByte(SBMsg.command)) {
    writeOnDebug(0xA0);
    writeOnDebug(SBMsg.command);

    if (SBMsg.command & EXT_INSTR) {
      //status.instructionType = EXT_INSTR;
      // TODO: manage ext instruction
      SBMsg.instructionType = SBMsg.command;
      //writeOnDebug(0xA0);

      if (SBMsg.instructionType == EXT_INSTR_CHIPLIST) {
        //in questo caso il payloadsize � di 2 byte
        SBMsg.payloadSize = 0;
        for (int i = 0; i < 2; i++) {//ricevo 2 byte di payloadsize
          while (!rcvByte(SBMsg.dataRcvLength));
          if (i == 0) {
            SBMsg.payloadSize = ((int) SBMsg.dataRcvLength) * 256;
          } else {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength;
          }
        }
      } else if (SBMsg.instructionType == EXT_INSTR_SAVE_FILE) {

        SBMsg.payloadSize = 0;
        for (int i = 0; i < 3; i++) {//ricevo 3 byte di payloadsize
          while (!rcvByte(SBMsg.dataRcvLength));
          if (i == 0) {
            SBMsg.payloadSize = ((int) SBMsg.dataRcvLength) * 65536;
          } else if (i == 1) {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength * 256;
          } else {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength;
          }
        }
        return;
      } else if (SBMsg.instructionType == EXT_INSTR_TEST) {
        SBMsg.payloadSize = 0;
        return;
      } else {
        SBMsg.payloadSize = 0;
        while (!rcvByte(SBMsg.dataRcvLength));
        SBMsg.payloadSize += (int) SBMsg.dataRcvLength;

        //Serial.print("TEST_payloadSize: 0x");
        //Serial.println(SBMsg.payloadSize, HEX);
      }

    } else {
      // Manage normal instruction
      //*** Receive address
      status.addressingMode = SBMsg.command & ADDRESS_MODE_MASK;

      if (status.addressingMode == LastAddressValue) {
        status.addressingMode = status.lastAddressingMode;
      } else {
        status.lastAddressingMode = status.addressingMode;
        if (status.addressingMode == NoAddressValue) {
          // No addressing byte to receive
          SBMsg.chipAddressLength = 0;

        } else if (status.addressingMode == ShortAddressValue) {
          SBMsg.chipAddressLength = 1;
        } else if (status.addressingMode == FullAddressValue) {
          SBMsg.chipAddressLength = 6;
        }
      }
      SBMsg.dataRcvLength = SBMsg.command & DATA_LENGTH_MASK;
      writeOnDebug(0xA1);
      writeOnDebug(SBMsg.command);
      writeOnDebug(SBMsg.dataRcvLength);
      SBMsg.instructionType = NORMAL_INSTR;
    }
  } else {
    SBMsg.instructionType = NO_INSTR;
  }

  return;
}


int CommTCP::receiveData(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status) {
  int len = 0;
  //SBMsg.dataRcvLength = 0;


  if (SBMsg.instructionType & EXT_INSTR) {

    if (SBMsg.instructionType == NULL) {//EXT_INSTR_CHIPLIST_MASK){
      //			for(int i = 0; i < SBMsg.dataRcvLength; i++){
      //				while(Serial.available() == 0); // valutare delay 1 microseconds
      //				Serial.readBytes(&SBMsg.dataRcv[i], 2);
      //				delayMicroseconds(1);
      //			}
    } else if (SBMsg.instructionType == EXT_INSTR_SAVE_FILE) {

//			SPIFFS.begin(true);
//			File file = SPIFFS.open("/data.txt", "a");
//			//			if(!file){
//			//				Serial.println("ERRORE CREAZIONE FILE");
//			//				return 0;
//			//			}
//			int packetToReceive = SBMsg.payloadSize / DATA_RCVD_MAXLEN;
//			int byteReceived = 0;
//			while(packetToReceive > 0){
//				for(int i = 0; i < 32; i++){
//					while(client ->available() == 0);
//					//Serial.readBytes(&SBMsg.dataRcv[i], 1);
//					rcvByte(SBMsg.dataRcv[i]);
//					//writeFile(SBMsg.dataRcv[i]);
//					file.print((char)SBMsg.dataRcv[i]);
//				}
//				byteReceived += 32;
//				packetToReceive--;
//				if(SBMsg.payloadSize - byteReceived > 0){
//					client->write(0xA0); //ok message
//					client->write((unsigned char)0x00); //payload size
//				}
//			}
//			if(SBMsg.payloadSize - byteReceived > 0){
//				for(int j = 0; j < SBMsg.payloadSize - byteReceived; j++){
//					while(client->available() == 0);
//					rcvByte(SBMsg.dataRcv[j]);
//					file.print((char)SBMsg.dataRcv[j]);
//				}
//			}
//			file.close();
//			SBMsg.instructionType = EXT_INSTR_SAVE_FILE;

    } else {
      for (int i = 0; i < SBMsg.payloadSize; i++) {
        while (client->available() == 0);
        rcvByte(SBMsg.dataRcv[i]);
        delayMicroseconds(1);
      }

    }

  } else { // NORMAL_INSTR

    //**** Receive address
    // int len = 0;
    //**** Receive address
    for (int i = 0; i < SBMsg.chipAddressLength; i++) {
      rcvByte(SBMsg.chipAddress[i]);
    }
    len = SBMsg.chipAddressLength;

    //**** Receive register
    rcvByte(SBMsg.registerAddress[0]);

    writeOnDebug(0xB0);
    writeOnDebug(SBMsg.registerAddress[0]);
    writeOnDebug(SBMsg.dataRcvLength);

    // Establish if is a read or write operation (1 read, 0 false)
    status.isRead = IS_WRITE_MASK & SBMsg.registerAddress[0];

    //**** Receive data
    if (status.isRead) {
      for (int i = 0; i < SBMsg.dataRcvLength; i++) {
        SBMsg.dataRcv[i] = 0xFF;                        // FF in order to wait for data
      }
      len = SBMsg.dataRcvLength;
    } else {
      for (int i = 0; i < SBMsg.dataRcvLength; i++) {
        rcvByte(SBMsg.dataRcv[i]);
      }
      len = SBMsg.dataRcvLength;
    }
  }
  return len;
}


void CommTCP::sendResponse(MsgSENSIBUS &SBMsg) {
  writeOnDebug(0xD0);
  if ((SBMsg.instructionType & EXT_INSTR) == EXT_INSTR) {

    if (SBMsg.instructionType == EXT_INSTR_USBSPEED) {
      Serial.flush();
      delay(1000);

      // Clear receiving buffer
      while (Serial.available())
        Serial.read();
    } else if (SBMsg.instructionType == EXT_INSTR_TEST) {
      client->flush();
      for (int i = 0; i < SBMsg.dataToHostLength; i++) {
        writeOnDebug(SBMsg.dataToHost[i]);
        client->write(SBMsg.dataToHost[i]);
      }

    } else if (SBMsg.instructionType == EXT_INSTR_GETEIS ||
               SBMsg.instructionType == EXT_INSTR_GET_SENSOR) {
      // union output{
      // 	sp_double value;
      // 	uint8_t appo[8];
      // } measure;
      // client->write(0xA0);
      // int payloadSize;
      // payloadSize = spCluster.dimChipList * EIS_NUM_OUTPUT * 8;
      // client->write(payloadSize);
      // client->flush();
      // for(int i = 0; i < spCluster.dimChipList; i++){
      // 	for (int j = 0; j < EIS_NUM_OUTPUT; j++){
      // 		measure.value = output1[i][j];
      // 		client->write(measure.appo[7]);
      // 		client->write(measure.appo[6]);
      // 		client->write(measure.appo[5]);
      // 		client->write(measure.appo[4]);
      // 		client->write(measure.appo[3]);
      // 		client->write(measure.appo[2]);
      // 		client->write(measure.appo[1]);
      // 		client->write(measure.appo[0]);
      // 	}
      // }
    } else if (SBMsg.instructionType == EXT_INSTR_GET_POT) {

      // union output{
      // 	sp_double value;
      // 	uint8_t appo[8];
      // } measure;

      // client->write(0xA0);
      // int payloadSize;
      // payloadSize = spCluster.dimChipList * POT_NUM_OUTPUT * 8;
      // client->write(payloadSize);
      // client->flush();

      // for(int i = 0; i < spCluster.dimChipList; i++){
      // 	for (int j = 0; j < POT_NUM_OUTPUT; j++){
      // 		measure.value = output1[i][j];
      // 		client->write(measure.appo[7]);
      // 		client->write(measure.appo[6]);
      // 		client->write(measure.appo[5]);
      // 		client->write(measure.appo[4]);
      // 		client->write(measure.appo[3]);
      // 		client->write(measure.appo[2]);
      // 		client->write(measure.appo[1]);
      // 		client->write(measure.appo[0]);
      // 	}
      // }

    } else if (SBMsg.instructionType == EXT_INSTR_GET_CLASSIFICATION) {

      union output {
          float value;
          uint8_t appo[4];
      } measure;

//			client->write(0xA0); //ok message
//			uint8_t payloadSize;
//			payloadSize = spCluster.dimChipList *( (CLASS_VAL_SIZE+1) * (4/* Float ?*/ + 1/* CLASS_VAL ID*/) +
//					4 /*Distance*/ + 1 /* BS_STATE*/);
//
//			client->write(payloadSize);
//			client->flush();
//			/*	TODO: più chip?*/
//			for(int i = 0; i < spCluster.dimChipList; i++){
//				/*BS_STATE*/
//				client->write(spClassificatorKNN.getStateID(&spClassificatorKNN));
//				/* Distance */
//				measure.value = spClassificatorKNN.distanceValue;
//				client->write(measure.appo[3]);
//				client->write(measure.appo[2]);
//				client->write(measure.appo[1]);
//				client->write(measure.appo[0]);
//
//				for (int j = 0; j < CLASS_VAL_SIZE + 1; j++){
//					/*ID CLASSVAL*/
//					client->write( spClassificatorKNN.getClassValID(&spClassificatorKNN, classificationOutput[j].className) );
//					measure.value = classificationOutput[j].confidenceDegree; //TODO: manage multiple chips
//					client->write(measure.appo[3]);
//					client->write(measure.appo[2]);
//					client->write(measure.appo[1]);
//					client->write(measure.appo[0]);
//				}
//			}

    } else if (SBMsg.instructionType == EXT_INSTR_NUM_OF_CHIP) {

//			client->flush();
//			client->write(0xA0); //ok message
//			client->write(0x01); //payload size
//			//TODO detect numero dei chip su cavo all'avvio del MCU
//			client->write(0x02); //rispondo dicendo che ci sono 2 chip
    } else if (SBMsg.instructionType == EXT_INSTR_DETECT_CHIP) {
//			client->flush ();
//			client->write(0xA0); //ok message
//			int payloadSize = 2 * 5; //2 chip ogni id lungo 5 byte totale 2*5 byte di payload
//			//TODO detect id dei chip su cavo all'avvio del MCU
//			client->write(payloadSize); //rispondo dicendo che ci sono 2 chip
//			//Invio ID primo chip
//			client->write((unsigned char)0x00);
//			client->write((unsigned char)0x00);
//			client->write((unsigned char)0x00);
//			client->write(0x85);
//			client->write(0x05);
//			//invio ID secondo chip
//			client->write((unsigned char)0x00);
//			client->write((unsigned char)0x00);
//			client->write((unsigned char)0x00);
//			client->write(0x81);
//			client->write(0x05);
    } else {
      client->flush();
      client->write(0xA0); //ok message
      client->write((unsigned char) 0x00);; //payload size
    }
  } else {//Normal instructions
//		client->flush ();
//		for(int i = 0; i < SBMsg.dataToHostLength; i++){
//			writeOnDebug(SBMsg.dataToHost[i]);
//			client->write(SBMsg.dataToHost[i]);
//		}
  }
}

