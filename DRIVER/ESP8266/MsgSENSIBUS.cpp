#include "MsgSENSIBUS.h"


MsgSENSIBUS::MsgSENSIBUS(byte payload, byte data[DATA_RCVD_MAXLEN]) {

  instructionType = NO_INSTR;

  command = 0;

  payloadSize = 0;

  registerAddress[0] = 0;

  chipAddressLength = 0;
  for (int i = 0; i < ADDRESS_MAXLEN; i++) {
    chipAddress[i] = 0;
  }

  dataRcvLength = payload;
  for (int i = 0; i < DATA_RCVD_MAXLEN; i++) {
    dataRcv[i] = data[i];
  }

  dataToHostLength = 0;
  for (int i = 0; i < DATA_TOHOST_MAXLEN; i++) {
    dataToHost[i] = 0;
  }
}


void MsgSENSIBUS::reset() {
  instructionType = NO_INSTR;

  command = 0;

  payloadSize = 0;

  registerAddress[0] = 0;

  chipAddressLength = 0;
  for (int i = 0; i < ADDRESS_MAXLEN; i++) {
    chipAddress[i] = 0;
  }

  dataRcvLength = 0;
  for (int i = 0; i < DATA_RCVD_MAXLEN; i++) {
    dataRcv[i] = 0;
  }

  dataToHostLength = 0;
  for (int i = 0; i < DATA_TOHOST_MAXLEN; i++) {
    dataToHost[i] = 0;
  }

}
