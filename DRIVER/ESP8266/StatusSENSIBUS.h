#ifndef STATUSSENSIBUS_H
#define STATUSSENSIBUS_H

#include <Arduino.h>
#include "MsgSENSIBUS.h"

#define MAXNUM_CHIPS 50


class StatusSENSIBUS {
public:
    byte addressingMode;
    byte lastAddressingMode;
    byte isRead;         //read or write operation (1 read, 0 false)



    byte clusterID;
    byte cluster[MAXNUM_CHIPS][ADDRESS_MAXLEN];

};


#endif
// ******************** Fine StatusSENSIBUS.h *******************






