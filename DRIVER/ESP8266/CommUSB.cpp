#include "CommUSB.h"
#include <SPIFFS.h>
// extern "C"{
#include "../../EXT_OPERATION/fileHandlerMCU.h"
// }
#include "../../EXT_OPERATION/ExtendedOperation.h"

extern HardwareSerial RedirSerial;

CommUSB::CommUSB() { //SensiBusStatus *s){
}


void CommUSB::receiveHeader(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status) {

  //byte rcvByte;

  //**** Receive first byte
  if (Serial.available()) {
    delayMicroseconds(1000); /* In modo tale da evitare errori lettura randomici
													(rilevati 0xFF invece di 0x94) che bloccaano il loop senza mandare in timeout
													winux, dato che il microcontrollore risponde correttamente ai testMCU().
													Valutare la possibilità di bloccare gli interrupt.
													TODO: Migliorare il sistema di invio pacchetti lato winux in modo tale che rinvii
													l' ultimo messaggio quando non riceve risposta, invece di continuare ad aspettarsene una
													quando MCU e SENSIPLUS rispondono (con testMCU())  */
    SBMsg.command = Serial.read();
    writeOnDebug(0xA0);
    writeOnDebug(SBMsg.command);


    //Serial.print("comando: 0x");
    //Serial.println(SBMsg.command, HEX);

    if (SBMsg.command & EXT_INSTR) {
      //status.instructionType = EXT_INSTR;
      SBMsg.instructionType = SBMsg.command;

      //Serial.print("intruction type: 0x");
      //Serial.println(SBMsg.instructionType, HEX);
      //RedirSerial.print("intruction type: 0x");
      //RedirSerial.println(SBMsg.instructionType, HEX);

      if (SBMsg.instructionType == EXT_INSTR_CHIPLIST) {
        //in questo caso il payloadsize � di 2 byte
        SBMsg.payloadSize = 0;
        for (int i = 0; i < 2; i++) {//ricevo 2 byte di payloadsize
          while (Serial.readBytes(&SBMsg.dataRcvLength, 1) != 1);
          if (i == 0) {
            SBMsg.payloadSize = ((int) SBMsg.dataRcvLength) * 256;
          } else {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength;
          }
        }
      } else if (SBMsg.instructionType == EXT_INSTR_SAVE_FILE) {
        SBMsg.payloadSize = 0;
        //				while(Serial.readBytes(&SBMsg.dataRcvLength, 1) != 1); //ricevo il payloadsize
        //				SBMsg.payloadSize += (int) SBMsg.dataRcvLength;
        SBMsg.payloadSize = 0;
        for (int i = 0; i < 3; i++) {//ricevo 3 byte di payloadsize
          while (Serial.readBytes(&SBMsg.dataRcvLength, 1) != 1);
          if (i == 0) {
            SBMsg.payloadSize = ((int) SBMsg.dataRcvLength) * 65536;
          } else if (i == 1) {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength * 256;
          } else {
            SBMsg.payloadSize += (int) SBMsg.dataRcvLength;
          }
        }
        return;
      } else if (SBMsg.instructionType == EXT_INSTR_TEST) {
        SBMsg.payloadSize = 0;
        return;
      } else {
        SBMsg.payloadSize = 0;
        while (Serial.readBytes(&SBMsg.dataRcvLength, 1) != 1); //ricevo il payloadsize
        SBMsg.payloadSize += (int) SBMsg.dataRcvLength;

        //Serial.print("TEST_payloadSize: 0x");
        //Serial.println(SBMsg.payloadSize, HEX);
      }

      //receiveData(SBMsg, status);
      //writeOnDebug(0xA0);

    } else {
      // Manage normal instruction
      //*** Receive address
      status.addressingMode = SBMsg.command & ADDRESS_MODE_MASK;

      if (status.addressingMode == LastAddressValue) {
        status.addressingMode = status.lastAddressingMode;
      } else {
        status.lastAddressingMode = status.addressingMode;
        if (status.addressingMode == NoAddressValue) {
          // No addressing byte to receive
          SBMsg.chipAddressLength = 0;

        } else if (status.addressingMode == ShortAddressValue) {
          SBMsg.chipAddressLength = 1;
        } else if (status.addressingMode == FullAddressValue) {
          SBMsg.chipAddressLength = 6;
        }
      }
      SBMsg.dataRcvLength = SBMsg.command & DATA_LENGTH_MASK;
      writeOnDebug(0xA1);
      writeOnDebug(SBMsg.command);
      writeOnDebug(SBMsg.dataRcvLength);
      SBMsg.instructionType = NORMAL_INSTR;
    }
  } else {
    SBMsg.instructionType = NO_INSTR;
  }

  return;

}


int CommUSB::receiveData(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status) {
  int len = 0;
  //SBMsg.dataRcvLength = 0;

  if (SBMsg.instructionType & EXT_INSTR) {
    //Serial.write(0xA1);
    if (SBMsg.instructionType == NULL) {//EXT_INSTR_CHIPLIST_MASK){
      //			for(int i = 0; i < SBMsg.dataRcvLength; i++){
      //				while(Serial.available() == 0); // valutare delay 1 microseconds
      //				Serial.readBytes(&SBMsg.dataRcv[i], 2);
      //				delayMicroseconds(1);
      //			}
    } else if (SBMsg.instructionType == EXT_INSTR_SAVE_FILE) {

      SPIFFS.begin(true);
      File file = SPIFFS.open("/data.txt", "a");
      //			if(!file){
      //				Serial.println("ERRORE CREAZIONE FILE");
      //				return 0;
      //			}
      int packetToReceive = SBMsg.payloadSize / DATA_RCVD_MAXLEN;
      int byteReceived = 0;
      while (packetToReceive > 0) {
        for (int i = 0; i < 32; i++) {
          while (Serial.available() == 0);
          Serial.readBytes(&SBMsg.dataRcv[i], 1);
          //writeFile(SBMsg.dataRcv[i]);
          file.print((char) SBMsg.dataRcv[i]);
        }
        byteReceived += 32;
        packetToReceive--;
        if (SBMsg.payloadSize - byteReceived > 0) {
          Serial.write(0xA0); //ok message
          Serial.write(0x00); //payload size
        }
      }
      if (SBMsg.payloadSize - byteReceived > 0) {
        for (int j = 0; j < SBMsg.payloadSize - byteReceived; j++) {
          while (Serial.available() == 0);
          Serial.readBytes(&SBMsg.dataRcv[j], 1);
          //writeFile(SBMsg.dataRcv[j]);
          file.print((char) SBMsg.dataRcv[j]);
        }
      }
      file.close();
      SBMsg.instructionType = EXT_INSTR_SAVE_FILE;

    } else {
      for (int i = 0; i < SBMsg.payloadSize; i++) {
        while (Serial.available() == 0); // valutare delay 1 microseconds
        Serial.readBytes(&SBMsg.dataRcv[i], 1);
        delayMicroseconds(1);

        //Serial.print("TEST_dataRcv: 0x");
        //Serial.println(SBMsg.dataRcv[i], HEX);
      }

    }

  } else { // NORMAL_INSTR

    //**** Receive address
    len = Serial.readBytes(SBMsg.chipAddress, SBMsg.chipAddressLength);

    //**** Receive register
    Serial.readBytes(SBMsg.registerAddress, 1);
    writeOnDebug(0xB0);
    writeOnDebug(SBMsg.registerAddress[0]);
    writeOnDebug(SBMsg.dataRcvLength);

    // Establish if is a read or write operation (1 read, 0 false)
    status.isRead = IS_WRITE_MASK & SBMsg.registerAddress[0];

    //**** Receive data
    if (status.isRead) {
      for (int i = 0; i < SBMsg.dataRcvLength; i++) {
        SBMsg.dataRcv[i] = 0xFF;                        // FF in order to wait for data
      }
      len = SBMsg.dataRcvLength;
    } else {
      len = Serial.readBytes(SBMsg.dataRcv, SBMsg.dataRcvLength);
    }
  }
  return len;
}


void CommUSB::sendResponse(MsgSENSIBUS &SBMsg) {
  writeOnDebug(0xD0);
  if ((SBMsg.instructionType & EXT_INSTR) == EXT_INSTR) {

    if (SBMsg.instructionType == EXT_INSTR_USBSPEED) {
      //Serial.write(0xBB);
      //Serial.write(newSpeed);
      Serial.flush();
      delay(1000);

      // Clear receiving buffer
      while (Serial.available())
        Serial.read();

      //Serial.begin(newUSBSpeed);

      // Clear receiving buffer
      while (Serial.available())
        Serial.read();
    } else if (SBMsg.instructionType == EXT_INSTR_TEST) {
      Serial.flush();
      for (int i = 0; i < SBMsg.dataToHostLength; i++) {
        writeOnDebug(SBMsg.dataToHost[i]);
        Serial.write(SBMsg.dataToHost[i]);
      }

    } else if (SBMsg.instructionType == EXT_INSTR_GETEIS ||
               SBMsg.instructionType == EXT_INSTR_GET_SENSOR) {
      union output {
          sp_double value;
          uint8_t appo[8];
      } measure;
      Serial.write(0xA0); //ok message
      int payloadSize;
      payloadSize = spCluster.dimChipList * MEASURES_SIZE * 8;
      Serial.write(payloadSize);
      Serial.flush();
      for (int i = 0; i < spCluster.dimChipList; i++) {
        for (int j = 0; j < MEASURES_SIZE; j++) {
          measure.value = extOpOutput[i][j];

          Serial.write(measure.appo[7]);
          Serial.write(measure.appo[6]);
          Serial.write(measure.appo[5]);
          Serial.write(measure.appo[4]);
          Serial.write(measure.appo[3]);
          Serial.write(measure.appo[2]);
          Serial.write(measure.appo[1]);
          Serial.write(measure.appo[0]);
        }
      }
    } else if (SBMsg.instructionType == EXT_INSTR_GET_POT) {

      // union output{
      // 	sp_double value;
      // 	uint8_t appo[8];
      // } measure;

      // Serial.write(0xA0); //ok message
      // int payloadSize;
      // payloadSize = spCluster.dimChipList * POT_NUM_OUTPUT * 8;

      // Serial.write(payloadSize);
      // Serial.flush ();
      // for(int i = 0; i < spCluster.dimChipList; i++){
      // 	for (int j = 0; j < POT_NUM_OUTPUT; j++){
      // 		measure.value = output1[i][j];
      // 		Serial.write(measure.appo[7]);
      // 		Serial.write(measure.appo[6]);
      // 		Serial.write(measure.appo[5]);
      // 		Serial.write(measure.appo[4]);
      // 		Serial.write(measure.appo[3]);
      // 		Serial.write(measure.appo[2]);
      // 		Serial.write(measure.appo[1]);
      // 		Serial.write(measure.appo[0]);
      // 	}
      // }

    } else if (SBMsg.instructionType == EXT_INSTR_GET_CLASSIFICATION) {

      union output {
          float value;
          uint8_t appo[4];
      } measure;

      Serial.write(0xA0); //ok message
      uint8_t payloadSize;
      payloadSize = spCluster.dimChipList * ((CLASS_VAL_SIZE + 1) * (4/* Float ?*/ + 1/* CLASS_VAL ID*/) +
                                             4 /*Distance*/ + 1 /* BS_STATE*/);

      Serial.write(payloadSize);
      Serial.flush();
      /*	TODO: più chip?*/
      for (int i = 0; i < spCluster.dimChipList; i++) {
        /*BS_STATE*/
        Serial.write(spClassificatorMLP.getStateID(&spClassificatorMLP));
        /* Distance */
        measure.value = spClassificatorMLP.distanceValue;
        Serial.write(measure.appo[3]);
        Serial.write(measure.appo[2]);
        Serial.write(measure.appo[1]);
        Serial.write(measure.appo[0]);

        for (int j = 0; j < CLASS_VAL_SIZE + 1; j++) {
          /*ID CLASSVAL*/
          Serial.write(spClassificatorMLP.getClassValID(&spClassificatorMLP, classificationOutput[j].className));
          measure.value = classificationOutput[j].confidenceDegree; //TODO: manage multiple chips
          Serial.write(measure.appo[3]);
          Serial.write(measure.appo[2]);
          Serial.write(measure.appo[1]);
          Serial.write(measure.appo[0]);
        }
      }

    } else if (SBMsg.instructionType == EXT_INSTR_NUM_OF_CHIP) {
      Serial.flush();
      Serial.write(0xA0); //ok message
      Serial.write(0x01); //payload size
      //TODO detect numero dei chip su cavo all'avvio del MCU
      Serial.write(0x02); //rispondo dicendo che ci sono 2 chip
    } else if (SBMsg.instructionType == EXT_INSTR_DETECT_CHIP) {
      Serial.flush();
      Serial.write(0xA0); //ok message
      int payloadSize = 2 * 5; //2 chip ogni id lungo 5 byte totale 2*5 byte di payload
      //TODO detect id dei chip su cavo all'avvio del MCU
      Serial.write(payloadSize); //rispondo dicendo che ci sono 2 chip
      //Invio ID primo chip
      Serial.write(0x00);
      Serial.write(0x00);
      Serial.write(0x00);
      Serial.write(0x85);
      Serial.write(0x05);
      //invio ID secondo chip
      Serial.write(0x00);
      Serial.write(0x00);
      Serial.write(0x00);
      Serial.write(0x81);
      Serial.write(0x05);
    } else {
      Serial.flush();
      Serial.write(0xA0); //ok message
      Serial.write(0x00); //payload size
    }
  } else {//Normal instructions
    Serial.flush();
    for (int i = 0; i < SBMsg.dataToHostLength; i++) {
      writeOnDebug(SBMsg.dataToHost[i]);
      Serial.write(SBMsg.dataToHost[i]);
    }
  }

}


// ******************** Fine SensiBusMsgFromUSB.cpp *******************

