#ifndef COMMTCP_H
#define COMMTCP_H

#ifdef ESP8266
#include <WiFiClient.h>
#else

#include <WiFi.h>

#endif

#include "StatusSENSIBUS.h"
#include "MsgSENSIBUS.h"


class CommTCP {

private:
    //StatusSENSIBUS *status;
    WiFiServer *server;
    WiFiClient *client;

    boolean rcvByte(byte &data);

public:

    CommTCP();

    byte addressLength;
    byte registerAddress[1];
    byte chipAddress[ADDRESS_MAXLEN];
    byte dataRcvLength;
    byte dataRcv[DATA_RCVD_MAXLEN];

    void receiveHeader(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status);

    int receiveData(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status);

    void sendResponse(MsgSENSIBUS &SBMsg);

    void init(WiFiClient *client, WiFiServer *server);
};

// ******************** Fine CommTCP.h *******************
#endif

