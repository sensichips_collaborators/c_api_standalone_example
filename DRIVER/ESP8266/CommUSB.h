#ifndef SENSIBUSUSB_H
#define SENSIBUSUSB_H

#include "StatusSENSIBUS.h"
#include "MsgSENSIBUS.h"

extern "C" {
#include "../../API/config/init.h"
}


class CommUSB {

private:
    //StatusSENSIBUS *status;
    //byte buffer[DATA_RCVD_MAXLEN];
    int newUSBSpeed = 0;

public:

    CommUSB(); //SensiBusStatus *s);

    void receiveHeader(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status);

    int receiveData(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status);

    void sendResponse(MsgSENSIBUS &SBMsg);
};

// ******************** Fine SensiBusMSGFROMUSB.h *******************
#endif

