#ifndef MSGSENSIBUS_H
#define MSGSENSIBUS_H

#include <Arduino.h>

#define DEBUG false


#define TYPE_INSTR_MASK              0x80   // 10000000 <=> EXT instruction 


// Three type of instruction: no instruction, normal instruction (read or write from SB), ext instruction
#define NO_INSTR               0xFF   // 11111111
#define NORMAL_INSTR           0x7F   // 01111111

// NORMAL_INSTR section START             // 0xxxxxxx

#define ADDRESS_MODE_MASK           96    // 01100000
#define DATA_LENGTH_MASK            31    // 00011111

#define ADDRESS_MAXLEN              6
#define DATA_RCVD_MAXLEN            32 //this value must be at least > NUM_OF_CHIP * 5
#define DATA_TOHOST_MAXLEN          32


// Detect op type to SENSIBUS (READ/WRITE) on register address
#define IS_WRITE_MASK               1    // 00000001

// Chips addresing mode
#define NoAddressValue         0x00       // 00000000
#define ShortAddressValue      0x20       // 00100000
#define FullAddressValue       0x40       // 01000000
#define LastAddressValue       0x60       // 01100000

// NORMAL_INSTR END

const int USB_SPEED[4] = {115200, 230400, 460800, 921600};
//extern HardwareSerial RedirSerial;

#define DEBUG false

static inline void writeOnDebug(byte toSend) {
  if (DEBUG) {
    Serial.write(toSend);
    //RedirSerial.write(toSend);
  }
}

// Responses
#define ECHO_RESP_OK                 0x54   // 01010100
#define ECHO_RESP_ERR                0x28   // 00101000

#define START_COMM_OK                0x80   // 10000000

class MsgSENSIBUS {
public:

    MsgSENSIBUS() {}

    MsgSENSIBUS(byte, byte data[DATA_RCVD_MAXLEN]);

    void reset();

    byte instructionType;

    byte command;

    byte registerAddress[1];

    byte chipAddressLength;
    byte chipAddress[ADDRESS_MAXLEN];

    int payloadSize; //Extended mode purpose
    byte dataRcvLength;
    byte dataRcv[DATA_RCVD_MAXLEN];


    byte dataToHostLength;
    byte dataToHost[DATA_TOHOST_MAXLEN];

};

// ******************** Fine MsgSENSIBUS.h *******************
#endif




