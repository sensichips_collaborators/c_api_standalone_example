#include "CommSENSIBUS.h"

extern "C" {
#include "../../EXT_OPERATION/ExtendedOperation.h"
}

#include "../../EXT_OPERATION/fileHandlerMCU.h"
#include "MsgSENSIBUS.h"
#include "StatusSENSIBUS.h"
#include "SENSIBUS_info.h"


// ******************** Inizio SensiBus.cpp *******************

#ifdef DELAYMICROSECONDS
//#define REDIR_DEBUG
#ifdef REDIR_DEBUG
extern HardwareSerial RedirSerial;
#endif

CommSENSIBUS::CommSENSIBUS(uint8_t p, byte newSPEED) {
  PIN_DATI = p;
  MODE_INPUT(PIN_DATI);
  WRITE_HIGH(PIN_DATI);

  TS_START_COM = 102;
  TS_START_COM_2 = 51;
  DELAY_AFTER_START = TS_START_COM_2;

  // RUN8
  SYS_CLOCK_DRIVER = SYS_CLOCK_RUN8;
  START_COM_DELAY = TS_START_COM + TS_START_COM + TS_START_COM_2; //run8
  START_COM_DELAY_2 = TS_START_COM; //run8
  LAST_SPEED = newSPEED;

  setTS(newSPEED);
}

void CommSENSIBUS::transaction(MsgSENSIBUS &SBMsg, StatusSENSIBUS &SBStatus) {
  // byte alive;
  // byte rcvByte;
  // byte errorCode = 0;
  // char msg[10];
  boolean startComResult = false;

  if (SBMsg.instructionType == NORMAL_INSTR || (SBMsg.instructionType & EXT_INSTR) == EXT_INSTR) {
    startComResult = startCom();
    //RedirSerial.println("_START_COM_");
  }

  if ((SBMsg.instructionType & EXT_INSTR) == EXT_INSTR) {
    //Serial.write(0xA4);

    //    SBMsg.dataToHost[0] = ECHO_RESP_OK;
    //    SBMsg.dataToHostLength = 1; // At least will be sent 1 byte. Could be updated by other sections
    //
    //    if (startComResult){
    //      SBMsg.dataToHost[0] = SBMsg.dataToHost[0] | START_COMM_OK;
    //    }

    if (SBMsg.instructionType == EXT_INSTR_TEST) {
      SBMsg.dataToHost[0] = ECHO_RESP_OK;
      SBMsg.dataToHostLength = 1; // At least will be sent 1 byte. Could be updated by other sections

      if (startComResult) {
        SBMsg.dataToHost[0] = SBMsg.dataToHost[0] | START_COMM_OK;
      }
      SBMsg.dataToHost[1] = FIRMWARE_VERSION;
      SBMsg.dataToHostLength = 2;

    } else if (SBMsg.instructionType == EXT_INSTR_USBSPEED) {
      //SBMsg.dataToHost[0] = 0xAA;
      //Serial.write(0xA5);

    } else if (SBMsg.instructionType == EXT_INSTR_SENSIBUSSPEED) {
      // char appo[5] = "0xFF";
      // if(SBMsg.dataRcv[0] == 0xFF){
      // 	strcpy(appo, "0xFF");
      // } else if(SBMsg.dataRcv[0] == 0x80){
      // 	strcpy(appo, "0x80");
      // } else if(SBMsg.dataRcv[0] == 0x40){
      // 	strcpy(appo, "0x40");
      // } else if(SBMsg.dataRcv[0] == 0x20){
      // 	strcpy(appo, "0x20");
      // } else if(SBMsg.dataRcv[0] == 0x10){
      // 	strcpy(appo, "0x10");
      // } else if(SBMsg.dataRcv[0] == 0x08){
      // 	strcpy(appo, "0x08");
      // }

      // set_speed(appo);
    } else if (SBMsg.instructionType == EXT_INSTR_SETEIS) {
      if (SBMsg.dataRcvLength >= 0x0D) {
        set_SPMeasurementParameterEIS(SBMsg.dataRcv);
      }
      spMeasurement[instanceID].methods->setEIS(&spMeasurement[instanceID], &paramEIS);
      SBMsg.instructionType = EXT_INSTR_SETEIS;
    } else if (SBMsg.instructionType == EXT_INSTR_SETADC) {
      if (SBMsg.dataRcvLength > 0x00) {
        set_SPMeasurementParameterADC(SBMsg.dataRcv);
      }
      spMeasurement[instanceID].methods->setADC(&spMeasurement[instanceID], &paramADC);
      SBMsg.instructionType = EXT_INSTR_SETADC;
    } else if (SBMsg.instructionType == EXT_INSTR_GETEIS) {
      spMeasurement[instanceID].methods->getSingleEIS(&spMeasurement[instanceID], extOpOutput, &paramEIS);
      SBMsg.instructionType = EXT_INSTR_GETEIS;
    } else if (SBMsg.instructionType == EXT_INSTR_CHIPLIST) {
      set_SPChipList(SBMsg.dataRcv, SBMsg.payloadSize);
      SBMsg.instructionType = EXT_INSTR_CHIPLIST;
    } else if (SBMsg.instructionType == EXT_INSTR_RESET) {
      //printf("TEST");
      softResetMCU();
      SBMsg.instructionType = EXT_INSTR_RESET;
    } else if (SBMsg.instructionType == EXT_INSTR_CLEAR_CACHE) {
      //spProtocolCache.clearCache(&spProtocolCache);
      //filterOutUnavailableChips(&spProtocol, &spCluster);
      SBMsg.instructionType = EXT_INSTR_CLEAR_CACHE;
    } else if (SBMsg.instructionType == EXT_INSTR_SET_SENSOR) {
      set_Sensor(SBMsg.dataRcv);
      //setSENSOR_5(&spMeasurementRUN5[instanceID], &spParamSENSOR, NULL);
      spMeasurement[instanceID].methods->setSENSOR(&spMeasurement[instanceID], &paramSENSOR);
      SBMsg.instructionType = EXT_INSTR_SET_SENSOR;
    } else if (SBMsg.instructionType == EXT_INSTR_GET_SENSOR) {
      spMeasurement[instanceID].methods->getSENSOR(&spMeasurement[instanceID], extOpOutput, &paramSENSOR);
      //RedirSerial.println("getSENSOR");
      SBMsg.instructionType = EXT_INSTR_GET_SENSOR;
    } else if (SBMsg.instructionType == EXT_INSTR_SET_POT) {
      set_SPMeasuramentParamenterPOT(SBMsg.dataRcv);
      //spMeasurement[instanceID].methods->setPOT(&spMeasurement[instanceID], &paramPOT);
      SBMsg.instructionType = EXT_INSTR_SET_POT;
    } else if (SBMsg.instructionType == EXT_INSTR_GET_POT) {
      //spMeasurementRUNX[instanceID].getPOT(&spMeasurementRUNX[instanceID], output1, &spParamPOT, NULL);
      SBMsg.instructionType = EXT_INSTR_GET_POT;
    } else if (SBMsg.instructionType == EXT_INSTR_GET_CLASSIFICATION) {
      get_Classification(SBMsg.dataRcv);
      SBMsg.instructionType = EXT_INSTR_GET_CLASSIFICATION;
    } else if (SBMsg.instructionType == EXT_INSTR_SET_AUX) {
      set_SPMeasurementParameterAUX(SBMsg.dataRcv, &paramAUX);
      SBMsg.instructionType = EXT_INSTR_SET_AUX;
    } else if (SBMsg.instructionType == EXT_INSTR_SAVE_FILE) {
      //writeFile(SBMsg.dataRcv);
      SBMsg.instructionType = EXT_INSTR_SAVE_FILE;
    } else if (SBMsg.instructionType == EXT_INSTR_READ_FILE) {
      readFile();
      SBMsg.instructionType = EXT_INSTR_READ_FILE;
    } else if (SBMsg.instructionType == EXT_INSTR_DELETE_FILE) {
      deleteFile();
      SBMsg.instructionType = EXT_INSTR_DELETE_FILE;
    } else if (SBMsg.instructionType == EXT_INSTR_FORMAT_FILE) {
      formatFile();
      SBMsg.instructionType = EXT_INSTR_FORMAT_FILE;
    }

  } else if ((SBMsg.instructionType & NORMAL_INSTR) == NORMAL_INSTR) { //NORMAL INSTRUCTION
    if (startComResult) {
      // Serial.println("startCom() OK!!!");
      // Send address
      writeOnDebug(0xC0);
      //  Serial.print("Address size: ");
      // Serial.println(SBMsg.chipAddressLength);
      for (int i = 0; i < SBMsg.chipAddressLength; i++) {
        //writeOnDebug(SBMsg.chipAddress[i]);
        write(SBMsg.chipAddress[i]);
      }

      // Send register
      write(SBMsg.registerAddress[0]);
      // Send data
      SBMsg.dataToHostLength = SBMsg.dataRcvLength;
      //writeOnDebug(SBMsg.dataRcvLength);
      for (int i = 0; i < SBMsg.dataRcvLength; i++) {
        if (SBStatus.isRead) {
          SBMsg.dataToHost[i] = read();
        } else {
          SBMsg.dataToHost[i] = SBMsg.dataRcv[i];
          //writeOnDebug(SBMsg.dataRcv[i]);
          write(SBMsg.dataRcv[i]);
        }
      }

      // Detect SENSIBUS_SET speed change
      if (SBMsg.registerAddress[0] == SENSIBUS_SPEED && !SBStatus.isRead) {
        setTS(SBMsg.dataRcv[0]);
        LAST_SPEED = SBMsg.dataRcv[0];
      } else if (SBMsg.registerAddress[0] == 0xF8 && SBMsg.dataRcv[0] == 0x00) { //Detect USER_EFUSE S 0X00
        this->tryDetectRUN();
      } else if (SBMsg.registerAddress[0] == 0x00 && (SBMsg.dataRcv[0] == 0x42 || SBMsg.dataRcv[0] ==
                                                                                  0x40)) { //Detect Soft_reset WRITE COMMAND S  {0X42, 0x40}
        this->tryDetectRUN();
      } else if (SBMsg.registerAddress[0] == SENSIBUS_ADDRESS && !SBStatus.isRead) {
        if (SBMsg.dataRcv[0] == SENSIBUS_VALUE_FULL_ADDRESS) {
          spMeasurement[instanceID].spProtocol->addressingMode = FULL_ADDRESS;
          spProtocol.addressingMode = FULL_ADDRESS;

        } else if (SBMsg.dataRcv[0] == SENSIBUS_VALUE_SHORT_ADDRESS) {
          spMeasurement[instanceID].spProtocol->addressingMode = SHORT_ADDRESS;
          spProtocol.addressingMode = SHORT_ADDRESS;

        } else if (SBMsg.dataRcv[0] == SENSIBUS_VALUE_NO_ADDRESS) {
          spMeasurement[instanceID].spProtocol->addressingMode = NO_ADDRESS;
          spProtocol.addressingMode = NO_ADDRESS;
        }
      }
    } else {
      //print("startCom() failed!!!\n");
      // Serial.println("startCom() failed!!!");
    }

  }

}


bool CommSENSIBUS::startCom() {
  MODE_OUTPUT(PIN_DATI);               // Setto il DATI come OUT
  noInterrupts();                 // Disabilito gli Interrupts
  WRITE_LOW(PIN_DATI);                 // Piloto il segnale BASSO
  delayMicroseconds(START_COM_DELAY);   // Aspetto 1 volta e mezza TS
  interrupts();                   // riabilito gli interrupts
  MODE_INPUT(PIN_DATI);                // Rilascio la linea
  delayMicroseconds(
          START_COM_DELAY_2);        // Aspetto TS/2 prima di campionare una risposta ALIVE (verificato con oscilloscopio)

  // Return ALIVE:
  bool output = !READ(PIN_DATI);

  delayMicroseconds(DELAY_AFTER_START);        // Wait after startComm as in BusPirate
  //delay(1);
  // Return ALIVE:
  return output;
}


/*
byte CommSENSIBUS::read() {
	byte mask;            // la mia bitmask di partenza: 1000 0000
	byte output=0;    // userò questo byte per leggere
	byte bitLetto = 0;

	noInterrupts();                               // disattivo gli interrupt

	for (mask = 0x80; mask>0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB
		MODE_OUTPUT(PIN_DATI);                             // Preparo la linea in OUT

		WRITE_LOW(PIN_DATI);                               // Abbasso la linea per 1/4 di TS
		delayMicroseconds(TS_4);                      // mantengo la linea BASSA per 1/4 di TS

		MODE_INPUT(PIN_DATI);
		//WRITE_HIGH(PIN_DATI);

		delayMicroseconds(TS_2_Read);
		//MODE_INPUT(PIN_DATI);
		bitLetto = READ(PIN_DATI);
		delayMicroseconds(TS_4);

		output <<= 1;
		output += bitLetto;

		//delayMicroseconds(TS_4);                      // Mantegno il segnale stabile per 1/4 di TS

		delayMicroseconds(DELAY_BETWEEN_BIT);
	}
	interrupts();                                 // Riattivo gli interrupt

	delayMicroseconds(DELAY_BETWEEN_BYTE);        // Wait after a byte as in BusPirate

	return output;
}
*/


byte CommSENSIBUS::read() {
  byte mask;            // la mia bitmask di partenza: 1000 0000
  byte output = 0;    // userò questo byte per leggere
  byte bitLetto = 0;
  //portMUX_TYPE mux;
  //mux.count = 1;
  //mux.owner = CORE_ID_APP;

  // noInterrupts();                               // disattivo gli interrupt
  //DISABLE_INTERRUPTS();
  //vTaskEnterCritical(&mux);
  portDISABLE_INTERRUPTS();

  for (mask = 0x80; mask > 0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB
    MODE_OUTPUT(PIN_DATI);                             // Preparo la linea in OUT

    WRITE_LOW(PIN_DATI);                               // Abbasso la linea per 1/4 di TS
    delayMicroseconds(TS_4);                      // mantengo la linea BASSA per 1/4 di TS

    /* TEST OPEN DRAIN SENZA  INPUT MODE*/
    /* rilascio la linea se in output_open_drain,
    il cambio di "pinMode" introduce un ritardo di un ordine di grandezza */
    //WRITE_HIGH(DATI);
    MODE_INPUT(PIN_DATI);
    //WRITE_HIGH(PIN_DATI);

    delayMicroseconds(TS_2_Read);
    //MODE_INPUT(PIN_DATI);
    bitLetto = READ(PIN_DATI);
    delayMicroseconds(TS_4);

    output <<= 1;
    output += bitLetto;

    //delayMicroseconds(TS_4);                      // Mantegno il segnale stabile per 1/4 di TS

    delayMicroseconds(DELAY_BETWEEN_BIT);
  }
  //interrupts();                                 // Riattivo gli interrupt
  //ENABLE_INTERRUPTS();
  //vTaskExitCritical(&mux);
  portENABLE_INTERRUPTS();

  delayMicroseconds(DELAY_BETWEEN_BYTE);        // Wait after a byte as in BusPirate

  return output;
}


int CommSENSIBUS::getTS_START_COM() {
  return TS_START_COM;
}


void CommSENSIBUS::write(byte val) {
  byte mask;                                      // la mia bitmask di partenza: 1000 0000
  //portMUX_TYPE mux;
  // mux.count = 1;
  //mux.owner = CORE_ID_APP;

  for (mask = 0x80; mask > 0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB

    MODE_OUTPUT(PIN_DATI);                             // Preparo la linea in OUT
    //noInterrupts();                               // disattivo gli interrupt
    //DISABLE_INTERRUPTS();
    // vTaskEnterCritical(&mux);
    portDISABLE_INTERRUPTS();

    WRITE_LOW(PIN_DATI);                               // Abbasso la linea per 1/4 di TS
    delayMicroseconds(TS_4);                      // mantengo la linea BASSA per 1/4 di TS

    if (val & mask) {                              // se bitwise AND resolves to true
      WRITE_HIGH(PIN_DATI);                            // send 1
    } else {                                         // SE bitwise AND resolves to false
      WRITE_LOW(PIN_DATI);                             // send 0
    }

    delayMicroseconds(TS_2_Write + TS_4);                      // Mantegno il segnale stabile per 1/4 di TS

    //interrupts();                                 // Riattivo gli interrupt
    //ENABLE_INTERRUPTS();
    //vTaskExitCritical(&mux);
    portENABLE_INTERRUPTS();

    MODE_INPUT(
            PIN_DATI);                              // Rilascio la linea (La R di pull-up dovrebbe portare la linea a Vdd)

    delayMicroseconds(DELAY_BETWEEN_BIT);                      // Attendo l'ultimo quarto di TS
  }
  delayMicroseconds(DELAY_BETWEEN_BYTE);        // Wait after a byte as in BusPirate

}

/*
void CommSENSIBUS::write(byte val) {
	byte mask;                                      // la mia bitmask di partenza: 1000 0000

	for (mask = 0x80; mask>0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB

		MODE_OUTPUT(PIN_DATI);                             // Preparo la linea in OUT
		noInterrupts();                               // disattivo gli interrupt

		WRITE_LOW(PIN_DATI);                               // Abbasso la linea per 1/4 di TS
		delayMicroseconds(TS_4);                      // mantengo la linea BASSA per 1/4 di TS

		if (val & mask){                              // se bitwise AND resolves to true
			WRITE_HIGH(PIN_DATI);                            // send 1
		}
		else{                                         // SE bitwise AND resolves to false
			WRITE_LOW(PIN_DATI);                             // send 0
		}

		delayMicroseconds(TS_2_Write + TS_4);                      // Mantegno il segnale stabile per 1/4 di TS

		interrupts();                                 // Riattivo gli interrupt

		MODE_INPUT(PIN_DATI);                              // Rilascio la linea (La R di pull-up dovrebbe portare la linea a Vdd)

		delayMicroseconds(DELAY_BETWEEN_BIT);                      // Attendo l'ultimo quarto di TS
	}
	delayMicroseconds(DELAY_BETWEEN_BYTE);        // Wait after a byte as in BusPirate

}
*/


void CommSENSIBUS::setTS(byte newSPEED) {

  // TS in micro seconds
  TS = (int) (((float) ((int) newSPEED * 4 * 1000000)) / (float) SYS_CLOCK_DRIVER);
  //snprintf (msg, 75, "TS %ld", TS);
  //Serial.println(msg);

  TS_2_Write = (TS / 2) - 1;
  TS_2_Read = (TS / 2);
  TS_4 = TS / 4;
  TS_8 = TS / 8;
  TS_16 = TS / 16;
  DELAY_BETWEEN_BIT = TS / 4;
  DELAY_BETWEEN_BYTE = TS;

  //TS_START_COM = newSPEED;
}

bool CommSENSIBUS::tryDetectRUN() {

  /*RUN8 test*/
  SYS_CLOCK_DRIVER = SYS_CLOCK_RUN8;
  START_COM_DELAY = TS_START_COM + TS_START_COM + TS_START_COM_2; //run8
  START_COM_DELAY_2 = TS_START_COM+5;
  setTS(LAST_SPEED);

  if (startCom())  /* Detected RUN8  */
  {
#ifdef REDIR_DEBUG
    RedirSerial.println("Detected run8 speeds");
#endif
    return true;
  }
  /*RUN7 test*/
  SYS_CLOCK_DRIVER = SYS_CLOCK_RUN7;
  START_COM_DELAY = TS_START_COM + TS_START_COM_2 + TS_START_COM_2;
  START_COM_DELAY_2 = TS_START_COM_2+5;
  setTS(LAST_SPEED);
  if (startCom())   /* Detected RUN7  */
  {
#ifdef REDIR_DEBUG
    RedirSerial.println("Detected run7 speeds");
#endif
    return true;
  }

  /* Nothing detected default to RUN7*/
#ifdef REDIR_DEBUG
  RedirSerial.println("Nothing detected default to RUN7");
#endif
  return false;
}

#else

#define CkT_LOWEST_SPEED 16310

static inline unsigned get_ccount(void){
  unsigned r;
  asm volatile ("rsr %0, ccount" : "=r"(r));
  return r;
}

static inline void waitForTick(int counterLimit){

  int startTime = get_ccount();
  while((get_ccount() - startTime) <= counterLimit);
}



CommSENSIBUS::CommSENSIBUS (uint8_t p, byte newSPEED) {
  PIN_DATI = p;
  MODE_OUTPUT(PIN_DATI);
  WRITE_HIGH(PIN_DATI);
  //pinMode(PIN_DATI,OUTPUT_OPEN_DRAIN);
  //digitalWrite(PIN_DATI, HIGH);

  CkT_START_COM = CkT_LOWEST_SPEED;
  CkT_START_COM_2 = CkT_LOWEST_SPEED/2;
  CkT_START_COM_4 = CkT_LOWEST_SPEED/4;
  CkT_START_COM_8 = CkT_LOWEST_SPEED/8;

  newSPEED = adaptSPEED(newSPEED);
  setSPEED(newSPEED);

  CkT_RiseTime = 1000;  // 6us with internal pull-up
  TkZero32x    = 140;
}

void CommSENSIBUS::transaction(MsgSENSIBUS &SBMsg, StatusSENSIBUS &SBStatus){
  byte alive;
  byte rcvByte;
  byte errorCode = 0;
  char msg[10];
  boolean startComResult = false;

  if (SBMsg.instructionType == NORMAL_INSTR || (SBMsg.instructionType & EXT_INSTR) == EXT_INSTR){
    startComResult = startCom();
  }

  if ((SBMsg.instructionType & EXT_INSTR) == EXT_INSTR){
    //Serial.write(0xA4);

    SBMsg.dataToHost[0] = ECHO_RESP_OK;
    SBMsg.dataToHostLength = 1; // At least will be sent 1 byte. Could be updated by other sections

    if (startComResult){
      SBMsg.dataToHost[0] = SBMsg.dataToHost[0] | START_COMM_OK;
    }

    if ((SBMsg.instructionType & EXT_INSTR_TEST_MASK) == EXT_INSTR_TEST_MASK){
      SBMsg.dataToHost[1] = FIRMWARE_VERSION;
      SBMsg.dataToHostLength = 2;

    } else if ((SBMsg.instructionType & EXT_INSTR_USBSPEED_MASK) == EXT_INSTR_USBSPEED_MASK){
      //SBMsg.dataToHost[0] = 0xAA;
      //Serial.write(0xA5);
    } else if ((SBMsg.instructionType & EXT_INSTR_ELAPSED_START_COM) == EXT_INSTR_ELAPSED_START_COM){
      // Send elapsed time in microseconds
      float elapsed = elapsedStartComResponse();
      //Serial.println(elapsed);
      byte *b = (byte *)&elapsed;
      for(int i = 0; i < 4; i++){
        SBMsg.dataToHost[i + 1] = b[i];
      }
      SBMsg.dataToHostLength = 5;

    } else if ((SBMsg.instructionType & EXT_INSTR_TEST_SPEED32X_COM) == EXT_INSTR_TEST_SPEED32X_COM){
      setTkZero32x(SBMsg.dataRcv[0]);
      SBMsg.dataToHost[1] = SBMsg.dataRcv[0];
      SBMsg.dataToHostLength = 2;
    }


  } else if ((SBMsg.instructionType & NORMAL_INSTR) == NORMAL_INSTR) {
    if (startComResult){
      // Send address
      writeOnDebug(0xC0);
      for(int i = 0; i < SBMsg.chipAddressLength; i++){
        writeOnDebug(SBMsg.chipAddress[i]);
        write(SBMsg.chipAddress[i]);
      }

      // Send register
      write(SBMsg.registerAddress[0]);
      // Send data
      SBMsg.dataToHostLength = SBMsg.dataRcvLength;
      writeOnDebug(SBMsg.dataRcvLength);

      // Detect SENSIBUS_SET speed change and adapt to availables
      if (SBMsg.registerAddress[0] == SENSIBUS_SPEED && !SBStatus.isWrite){
        SBMsg.dataRcv[0] = adaptSPEED(SBMsg.dataRcv[0]);
      }


      for(int i = 0; i < SBMsg.dataRcvLength; i++){
        if (SBStatus.isWrite){
          SBMsg.dataToHost[i] = read();

        } else {
          SBMsg.dataToHost[i] = SBMsg.dataRcv[i];
          writeOnDebug(SBMsg.dataRcv[i]);
          write(SBMsg.dataRcv[i]);
        }
      }

      // Detect SENSIBUS_SET speed change and update MCU
      if (SBMsg.registerAddress[0] == SENSIBUS_SPEED && !SBStatus.isWrite){
        setSPEED(SBMsg.dataRcv[0]);
      }
    }

  }

}




bool CommSENSIBUS::startCom() {
  //MODE_OUTPUT(pin);                                                   // Setto il pin come OUT
  noInterrupts();                                                    // Disabilito gli Interrupts
  WRITE_LOW(PIN_DATI);
  //digitalWrite(PIN_DATI, LOW);                                               // Piloto il segnale BASSO
  waitForTick(CkT_START_COM   + CkT_START_COM_2 + CkT_START_COM_8);     // Aspetto 1 volta e mezza TS
  interrupts();                                                         // riabilito gli interrupts
  WRITE_HIGH(PIN_DATI);
  //digitalWrite(PIN_DATI, HIGH);                                              // Rilascio la linea
  waitForTick(CkT_START_COM_2);                                         // Aspetto TS/2 prima di campionare una risposta ALIVE

  // Return ALIVE:
  bool output = !READ(PIN_DATI);
  while(!READ(PIN_DATI));
  waitForTick(CkT_RiseTime);

  return output;
}


byte CommSENSIBUS::read() {
  byte mask;            // la mia bitmask di partenza: 1000 0000
  byte output=0;    // userò questo byte per leggere
  byte bitLetto = 0;
  unsigned long counter = 0;
  unsigned long totalCounter = 0;
  int startTime = 0;
  int endTime = 0;
  float singleCycleTime = 0.00815; //microseconds
  float timeOutBias = 0.460;
  float CkT_time = 0.00625;         //microseconds
  float timeOut = CkT * CkT_time + timeOutBias;
  int counterThreshold = timeOut / (singleCycleTime * 2);


  for (mask = 0x80; mask>0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB
    counter = 0;
    totalCounter = 0;
    noInterrupts();                               // disattivo gli interrupt
    startTime = micros();
    WRITE_LOW(PIN_DATI);
    //digitalWrite(PIN_DATI, LOW);                               // Abbasso la linea per 1/4 di TS
    waitForTick(CkT_4);                      // mantengo la linea BASSA per 1/4 di TS

    WRITE_HIGH(PIN_DATI);
    //digitalWrite(PIN_DATI, HIGH);

    waitForTick(CkT_2);
    endTime = startTime;

    while((endTime - startTime) < timeOut){
      if (!READ(PIN_DATI)){
        counter++;
      }
      totalCounter++;
      if (totalCounter > 100){
        endTime = micros();
        totalCounter = 0;
      }
      writeOnDebug(counter);
    }
    interrupts();                                 // Riattivo gli interrupt

    bitLetto = counter > 0 ? 0 : 1;
    output <<= 1;
    output += bitLetto;

    waitForTick(CkT_RiseTime);

  }

  return output;
}



void CommSENSIBUS::write(byte val) {
  byte mask;                                      // la mia bitmask di partenza: 1000 0000

  noInterrupts();                               // disattivo gli interrupt
  for (mask = 0x80; mask>0; mask >>= 1) {         // itero attravero la bitmask partendo da MSB

    WRITE_LOW(PIN_DATI);
    //digitalWrite(PIN_DATI, LOW);                               // Abbasso la linea per 1/4 di TS
    waitForTick(CkT_4);

    if (val & mask){                              // se bitwise AND resolves to true
      WRITE_HIGH(PIN_DATI);
      //digitalWrite(PIN_DATI, HIGH);
    }

    waitForTick(CkT_3_4);

    //waitForTick(CkT_4);
    if (!(val & mask)){                              // se bitwise AND resolves to true
      WRITE_HIGH(PIN_DATI);
      //digitalWrite(PIN_DATI, HIGH);
    }

    waitForTick(CkT_RiseTime);

    while(!READ(PIN_DATI));

    waitForTick(CkT_RiseTime);

  }
  interrupts();                                 // Riattivo gli interrupt

}

byte CommSENSIBUS::adaptSPEED(byte newSPEED){
  byte output = newSPEED;

  if (newSPEED > 0x80 && newSPEED <= 0xFF){
    //1X
    output = 0xFF;
  } else if (newSPEED > 0x40 && newSPEED <= 0x80){
    // 2X
    output = 0x80;
  } else if (newSPEED > 0x20 && newSPEED <= 0x40){
    // 4x
    output = 0x40;
  } else if (newSPEED > 0x10 && newSPEED <= 0x20){
    // 8x
    output = 0x20;
  } else if (newSPEED > 0x08 && newSPEED <= 0x10){
    // 16x
    output = 0x10;
  } else if (newSPEED <= 0x08){
    // 32x
    output = 0x08;
  }

  return output;


}

void  CommSENSIBUS::setTkZero32x(int i){
  TkZero32x = i;
}




void CommSENSIBUS::setSPEED(byte newSPEED){
  CkT = CkT_LOWEST_SPEED;

  if (newSPEED > 0x80 && newSPEED <= 0xFF){
    //1X
    CkT = 16350;
    CkT_3_4 = 12254;
    CkT_2 = 8158;
    CkT_2_read = CkT_2;
    CkT_4 = 4062;
  } else if (newSPEED > 0x40 && newSPEED <= 0x80){
    // 2X
    CkT = 8222;
    CkT_3_4 = 6158;
    CkT_2 = 4094;
    CkT_2_read = CkT_2;
    CkT_4 = 2030;
  } else if (newSPEED > 0x20 && newSPEED <= 0x40){
    // 4x
    CkT = 4126;
    CkT_3_4 = 3086;
    CkT_2 = 2046;
    CkT_2_read = CkT_2;
    CkT_4 = 1006;
  } else if (newSPEED > 0x10 && newSPEED <= 0x20){
    // 8x
    CkT = 2078;
    CkT_3_4 = 1550;
    CkT_2 = 1022;
    CkT_2_read = CkT_2;
    CkT_4 = 494;
  } else if (newSPEED > 0x08 && newSPEED <= 0x10){
    // 16x
    CkT = 1054;
    CkT_3_4 = 782;
    CkT_2 = 510;
    CkT_2_read = CkT_2;
    CkT_4 = 238;
  } else if (newSPEED <= 0x08){
    // 32x
    CkT = 542;
    CkT_3_4 = 398;
    CkT_2 = 254;
    CkT_2_read = 230;
    CkT_4 = TkZero32x;
  }

  return;
}


float CommSENSIBUS::elapsedStartComResponse(){
  // Start the timer
  unsigned long counter = 0;
  unsigned long startTime = 0;
  unsigned long endTime = 0;
  float elapsed = 0;
  unsigned long repetition = 100;

  for(int i = 0; i < repetition; i++){
    //MODE_OUTPUT(pin);               // Setto il pin come OUT
    noInterrupts();                 // Disabilito gli Interrupts

    WRITE_LOW(PIN_DATI);
    //digitalWrite(PIN_DATI, LOW);                 // Piloto il segnale BASSO
    waitForTick(CkT_START_COM + CkT_START_COM_2 + CkT_START_COM_8);   // Aspetto 1 volta e mezza TS
    interrupts();                   // riabilito gli interrupts
    WRITE_HIGH(PIN_DATI);
    //digitalWrite(PIN_DATI, HIGH);                // Rilascio la linea

    // Wait for signal to rise
    while(!READ(PIN_DATI));

    // Wait for start communication from chips
    while(READ(PIN_DATI));


    startTime = micros();
    while(!READ(PIN_DATI)){
      counter++;
      endTime = micros();
    }

    // Stop the timer
    endTime = micros();
    elapsed = elapsed + (endTime - startTime);

    //Serial.println("Counter: %d", (int)counter);
    // Start the timer
    /*
    counter = 0;
    startTime = micros();
    while(counter++ < 485){
      digitalRead(pin);
    }
    // Stop the timer
    endTime = micros();
    elapsed = endTime - startTime;
     */
    waitForTick(CkT_RiseTime);
  }

  elapsed = elapsed/repetition;

  //Serial.println(elapsed);
  //Serial.println(counter);

  return elapsed;

}

#endif




// ******************** Fine CommSENSIBUS.cpp *******************

