/*
 * initDriver.h
 *
 *  Created on: 23 mag 2018
 *      Author: Luca
 */

#ifndef ESP8266_SENDDATA_H_
#define ESP8266_SENDDATA_H_


#include "../../API/util/types.h"


void sendData(uint8_t header, uint8_t command, byte *addressByte, uint8_t dimAddress,
              byte *data, uint8_t dimData, byte *out);


#endif /* ESP8266_SENDDATA_H_ */
