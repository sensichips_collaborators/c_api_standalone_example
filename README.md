## Introduction 
This repository contains an example of using the Sensichips API in C. The example shows how to configure data structures and how to call the main functions to acquire data from direct sensors and EIS sensors.

The project is intended to be used on microcontrollers based on the **Arduino framework**. Specifically, the API is distributed through a precompiled library for MCUs belonging to the **ESP32 family** (both Xtensa and RISC-V architectures). For other architectures, the necessary library must be requested.


## Project configuration

Any project using the C API requires some preliminary attention:
* You need to properly configure the GPIOs used to communicate with the SENSIPLUS chip through the SENSIBUS communication protocol. Specifically, by properly configuring the directives found in the _CommSENSIBUS.h_ file in the _DRIVER/ESP8266_ folder. The project is preconfigured to work with the XIAO ESP32C3 microcontroller, which is the one most commonly distributed. If this is the case, you probably do not need to edit this file. The communication protocol used is a single line protocol that requires a pull-up on the DATA line (usually 10kOhm).
  The following is an excerpt from the file _DRIVER/ESP8266/CommSENSIBUS.h_
    ```c
        ...
    #ifdef ESP8266
    #define   DATI          D3
    #define   SENSIBUS_VCC  D2
    
    #elif defined(ARDUINO_XIAO_ESP32C3) || defined(ARDUINO_XIAO_ESP32S3)
    #define   SENSIBUS_VCC  D6
    #define   DATI          D10
    #define   DONTCARE      D9
    #define   LED_BUILTIN   D7
    
    #else // Suitable for ESP32
    #define   SENSIBUS_VCC  32
    #define   DATI          25
    #define   DONTCARE      26
    #define   LED_BUILTIN   4
    #endif
        ...
  ```

* The firmware will self-configure depending on the sensor hardware revision used (RUN8, RUN9). Depending on the hardware available, it is necessary to enable the appropriate #define within the API/config/config.h file.
    ```c
    #ifndef CONFIG_H_
    #define CONFIG_H_

    /* UNCOMMENT THE RIGHT #define for the hardware used */
    //#define RUN8
    #define RUN9
        ...
    ```


* In order to function properly, the API system requires some additional configuration files to be loaded into the flash memory of the target microcontroller. Currently, the system provides the use of a SPIFFS partition where these files reside.You can create and load this data partition in several ways. The configuration files in question reside in the _data_ folder.


* The API system is provided through the use of a precompiled library located in _API/lib_ and named _libapi.a_. So it is necessary to "remind" the compiler of its existence.


---
### License
© 2023 Sensichips Srl

_michele.vitelli@sensichips.com_