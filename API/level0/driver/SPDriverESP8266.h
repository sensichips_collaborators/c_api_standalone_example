/*
 * SPDriverESP8266.h
 *
 *  Created on: 22 mag 2018
 *      Author: Luca Gerevini
 */

#ifndef LEVEL0_DRIVER_SPDRIVERESP8266_H_
#define LEVEL0_DRIVER_SPDRIVERESP8266_H_

#include "../../util/types.h"

#if defined (ARDUINO)

//#include "DRIVER/SendData.h"

#endif

typedef struct {
    //dim : size of buffer (sequence and dataread are buffer of uint8_t)
    void (*mcuDriver)(uint8_t, uint8_t, byte *, uint8_t, byte *, uint8_t, byte *);
} SPDriverESP8266;

void sendData(uint8_t header, uint8_t command, byte *addressByte, uint8_t dimAddress,
              byte *data, uint8_t dimData, byte *out);

#endif /* LEVEL0_DRIVER_SPDRIVERESP8266_H_ */
