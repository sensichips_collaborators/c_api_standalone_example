/*
 * SPDriver.h
 *
 *  Created on: 30/dic/2016
 *      Author: Marco
 */

#ifndef SPDRIVER_H_
#define SPDRIVER_H_
//// These values will give a baud rate of approx. 2.00 Mbps at 32 MHz system clock
//#define SPI_BAUD_M  216
//#define SPI_BAUD_E  12

#include "../../util/types.h"
#include "../../util/memoryHandler.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "SPDriverESP8266.h"

/*!
 * pfnDriver: struct which contains pointers to the function which a SPDriver needs to use.
 */
typedef struct{
	SPDriverESP8266* spDriverESP8266;
	//dim : size of buffer (sequence and dataread are buffer of uint8_t)
	void (*write)(uint8_t* buf, short dim);
	int (*read)(uint8_t* buf, short dim);
}SPDriver;

void SPDriver_Write(uint8_t* sequence, short dim);
int SPDriver_Read(uint8_t* dataRead,short dim);

#endif /* SPDRIVER_H_ */
