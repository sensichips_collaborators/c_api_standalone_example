#ifndef SPINSTRUCTIONCACHE_H_
#define SPINSTRUCTIONCACHE_H_

#include "../../chip/SPRegister.h"
#include "../../chip/SPRegisterStatus.h"
#include "../../../util/types.h"

typedef struct
{
    SPRegister *spRegister;
    byte values[2];
} CacheMap;

typedef struct struct_SPInstructionCache SPInstructionCache;

typedef struct
{
    void (*addInstruction)(SPInstructionCache *this_c, SPRegister *spRegister);
    bool (*checkInstruction)(SPInstructionCache *this_c, SPRegister *spRegister);
    void (*clearCache)(SPInstructionCache *this_c);
} SPInstructionCacheMethods;

struct struct_SPInstructionCache
{
    CacheMap instructionsSent[WRITABLE_REGISTERS - 2];
    uint8_t N;
    SPInstructionCacheMethods *methods;
};

void initSPInstructionCache(SPInstructionCache *);

#endif /* SPINSTRUCTIONCACHE_H_ */
