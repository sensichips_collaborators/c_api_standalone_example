// /*
//  * SPProtocolCache.h
//  *
//  *  Created on: May 18, 2021
//  *      Author:
//  */

// #ifndef API_LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLCACHE_H_
// #define API_LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLCACHE_H_

// #include "../../../util/stringHandler.h"
// #include "../../../util/printMCU.h"


// typedef struct {
// 	char registerName[REGISTERNAME_MAXLEN];
// 	uint8_t registerHex;
// 	char MSB[5]; /* direttamente nella forma 0x12\0, per evitare continui substring*/
// 	char LSB[5];
// }SPCacheRegisterBytes;

// typedef struct SPProtocolCache SPProtocolCache;

// struct SPProtocolCache{

// 	unsigned int HIT;
// 	unsigned int MISS;
// 	SPCacheRegisterBytes *cacheInstruction;

// 	int (*checkInstruction)(SPProtocolCache*,  SPDecoderGenericInstruction*);
// 	void (*clearCache)(SPProtocolCache*);

// };


// void initSPProtocolCache(SPProtocolCache* this_c,SPCacheRegisterBytes* cacheInstruction );

// #endif /* API_LEVEL1_PROTOCOLS_UTIL_SPPROTOCOLCACHE_H_ */
