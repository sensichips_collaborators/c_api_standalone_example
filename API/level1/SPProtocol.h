/**
 * @ingroup level1
 *
 * @file SPProtocol.h
 * @brief SPProtocol control the interaction with the bridge connected to the SENSIPLUS.
 */


#ifndef LEVEL1_SPPROTOCOL_H_
#define LEVEL1_SPPROTOCOL_H_

#include "../config/config.h"
#include "../util/types.h"
#include "chip/SPCluster.h"
#include "chip/SPChip.h"
#include "../level0/driver/SPDriver.h"
#include "../level2/Parameters/SendInstructionInOut.h"

#define PROTOCOL_SPI 0
#define PROTOCOL_I2C 1
#define PROTOCOL_SENSIBUS 2
#define PROTOCOL_BUSPIRATE 3

#define NO_ADDRESS 0
#define SHORT_ADDRESS 1
#define FULL_ADDRESS 2

#define OUTPUT_DRIVE_4      0
#define OUTPUT_DRIVE_8      1
#define OUTPUT_DRIVE_12     2
#define OUTPUT_DRIVE_16     3

static const char LOG_MESSAGE_PROTOCOL[] = "SPProtocol";

typedef struct struct_SPProtocol SPProtocol;

typedef struct {
    SendInstructionInOut (*sendInstruction)(SPProtocol *this_c, SPRegister *spRegister, SPCluster *cluster);

    void (*activateMulticast)(SPProtocol *this_c);

    void (*filterOutUnavailableChips)(SPProtocol *this_c);

    void (*changeRegistersBank)(SPProtocol *this_c, SPCluster *spCluster, byte spRegisterBank);

    /**
     * @brief Configure the addressing type (used for communication) and GPIO output drive current
     * by sending the correct instruction.
     * @param this_c
     * @param newAddressingType
     * @param outputDriveMode
     */
    void (*setAddressingAndOutputDrive)(SPProtocol *this_c, byte newAddressingType, byte outputDriveMode);

} SPProtocolMethods;

struct struct_SPProtocol {
    void (*spSend)(SPDriver *spDriver, byte *byteReceived, SPRegister *r, SPChip *spChip, uint8_t addressingMode);

    long milllisecs;

    SPCluster *spCluster;
    SPDriver *spDriver;
    int addressingMode;
    bool isCacheActivated;
    bool multicastActivated;

    SPProtocolMethods *methods;
};

void initSPProtocol(SPProtocol *this_c);

#endif /* LEVEL1_SPPROTOCOL_H_ */
