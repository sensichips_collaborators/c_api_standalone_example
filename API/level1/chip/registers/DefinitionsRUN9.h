/*
 * DefinitionsRUN9.h
 *
 *  Created on: Dec 1, 2022
 *      Author: zen
 */

#ifndef API_LEVEL1_CHIP_REGISTERS_DEFINITIONS_RUN9_H_
#define API_LEVEL1_CHIP_REGISTERS_DEFINITIONS_RUN9_H_

/*
 *  RUN9 Registers and fields definitions
 */

#if defined(SENSIDRIVE)
#define REGISTERS_SIZE 55
#define FIELDS_SIZE 263
#define WRITABLE_REGISTERS 47
#else
#define REGISTERS_SIZE 45
#define FIELDS_SIZE 235
#define WRITABLE_REGISTERS 37
#endif

/*  BANK1 Registers */
#define STATUS 0
#define COMMAND 1
#define INT_CONFIG 2
#define ANA_CONFIG 3
#define DIG_CONFIG 4
#define CHA_DIVIDER 5
#define CHA_CONFIG 6
#define CHA_LIMIT 7
#define CHA_CONDIT 8
#define CHA_PSHIFT 9
#define CHA_FILTER 10
#define CHA_SELECT 11
#define US_BURST 12
#define US_TRANSMIT 13
#define RXCOUNTERL 14
#define US_PROBE 15
#define RXCOUNTERH 16
#define US_RECEIVE 17
#define DSS_DIVIDER 18
#define DDS_FIFO 19
#define DDS_CONFIG 20
#define DAS_CONFIG 21
#define DSS_SELECT 22
#define TIMERL 23
#define TIMER_DIVL 24
#define TIMERH 25
#define TIMER_DIVH 26
#define AUX_CONFIG 27
#define PPR_COUNT 28
#define PPR_THRES 29
#define PPR_SELECT 30
#define CHA_FIFOL 31
#define FIFO_CONFIG 32
#define CHA_FIFOH 33
#define SENSIBUS_SET 34
#define DEVICE_ID0 35
#define DEVICE_ID1 36
#define DEVICE_ID2 37
#define USER_EFUSE 38

/* BANK2 Registers */
#define GPIO 39
#define PORT_FLAG1 40
#define PORT_FLAG2 41
#define PORT_FLAG3 42
#define PORT_DELAY 43
#define PORT_SET 44

/* SENSIDRIVE Registers */
#if defined(SENSIDRIVE)
#define MUX_CONFIG 45
#define DRV_SELECT 46
#define DRV_AMP0 47
#define DRV_AMP1 48
#define DRV_AMP2 49
#define DRV_AMP3 50
#define DRV_AMP4 51
#define DRV_AMP5 52
#define DRV_AMP6 53
#define DRV_QUENCH 54
#endif



/* Fields */

/* STATUS Register Fields */
#define RLK 0
#define VSI 1
#define HAI 2
#define TII 3
#define L1I 4
#define E1I 5
#define F1I 6
#define W1I 7
#define INT 8

/* COMMAND Register Fields */
#define COMMAND_FIELD 9

/* INT_CONFIG Register Fields */
#define PPE_i 10
#define PKE_i 11
#define COE_i 12
#define TTE_i 13
#define UAE_i 14
#define DC_i 15
#define SYE_i 16
#define RDE_i 17
#define VSE_i 18
#define HAE_i 19
#define TIE_i 20
#define L1E_i 21
#define E1E_i 22
#define F1E_i 23
#define W1E_i 24
#define INE_i 25

/* ANA_CONFIG Register Fields */
#define OSS 26
#define IFS 27
#define VSE 28
#define VR 29
#define AOE 30
#define HCL 31
#define PDE 32
#define PME 33
#define DAE 34
#define ADE 35
#define IAE 36
#define CME 37
#define BGE 38
#define OSE 39

/* DIG_CONFIG Register Fields */
#define SPI 40
#define SPA 41
#define B12 42
#define SRA 43
#define MCA 44
#define PRE 45
#define HCC 46
#define TME 47
#define RYP 48
#define IPP 49
#define INL 50
#define INP 51

/* CHA_DIVIDER Register Fields */
#define SAM_DIVIDER 52
#define OSR_DIVIDER11_7 53
#define OSR_DIVIDER6_0 54
#define FRD_CHA_DIVIDER 55

/* CHA_CONFIG Register Fields */
#define LPC 56
#define LPE 57
#define LPA 58
#define HSD 59
#define ACM 60
#define SAX 61
#define SAE 62
#define SAD 63
#define IDM 64
#define LWL 65
#define NAB 66
#define CCO 67
#define FP 68

/* CHA_LIMIT Register Fields */
#define ADC_LIMIT_14_7 69
#define ADC_LIMIT_6_0 70
#define LH 71

/* CHA_CONDIT Register Fields */
#define CMFB 72
#define IS 73
#define GA 74
#define LSE 75
#define VSG 76
#define VSCM_BIAS 77

/* CHA_PSHIFT Register Fields */
#define LPS 78
#define EMA 79
#define COARSE_4_1 80
#define COARSE_0 81
#define FINE 82

/* CHA_FILTER Register Fields */
#define PCE 83
#define PCI 84
#define PCP 85
#define SLC 86
#define ACP 87
#define EXC 88
#define EXS 89
#define EXT 90
#define PKD 91
#define QI 92
#define DEM 93
#define HS 94

/* CHA_SELECT Register Fields */
#define DS 95
#define VS 96
#define SVP 97
#define CES 98
#define AOS 99
#define AIN 100
#define IVM 101
#define FSO_V 102
#define FSC_VSCM 103
#define SENSEL_CHA_SELECT 104

/* US_BURST Register Fields */
#define PRI7_0 105
#define TUS 106
#define RXL 107
#define IQT 108
#define PIT 109
#define BURST 110

/* US_TRANSMIT Register Fields */
#define PRI11_8 111
#define TXSTART11_8 112
#define TXSTART7_0 113

/* RXCOUNTERL Register Fields */
#define RXCOUNTER7_0 114
#define PROBE_RXCOUNTER 115

/* US_PROBE Register Fields */
#define RXSTART7_0 116
#define PROBE_RXSTART 117

/* RXCOUTERH Register Fields */
#define RXCOUNTER23_16 118
#define RXCOUNTER15_8 119

/* US_RECEIVE Register Fields */
#define RXSTART23_16 120
#define RXSTART15_8 121

/* DSS_DIVIDER Register Fields */
#define DSS_DIVIDER14_8 122
#define DSS_DIVIDER7_0 123
#define FRD_DSS_DIVIDER 124

/* DDS_FIFO Register Fields */
#define FIFO11_8 125
#define FIFO7_0 126
#define AFIFO 127

/* DDS_CONFIG Register Fields */
#define MAS 128
#define FI 129
#define _2DF 130
#define DIF 131
#define D2G 132
#define FD 133
#define SEQ 134
#define BIP 135
#define SYM 136
#define CON 137
#define DEPTH 138

/* DAS_CONFIG Register Fields */
#define SDP 139
#define OSM 140
#define DAS_BIAS11_8 141
#define DAS_BIAS7_0 142

/* DSS_SELECT Register Fields */
#define DF 143
#define VF 144
#define MA 145
#define IVS1 146  /* MSB bit  */
#define IVS 147	  /* LSB bit */
#define FSO_D 148
#define FSC_DAS 149
#define SENSEL_DSS_SELECT 150

/* TIMERL Register Fields */
#define TIMER7_0 151
#define DUTY_CYCLE_TIMERL 152

/* TIMER_DIVL Register Fields */
#define TIMER_DIVIDER7_0 153
#define DUTY_CYCLE_TIMER_DIVL 154

/* TIMERH Register Fields */
#define TIMER23_16 155
#define TIMER15_8 156

/* TIMER_DIVH Register Fields */
#define TIMER_DIVIDER23_16 157
#define TIMER_DIVIDER15_8 158

/* AUX_CONFIG Register Fields */
#define AXS 159
#define PAS 160
#define PAM 161
#define AUX_BIAS11_8 162
#define AUX_BIAS7_0 163

/* PPR_COUNT Register Fields */
#define HITS_COUNTER15_8 164
#define HITS_COUNTER7_0 165

/* PPR_THRES Register Fields */
#define HITS_THRESHOLD15_8 166
#define HITS_THRESHOLD7_0 167

/* PPR_SELECT Register Fields */
#define PRG 168
#define AEF 169
#define A 170
#define B 171
#define PIM 172
#define CFT 173
#define _3ED 174
#define SCI 175
#define SENSEL_PPR_SELECT 176

/* CHA_FIFOL Register Fields */
#define CHA_FIFO19_12 177
#define CHA_FIFO11_4 178

/* FIFO_CONFIG Register Fields */
#define WTM 179
#define FLS 180
#define FM 181
#define FWD 182

/* CHA_FIFOH Register Fields */
#define CRC 183
#define NDAT 184
#define CHA_FIFO3_0 185

/* SENSIBUS_SET Register Fields */
#define SBUS_TIMESLOT 186
#define SIF 187
#define ADM 188
#define SMT 189
#define SR 190
#define E8 191
#define E4 192

/* DEVICE_ID0 Register Fields */
#define SERIALNUMBER1 193
#define SERIALNUMBER0 194

/* DEVICE_ID1 Register Fields */
#define SERIALNUMBER3 195
#define SERIALNUMBER2 196

/* DEVICE_ID2 Register Fields */
#define CHECKSUM 197
#define SERIALNUMBER4 198

/* USER_EFUSE Register Fields */
#define USER_EFUSE2 199
#define OS_TRIM 299

/* BANK 2 */
/* GPIO Register Fields */
#define GCS 201
#define GSC 202
#define GSO2 203
#define GSO1_0 204
#define GRD 205
#define GIN 206

/* PORT_FLAG1 Register Fields */
#define PI7 207
#define PI6 208
#define PI5 209
#define PI4 210
#define PI3 211
#define PI2 212
#define PI1 213
#define PI0 214

/* PORT_FLAG2 Register Fields */
#define PDS 215
#define PPD 216
#define PVT 217
#define PTE 218
#define PI11 219
#define PI10 220
#define PI9 221
#define PI8 222

/* PORT_FLAG3 Register Fields */
#define PHP 223
#define PE5 224
#define PE4 225
#define PE3 226
#define PE2 227
#define PE1 228
#define PE0 229

/* PORT_DELAY Register Fields */
#define _2A_DELAY4 230
#define _2A_DELAY3_0 232
#define SETTLING 233

/* PORT_SET Register Fields */
#define PORT_SET1 234
#define PORT_SET0 234

/* SENSIDRIVE */
#if defined(SENSIDRIVE)
/* MUX_CONFIG Register Fields */
#define SDA 235
#define OHM 236
#define OLM 237
#define VIE 238
#define DVE 239
#define DIE 240

/* DRV_SELECT Register Fields */
#define MSEL 241
#define SSEL 242
#define DSEL 243

/* DRV_AMP0 Register Fields */
#define AMP1_3_0 244
#define AMPO_11_8 245
#define AMPO_7_0 246

/* DRV_AMP1 Register Fields */
#define AMP2_7_0 247
#define AMP1_11_4 248


/* DRV_AMP2 Register Fields */
#define	AMP3_11_4 249
#define	AMP3_3_0 250
#define AMP2_11_8 251

/* DRV_AMP3 Register Fields */
#define AMP5_3_0 252
#define AMP4_11_8 253
#define AMP4_7_0 254


/* DRV_AMP4 Register Fields */
#define AMP6_7_0 255
#define AMP5_11_4 256

/* DRV_AMP5 Register Fields */
#define AMP7_11_4 257
#define AMP7_3_0 258
#define AMP6_11_8 259


/* DRV_AMP6 Register Fields */
#define AMPL_11_8 260
#define AMPL_7_0 261

/* DRV_QUENCH Register Fields */
#define QUENCHE 262

#endif

#endif /* API_LEVEL1_CHIP_REGISTERS_RUN9REGISTERDEFINITIONS_H_ */
