#ifndef SPREGISTER_H_
#define SPREGISTER_H_

#include "../../util/types.h"

#define SP_REGISTER_BANK1 1
#define SP_REGISTER_BANK2 2

#ifndef DEF_SP_REGISTER_FIELD
#define DEF_SP_REGISTER_FIELD
typedef struct struct_SPRegisterField SPRegisterField;
#endif

#ifndef DEF_SP_REGISTER
#define DEF_SP_REGISTER
typedef struct struct_SPRegister SPRegister;
#endif

typedef struct
{
    void (*setAddress)(SPRegister *, byte);

    byte (*getAddress)(SPRegister *);

    void (*setRead)(SPRegister *);

    void (*setWrite)(SPRegister *);

    void (*setSingle)(SPRegister *);

    void (*setSingleMSBOnly)(SPRegister *);

    void (*setMultiple)(SPRegister *);

    void (*setMultipleN)(SPRegister *, uint8_t);

    void (*setMultipleMSBOnly)(SPRegister *, uint8_t);

    bool (*isRead)(SPRegister *);

    bool (*isWrite)(SPRegister *);

    bool (*isSingle)(SPRegister *);

    bool (*isMultiple)(SPRegister *);

    bool (*isMSBOnly)(SPRegister *);

    void (*resetDefault)(SPRegister *);

    void (*setFieldValue)(SPRegister *, SPRegisterField *, byte);

    byte (*getFieldValue)(SPRegister *, SPRegisterField *);

} SPRegisterMethods;

struct struct_SPRegister
{

    /* address and M/S R/W info */
    byte address;

    /* MSB and LSB data */
    byte MSBByte;
    byte LSBByte;

    /* bank */
    uint8_t bank;

    /* register's default value */
    byte defaultValueMSB;
    byte defaultValueLSB;

    uint8_t N;

    SPRegisterMethods *methods;

};

void initSPRegister(SPRegister *, uint8_t bank, byte address, byte defaultValueMSB, byte defaultValueLSB);

#endif /* SPREGISTER_H_ */
