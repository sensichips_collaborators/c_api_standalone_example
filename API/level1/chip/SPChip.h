#ifndef SPCHIP_H_
#define SPCHIP_H_

#include "../../util/types.h"
#include "SPFamily.h"
#include "SPSensingElementOnChip.h"
#include "../../util/stringHandler.h"
#include <string.h>

#define CHIP_TYPE_UNICAST 0
#define CHIP_TYPE_BROADCAST 1
#define CHIP_TYPE_BROADCAST_SLOW 2
#define CHIP_TYPE_MULTICAST 3

#define SERIALNUMBER_SIZE 5

typedef struct struct_SPChip SPChip;


typedef struct
{
    void (*getShortAddress)(SPChip *spChip, byte *output);
    void (*getAddress)(SPChip *spChip, byte *output, uint8_t commMod, uint8_t protocolName);
    void (*setSerialNumber)(SPChip *spChip, char *serialNumber);
} SPChipMethods;

struct struct_SPChip
{
    SPFamily *family;
    byte i2cAddress;
    byte serialNumber[SERIALNUMBER_SIZE];
    uint8_t oscTrimOverride;
    uint8_t hsOscTrimOverride;
    SPSensingElementOnChip *spSensingElementOnChipList;
    uint8_t chip_type;

    SPChipMethods *methods;
};

void initSPChip(SPChip *);
#endif