#ifndef SPSENSINGELEMENT_H_
#define SPSENSINGELEMENT_H_

#include "../../level2/Items/SPParamItemRSense.h"
#include "../../level2/Items/SPParamItemInGain.h"
#include "../../level2/Items/SPParamItemOutGain.h"
#include "../../level2/Items/SPParamItemContacts.h"
#include "../../level2/Items/SPParamItemHarmonic.h"
#include "../../level2/Items/SPParamItemModeVI.h"
#include "../../level2/Items/SPParamItemMeasures.h"
#include "../../level2/Items/SPParamItemFilter.h"
#include "../../level2/Items/SPParamItemQIPhaseShift.h"
#include "../../level2/Items/SPParamItemPhaseShiftMode.h"
#include "../../level2/Items/SPParamItemQI.h"
#include "../../level2/Items/SPParamItemDCBiasN.h"
#include "../../level2/Items/SPParamItemDCBiasP.h"
#include "../../level2/Items/SPParamItemADCConversionRate.h"
#include "../../level2/Items/SPParamItemPortADC.h"

#define SENSINGELEMENTNAME_MAXLEN 35

typedef struct
{
    char name[SENSINGELEMENTNAME_MAXLEN]; /*"ONCHIP_TEMPERATURE","OFFCHIP_VOC","OFFCHIP_BEND"*/
    // EIS params
    SPParamItemRSense rSense;
    SPParamItemInGain inGain;
    SPParamItemOutGain outGain;
    SPParamItemContacts contacts;
    float frequency;
    SPParamItemHarmonic harmonic;
    SPParamItemModeVI modeVI;
    SPParamItemMeasures measure;
    SPParamItemFilter filter;
    SPParamItemPhaseShiftMode phaseShiftMode;
    SPParamItemQIPhaseShift phaseShift;
    SPParamItemQI qi;
    SPParamItemDCBiasN dcBiasN;
    SPParamItemDCBiasP dcBiasP;

    // ADC params
    SPParamItemADCConversionRate ADCConversionRate;
    short NData; /*Number of integer to read in FIFO*/
    SPParamItemPortADC inPortADC;

} SPSensingElement;

void initSPSensingElement(SPSensingElement *);

#endif /* SPSENSINGELEMENT_H_ */