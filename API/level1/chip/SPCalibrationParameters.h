#ifndef SPCALIBRATIONPARAMETERS_H_
#define SPCALIBRATIONPARAMETERS_H_

typedef struct{
	/**Linear Transformation: Y=X*m+n*/
	float m;
	float n;
	float threshold;
}SPCalibrationParameters;

#endif /* SPCALIBRATIONPARAMETERS_H_ */