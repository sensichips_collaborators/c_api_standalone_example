#ifndef SPSENSINGELEMENTONCHIP_H_
#define SPSENSINGELEMENTONCHIP_H_

#include "SPSensingElementOnFamily.h"
#include "SPCalibrationParameters.h"

typedef struct{
	SPSensingElementOnFamily* spSensingElementOnFamily;
	SPCalibrationParameters spCalibrationParameters;
}SPSensingElementOnChip;

#endif /* SPSENSINGELEMENTONCHIP_H_ */