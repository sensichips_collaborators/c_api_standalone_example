#ifndef SPREGISTERFIELD_H_
#define SPREGISTERFIELD_H_

#include "../../util/types.h"
#include "SPRegister.h"

#ifndef DEF_SP_REGISTER_FIELD
#define DEF_SP_REGISTER_FIELD
typedef struct struct_SPRegisterField SPRegisterField;
#endif

typedef struct
{
    void (*setValue)(SPRegisterField *, byte);

    byte (*getValue)(SPRegisterField *);
} SPRegisterFieldMethods;

struct struct_SPRegisterField
{

    byte mask;

    bool LSB;

    SPRegister *spRegister;

    SPRegisterFieldMethods *methods;

};

void initSPRegisterField(SPRegisterField *, byte mask, bool LSB, SPRegister *);

#endif /* SPREGISTERFIELD_H_ */