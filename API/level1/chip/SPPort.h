#ifndef SPPORT_H_
#define SPPORT_H_

/*TODO: migliorare gestione isInternal e noPort con setter??*/

#include "../../util/types.h"

#define PORT_SIZE 52

#define PORT0 0
#define PORT1 1
#define PORT2 2
#define PORT3 3
#define PORT4 4
#define PORT5 5
#define PORT6 6
#define PORT7 7
#define PORT8 8
#define PORT9 9
#define PORT10 10
#define PORT11 11
#define PORT_HP 12
#define PORT_EXT1 13
#define PORT_EXT2 14
#define PORT_EXT3 15
#define PORT_EXT1_1 16
#define PORT_EXT2_1 17
#define PORT_EXT3_1 18
#define PORT_EXT1_2 19
#define PORT_EXT3_2 20
#define PORT_EXT3_3 21
#define PORT_TEMPERATURE 22
#define PORT_VOLTAGE 23
#define PORT_LIGHT 24
#define PORT_DARK 25
#define PORT_NA 26
#define PORT_SHORT 27
#define PORT_OPEN 28
#define SHA 29
#define AUX 30
#define PORT_27 31
#define PORT_EXT1_3 32
#define PORT_EXT2_2 33
#define DPORT_INT 34
#define DPORT_RDY 35
#define DPORT_SDO 36
#define DPORT_SCL 37
#define DPORT_CS 38
#define PORT_EXT1_SP2 39
#define PORT_HP_ID 40
#define PORT_DAS_CSENSE 41
#define PORT_AUX_CSENSE 42
#define PORT_DRIVE0 43
#define PORT_DRIVE1 44
#define PORT_DRIVE2 45
#define PORT_DRIVE3 46
#define PORT_DRIVE4 47
#define PORT_DRIVE5 48
#define PORT_DRIVE6 49
#define PORT_DRIVE7 50
#define NOPORT 51

typedef struct struct_SPPort SPPort;

struct struct_SPPort {
    //byte portValue;
    byte portIndex;

    bool (*isInternal)(SPPort *);

    byte *ports;
};

void initSPPort(SPPort *);

#endif /* SPPORT_H_ */
