//
// Created by zen on 5/9/23.
//

#ifndef SPOFFCHIPSENSIMOX_H
#define SPOFFCHIPSENSIMOX_H

#include "../../level2/Parameters/SPMeasurementParameterEIS.h"
#include "../../level2/Parameters/SPMeasurementParameterAUX.h"
#include "../../config/SPConfiguration.h"

#define DEFAULT_HEATER_TIME  0
#define DEFAULT_HEATER_FREQUENCY  78125.0

typedef struct SPOffChipSENSIMOX SPOffChipSENSIMOX;

struct SPOffChipSENSIMOX {
    byte mode;
    float heaterVoltage;
    float heaterTemperature;
    float heaterTime;
    float heaterSensorFrequency;

    SPMeasurementParameterEIS *paramEISResOutput; /* To evaluate associated resistance */
    SPMeasurementParameterAUX *paramAUX; /* Stimulus parameter */

};

void initSPOffChipSENSIMOX(SPOffChipSENSIMOX *this_c);

void initSPOffChipSENSIMOX_parameters(SPOffChipSENSIMOX *this_c, SPMeasurementParameterEIS *paramEISResOutput,
                                      SPMeasurementParameterAUX *paramAUX, byte mode, float heaterVoltage,
                                      float heaterTemperature, float heaterTime, float heaterSensorFrequency,
                                      byte AUXOutPort, SPConfiguration *spConfiguration);

#endif //SPOFFCHIPSENSIMOX_H
