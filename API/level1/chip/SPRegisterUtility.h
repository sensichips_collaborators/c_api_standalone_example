#ifndef SPREGISTER_UTILITY_H_
#define SPREGISTER_UTILITY_H_

#include "../../util/types.h"
#include "SPRegister.h"
#include "SPRegisterField.h"

void splitValuesInFields(byte *values, SPRegisterField **fields, uint8_t N_fields);
void mergeFields(SPRegisterField **fields, uint8_t N_fields, byte *values, uint8_t N_values);

#endif /* SPREGISTER_UTILITY_H_ */