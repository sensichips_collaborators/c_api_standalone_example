#ifndef SPFAMILY_H_
#define SPFAMILY_H_

#include "../../util/types.h"
#include "SPSensingElementOnFamily.h"

#define HWVERSION_MAXLEN 5

typedef struct{
	
	byte id;
	char hwVersion[HWVERSION_MAXLEN];
	uint8_t oscTrim; 
	int sysClock;
	SPSensingElementOnFamily* spSensingElementOnFamilyList;

}SPFamily;

#endif /* SPFAMILY_H_ */