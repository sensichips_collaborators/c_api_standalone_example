#ifndef SPSENSINGELEMENTONFAMILY_H_
#define SPSENSINGELEMENTONFAMILY_H_

#include "SPSensingElement.h"
#include "SPPort.h"

typedef struct{

	char id[SENSINGELEMENTNAME_MAXLEN];
	SPSensingElement* spSensingElement;
	SPPort port;
}SPSensingElementOnFamily;


#endif /* SPSENSINGELEMENTONFAMILY_H_ */