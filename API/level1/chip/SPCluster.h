#ifndef SPCLUSTER_H_
#define SPCLUSTER_H_

#include "../../config/config.h"
#include "../../util/types.h"
 #include "SPChip.h"
#include "SPFamily.h"
#include "SPRegisterStatus.h"
#include "../protocols/util/SPInstructionCache.h"


typedef struct struct_SPCluster SPCluster;

struct struct_SPCluster
{
	byte clusterID;
    SPChip *broadcastChip;
	SPChip *chipList;
	SPChip *multicastChip;
	SPInstructionCache cache;
	uint8_t dimChipList;
	SPRegisterStatus registerStatus;
	uint8_t lastBankSelected;
};

void initSPCluster(SPCluster*);

#endif
