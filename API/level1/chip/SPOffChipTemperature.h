/*
 * SPOffChipTemperature.h
 *
 */

#ifndef API_LEVEL1_CHIP_SPOFFCHIPTEMPERATURE_H_
#define API_LEVEL1_CHIP_SPOFFCHIPTEMPERATURE_H_

#include "../../level2/Parameters/SPMeasurementParameterEIS.h"
#include <math.h>

/* Steinhart-Hart and Beta coefficients for Panasonic ERTJZEV104 100K/25°C thermistor */
#define DEFAULT_A  0.9045302325E-3f;
#define DEFAULT_B  2.127572260E-4f;
#define DEFAULT_C  0.0001842534550E-7f;
#define DEFAULT_BETA  4700.0f;  /* 25/50 °C */

typedef struct SPOffChipTemperature SPOffChipTemperature;

struct SPOffChipTemperature{

	/* Steinhart-Hart and Beta coefficients*/
    // float A;
    // float B;
    // float C;
    // float Beta;
    /* EIS internal parameter */
	SPMeasurementParameterEIS* paramInternalEIS;
	/* Functions */
	float (*calcTempCelsiusFromRes)(SPOffChipTemperature* this_c, float resistance);		/* Uses "Pisa" Method */
	// float (*calcTempCelsiusFromResSH)(SPOffChipTemperature* this_c, float resistance);  /* Uses Steinhart-Hart  Method */
	

};

void initSPOffChipTemperature(SPOffChipTemperature* this_c);

#endif /* API_LEVEL1_CHIP_SPOFFCHIPTEMPERATURE_H_ */
