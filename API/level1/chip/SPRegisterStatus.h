#ifndef SPREGISTER_STATUS_H_
#define SPREGISTER_STATUS_H_

#include "../../util/types.h"
#include "SPRegister.h"
#include "SPRegisterField.h"
#include "../../config/config.h"


#ifdef RUN7
#include "registers/DefinitionsRUN7.h"

#elif defined(RUN8)

#include "registers/DefinitionsRUN8.h"

#elif defined(RUN9)
#include "registers/DefinitionsRUN9.h"
#endif

typedef struct struct_SPRegisterStatus SPRegisterStatus;

struct struct_SPRegisterStatus {
    SPRegister registers[REGISTERS_SIZE];
    SPRegisterField fields[FIELDS_SIZE];
};

void initSPRegisterStatus(SPRegisterStatus *);

#endif /* SPREGISTER_STATUS_H_ */
