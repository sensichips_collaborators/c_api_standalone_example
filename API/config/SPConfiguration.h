#ifndef SPCONFIGURATION_H_
#define SPCONFIGURATION_H_

#include "config.h"
#include "../util/types.h"
#include "../level1/SPProtocol.h"
#include "../level1/chip/SPCluster.h"
#include "../level1/chip/SPSensingElementOnChip.h"

#define VCC_NORMAL 0
#define VCC_RATIOMETRIC 1

#define VCC_Th1 1800
#define VCC_Th2 2500
#define VCC_Th3 3300

#define VREF_1 1300
#define VREF_2 2000
#define VREF_3 2800

typedef struct struct_SPConfiguration SPConfiguration;

typedef struct struct_SPConfiguration {
    uint8_t driverName;     /*{"SPI","I2C","SENSIBUS"}*/
    SPCluster *cluster;
    SPProtocol *protocol;
    short VREF;       // da impostare con setVCC
    uint8_t VCC_MODE; // 0 NORMAL - 1 RATIOMETRIC
    short VCC;

    short VCC_DEFAULT;
    short VCC_MODE_DEFAULT;

    char description[5]; // controllare

    void (*setVCC)(SPConfiguration *this_c, uint8_t VCC_MODE, short VCC);

    bool (*communicationErrorCheck)(SPConfiguration *this_c);

    SPSensingElementOnChip *(*searchSensingElementOnChip)(SPConfiguration *this_c, char *sensorName);

} SPConfiguration;

void initSPConfiguration(SPConfiguration *spConfiguration);

#endif /* SPCONFIGURATION_H_¯ */