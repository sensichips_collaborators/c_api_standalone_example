/**
 * @ingroup level0
 *
 * @file init.h
 * @brief
 */

#ifndef INIT_H_
#define INIT_H_

#include "../level0/driver/SPDriver.h"
#include "../level0/driver/SPDriverESP8266.h"

#include "../level1/chip/SPCluster.h"
#include "../level1/chip/SPFamily.h"
#include "../level1/chip/SPChip.h"
#include "../level1/chip/SPRegisterStatus.h"
#include "../level1/SPProtocol.h"
#include "../level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"

#include "../level2/Parameters/SPMeasurementParameterADC.h"
#include "../level2/Parameters/SPMeasurementParameterEIS.h"
#include "../level2/Parameters/SPMeasurementParameterSENSOR.h"
#include "../level2/Parameters/SPMeasurementParameterQI.h"
#include "../level2/status/SPMeasurementStatus.h"
#include "../level2/SPMeasurementRUN8.h"
#include "../level2/SPMeasurementRUN9.h"

#include "SPConfiguration.h"


extern SPCluster spCluster;
extern SPConfiguration spConfiguration;
extern SPProtocol spProtocol;
extern SPFamily spFamily;

extern SPSensingElement spSensingElement[NUM_OF_SE];
extern SPSensingElementOnFamily spSeOnFamily[NUM_SE_ON_FAMILY];
extern SPSensingElementOnChip spSeOnChip[NUM_OF_CHIPS][NUM_SE_ON_CHIP];
extern SPChip multicastChip, broadcastChip, chipList[NUM_OF_CHIPS];

extern SPDriverESP8266 driverESP8266;
extern SPDriver spDriver;

extern SPMeasurementParameterSENSOR paramSENSOR;
extern SPMeasurementParameterEIS paramEIS;
extern SPMeasurementParameterAUX paramAUX;
extern SPMeasurementParameterADC paramADC;
extern SPMeasurementParameterQI paramQI;

extern SPMeasurementStatus status[NUM_OF_MEASUREMENT];

extern SPMeasurementRUNX spMeasurement[NUM_OF_MEASUREMENT];

/* EXT_OPERATION Variables */
extern SPOffChipTemperature spOffChipTemperature;
extern SPOffChipSENSIMOX spOffChipSENSIMOX;

extern int instanceID;
extern sp_double extOpOutput[NUM_OF_CHIPS][MEASURES_SIZE]; /* EXT_OP purpose */

void initSPVariables();

void fillChip();

void initSPProtocolESP8266();

void initSPMeasurements();

/**
 * @brief Initialize a part of the data structures and the API SENSIPLUS communication, setting the desired configuration
 * @param spConfiguration Data structure that contains the configuration to apply
 */
void setINIT();


#endif /* INIT_H_ */
