#ifndef CONFIG_H_
#define CONFIG_H_

/* UNCOMMENT THE RIGHT #define for the hardware used */
//#define RUN8
#define RUN9
//#define SENSIDRIVE



#define NUM_SE_ON_FAMILY 75
#define NUM_SE_ON_CHIP 75
#define NUM_OF_SE 75
#define NUM_OF_CHIPS 10
#define MAX_BYTE_REC 10

#define NUM_OF_MEASUREMENT 16

#define SYS_CLOCK 10000000

#define ADC_NDATA_BYTE 2
#define ADC_NDATA_INT 1
#endif /* CONFIG_H_ */
