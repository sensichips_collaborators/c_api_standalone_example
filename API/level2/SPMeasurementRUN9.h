/*
 * SPMeasurementRUN9.h
 *
 *  Created on: Dec 1, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_SPMEASUREMENTRUN9_H_
#define API_LEVEL2_SPMEASUREMENTRUN9_H_

#include "SPMeasurementRUNX.h"
#include "SPMeasurementRUN8.h"
#include  "./Parameters/SPMeasurementParameterAUX.h"

void initSPMeasurementRUN9(SPMeasurementRUNX* this_c);


#endif /* API_LEVEL2_SPMEASUREMENTRUN9_H_ */
