// /*
//  * SPMeasurementRun7.h
//  *
//  *  Created on: 11 nov 2019
//  *      Author: Luca Gerevini
//  */

// #ifndef API_LEVEL2_SPMEASUREMENTRUN7_H_
// #define API_LEVEL2_SPMEASUREMENTRUN7_H_

// #include "Parameters/SPMeasurementParameterPort.h"
// #include "SPMeasurementRun5.h"
// #include "../../level2_autorange/SPAutoRangeAlgorithm.h"
// #include "../level1/chip/SPOffChipSpecificConductivity.h"
// #include "../level1/chip/SPOffChipSpecificSusceptance.h"
// #include "../level1/chip/SPOffChipPracticalSalinity.h"

// int setADC_7(SPMeasurementRunX* this_c, SPMeasurementParameterADC* param, SPMeasuramentOutput* m);
// int setEIS_7(SPMeasurementRunX* this_c, SPMeasurementParameterEIS* param, SPMeasuramentOutput* m);
// void setSENSOR_7(SPMeasurementRunX* this_c, SPMeasurementParameterSENSOR* param, SPMeasuramentOutput* m);
// int getI_7(SPMeasurementRunX* this_c, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// void singleCommandRead(SPMeasurementRunX* this_c, char* command, SendInstructionInOut* ref, int numStageFIFO_ToRead, int output[][SETTINGS_ADC_NDATA_BYTE]);
// /* Update */
// int getQ_7(SPMeasurementRunX* this_c, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// int getIQ_7(SPMeasurementRunX* this_c, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// void get_7(SPMeasurementRunX* this_c, SendInstructionInOut* ref, SPMeasurementParameterADC* param, bool inPhase, uint8_t out[][SETTINGS_ADC_NDATA_BYTE]);

// #endif /* API_LEVEL2_SPMEASUREMENTRUN7_H_ */
