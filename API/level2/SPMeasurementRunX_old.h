// /*
//  * SPMeasurementRun5.h
//  *
//  *  Created on: 05/nov/2016
//  *      Author: Marco Ferdinandi
//  *      Property: Sensichips s.r.l.
//  */

// #ifndef API_LEVEL2_SPMEASUREMENTRUNX_H_
// #define API_LEVEL2_SPMEASUREMENTRUNX_H_

// #include "../config/SPConfiguration.h"
// #include "../config/config.h"

// #include "../util/types.h"
// #include "../util/printMCU.h" //ARDUINO PURPOSE
// #include "Parameters/SPMeasurementParameterSensor.h"
// #include "../util/SPComplex.h"
// #include "Parameters/SPMeasurementOutput.h"
// #include "AvailableFrequencies.h"
// #include "Parameters/SPMeasurementParameterPOT.h"
// #include "status/SPMeasurementStatusPOT.h"
// #include "status/SPMeasurementStatusQI.h"
// #include "../util/SPDelay.h"
// #include "Items/SPParamItemMeasures.h"

// #define DEVICE_ID_NUMOFBYTES 	 6 //Define the number of bytes for the DEVICE_ID
// #define NUM_OF_INSTRUCTIONS	    11 //Sono le istruzioni mandate dalla set EIS e set ADC

// /*!
//  * SPMeasurementRun5: Struct which contains all informations needed in SENSIPLUS API usage.
//  *
//  */
// typedef struct SPMeasurementRunX SPMeasurementRunX;

// struct SPMeasurementRunX {
// 	SPProtocol* spProtocol;
// 	SPConfiguration* spConfiguration;
// 	SPMeasurementParameter* lastParameter;
// 	uint8_t measureIndexToSave;
// 	//SPConfiguration* spConfiguration;/*Pointer to SPConfiguration data variable
//     //(contains all information about a device configuration including chip Cluster)*/
// 	SPCluster* spCluster;
// 	uint8_t numofChips;
// 	sp_double m[NUM_OF_CHIPS];
// 	sp_double n[NUM_OF_CHIPS];
// 	sp_double clock;/*inizialize 10000000.0*/
// 	byte osc_trim;/*inizialize 0x06*/
// 	SPMeasurementStatusQI* qiStatus;
// 	SPMeasurementStatusPOT* potStatus;

// 	int (*setEIS)(SPMeasurementRunX*, SPMeasurementParameterEIS*, SPMeasuramentOutput*);
// 	int (*setADC)(SPMeasurementRunX*, SPMeasurementParameterADC*, SPMeasuramentOutput*);
// 	int (*setPOT)(SPMeasurementRunX*, SPMeasurementParameterPOT*, SPMeasuramentOutput*);
// 	int (*getPOT)(SPMeasurementRunX*, sp_double[][MEASURES_SIZE], SPMeasurementParameterPOT*, SPMeasuramentOutput*);
// 	void (*setSENSOR)(SPMeasurementRunX*, SPMeasurementParameterSensor*, SPMeasuramentOutput*);
// 	void (*generateCalibrationParameters)(SPMeasurementRunX*, char*);
// 	int (*getDataByte)(SPMeasurementRunX*, uint8_t[][SETTINGS_ADC_NDATA_BYTE], SPMeasurementParameterADC*, bool);
// 	void (*get)(SPMeasurementRunX* this_c, SendInstructionInOut* ref, SPMeasurementParameterADC* param, bool inPhase, uint8_t out[][SETTINGS_ADC_NDATA_BYTE]);
// 	int (*getData)(SPMeasurementRunX*, int[][SETTINGS_ADC_NDATA_INT], SPMeasurementParameterADC*, bool);
// 	int (*getIQ)(SPMeasurementRunX*, sp_double[][MEASURES_SIZE], SPMeasurementParameterEIS*);
// 	int (*getI)(SPMeasurementRunX*, SendInstructionInOut*, sp_double[][MEASURES_SIZE], SPMeasurementParameterEIS*);
// 	int (*getQ)(SPMeasurementRunX*, SendInstructionInOut*, sp_double[][MEASURES_SIZE], SPMeasurementParameterEIS*);
// 	int (*getSingleEIS)(SPMeasurementRunX*, sp_double[][MEASURES_SIZE], SPMeasurementParameterEIS*);
// 	int (*evaluateImpedance)(SPMeasurementRunX*, SPMeasurementParameterEIS*, sp_double[][MEASURES_SIZE], SendInstructionInOut*);
// 	int (*getSENSOR)(SPMeasurementRunX*, sp_double[][MEASURES_SIZE], SPMeasurementParameterSensor*);
// 	void (*autoRange)(int); //SPMeasurementRunX*, SPMeasurementParameterEIS*,
// };


// void initSPMeasurementRUN5(SPMeasurementRunX* this_c);
// void initSPMeasurementRUN7(SPMeasurementRunX* this_c);
// void initSPMeasurementRUN8(SPMeasurementRunX* this_c);



// /***********************************************************************************************************************************************************************************
//  * @fn      setINIT
//  *
//  * @brief
//  *
//  *	setINIT function configures CLOCK SYSTEM and VOLTAGE parameter on SENSIPLUS chip.
//  *
//  * @param	SPMeasurementRun5* spMeasurement - pointer to the SPMeasurementRun5 structured variable.
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int setINIT(SPMeasurementRunX* spMeasurement);


// /*********************************************************************
//  * @fn      getMeasurement
//  *
//  * @brief
//  *
//  *	getMeasurement function executes a single measure of the analyte indicated in the input parameter for each chip
//  *	contained in the cluster (which has chip belonging to the same family).Function controls if the analyte is measurable
//  *	with the cluster indicated in the SPMeasurementRun4 structured variable. If the result of this control is
//  *	positive measures start, otherwis it returns EXIT_FAIL indicating that there was an error.
//  *
//  * @param	SPMeasurementRun4* spMeasurement - pointer to the SPMeasurementRun4 structured variable.
//  * @param	out[] - Output Parameter. Array of NUM_OF_CHIP sp_double values. Values represent measure made on each chip belonging the cluster.
//  * @param	analyteName - string indicating the requested analyte name.
//  * Examples of Possible Values:
//  *
//  * Family: VOC1
//  * Analytes: Acetone - Alcol ...
//  *
//  * Family: MEASURING_INSTRUMENT
//  * RelativeHumidity - FlexSensor_Thumb - FlexSensor_MiddleFinger - Temperature
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int getMeasurement(SPMeasurementRunX* spMeasurement,sp_double out[], char* analyteName);





// /*********************************************************************
//  * @fn      getDevicesID
//  *
//  * @brief
//  *
//  *	getDeviceID function reads the devices ID from each SENSIPLUS chip connected.
//  *
//  * @param	char* dest[][] - matrix of pointers to char. Each row is relative to a single chip, so the number of rows is equal to the number of SENSIPLUS chip connected.
// 			For each row there are DEVICE_ID_NUMOFBYTES pointer to char. The structure of a single row is described following:

// 			1° byte (dest[i][0]) : FAMILY CODE	(hex format)
// 			2° byte (dest[i][1]) : SERIAL NUMBER 0 					(hex format)
// 			3° byte (dest[i][2]) : SERIAL NUMBER 1 					(hex format)
// 			4° byte (dest[i][3]) : SERIAL NUMBER 2 					(hex format)
// 			5° byte (dest[i][4]) : SERIAL NUMBER 3 					(hex format)
// 			6° byte (dest[i][5]) : CHECKSUM 	   					(hex format)

// 			Example:
// 			elements on first row:
// 			dest[i][0] = "0x01" dest[i][1] = "0x01" dest[i][2] = "0x01" dest[i][3] = "0x01" dest[i][4] = "0x01" dest[i][5] = "0x01"

//  *
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int getDevicesID(char* dest[NUM_OF_CHIPS][DEVICE_ID_NUMOFBYTES]);




// /******************************************************************************************************
//  * @fn      checkDevices
//  *
//  * @brief
//  *
//  *	checkDevices controls if chip are working properly and indicates it in a int array with a status code
//  *	for each chip.
//  *
//  * @param	SPMeasurementRun4* spMeasurement - pointer to the SPMeasurementRun4 structured variable.
//  *
//  * @param	status[] - Output Parameter. Array of NUM_OF_CHIP int values. Each value represents a status code
//  * indicating the working status of the relative chip.
//  *
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  *
//  *******************************************************************************************************/
// int checkDevices(SPMeasurementRunX* spMeasurement, int status[]);






// /*! @fn setEIS
//  *
//  * 	@brief			This function configure chip to make impedance measures on a particular port indicated in the variable pointed
//  * 					by the first input pointer (SPMeasurementParameterEIS).
//  *
//  * 	@param	SPMeaurementParameterEIS* - pointer to a SPMeasurementParameterEIS variable which must contain all necessary informations
//  * 			to make an impedance measurement.
//  *
//  *	@param	SPMeasurementQIStatus* - pointer to a SPMeasurementQIStatus variable which stores all necessary informations to execute an
//  *									impedance measurement. It allows to implement a filter which executes the average of the last 'N'
//  *									read values (where N is settable).
//  *
//  *	@return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// //int setEIS_5(SPMeasurementRunX* spMeasuramentRun5, SPMeasurementParameterEIS* param, SPMeasuramentOutput* m);

// /*! @fn setADC
//  *
//  * 	@brief			This function configures the SENSIPLUS chip ADC by using a set of parameters passed in input
//  * 					with the pointer to a SPMeasurementPArameterADC variable.
//  *
//  * 	@param	SPMeasurementParameterADC* - pointer to a SPMeasurementParameterADC variable which must contain all necessary information
//  * 			needed to configure properly the SENSIPLUS ADC.
//  *
//  * 	@return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// //int setADC_5(SPMeasurementRunX* spMeasuramentRun5, SPMeasurementParameterADC* param, SPMeasuramentOutput* m);
// //
// //int setPOT_5(SPMeasurementRunX* spMeasurementRun5, SPMeasurementParameterPOT* param, SPMeasuramentOutput* m);
// //int getPOT_5(SPMeasurementRunX* spMeasurementRun5, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterPOT* param, SPMeasuramentOutput* m);

// //void setSENSOR_5(SPMeasurementRunX* sp_MeasurementRun5, SPMeasurementParameterSENSOR* param, SPMeasuramentOutput* m);

// //void generateCalibrationParameters_new_5(SPMeasurementRunX* param, char* sensorName);
// void sendSequence1(SPMeasurementRunX* spMeasuramentRun5,SendInstructionInOut* ref);
// void responseTokenizerByte(uint8_t* out, char* msg);
// //int getDataByte_5(SPMeasurementRunX* spMeasurementRun5,uint8_t out[][SETTINGS_ADC_NDATA_BYTE],SPMeasurementParameterADC* param, bool inPhase);
// //int getData_5(SPMeasurementRunX* spMeasuramentRun5,int output[][SETTINGS_ADC_NDATA_INT], SPMeasurementParameterADC* param, bool inPhase);
// //int getIQ_5(SPMeasurementRunX* spMeasuramentRun5, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// //int getI_5(SPMeasurementRunX* spMeasurementRun5, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT],SPMeasurementParameterEIS* param);
// //int getQ_5(SPMeasurementRunX* spMeasuramentRun5, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// //int getSingleEIS_5(SPMeasurementRunX* spMeasurementRun5,sp_double out[][EIS_NUM_OUTPUT],SPMeasurementParameterEIS* param);
// //int evaluateImpedance_5(SPMeasurementRunX* spMeasurementRun5, SPMeasurementParameterEIS* param, sp_double out[][EIS_NUM_OUTPUT], SendInstructionInOut* ref);
// //int getSENSOR_5(SPMeasurementRunX* sp_MeasurementRun5, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterSENSOR* param);
// #endif /* API_LEVEL2_SPMEASUREMENTRUNX_H_ */
