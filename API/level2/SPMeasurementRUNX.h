#ifndef API_LEVEL2_SPMEASUREMENTRUNX_H_
#define API_LEVEL2_SPMEASUREMENTRUNX_H_

#include "../config/SPConfiguration.h"
#include "../config/config.h"

#include "../util/types.h"
#include "../util/printMCU.h" //ARDUINO PURPOSE
#include "../util/SPComplex.h"
#include "AvailableFrequencies.h"
#include "Parameters/SPMeasurementParameterPOT.h"
#include "Parameters/SPMeasurementParameterSENSOR.h"
#include "Parameters/SPMeasurementParameterAUX.h"
#include "API/level1/chip/SPRegisterUtility.h"
#include "status/SPMeasurementStatus.h"
#include "status/SPMeasurementStatusPOT.h"
#include "../util/SPDelay.h"
#include "Items/SPParamItemMeasures.h"

#include <math.h>

static const char LOG_MESSAGE_MEASUREMENT[] = "SPMeasurement";
static int DEBUG_LEVEL = 2;



#define DEVICE_ID_NUMOFBYTES 6 // Define the number of bytes for the DEVICE_ID
// #define NUM_OF_INSTRUCTIONS	    11 //Sono le istruzioni mandate dalla set EIS e set ADC

#define NORMALIZATION_FACTOR 32768

typedef struct SPMeasurementRUNX SPMeasurementRUNX;

typedef struct
{
    void (*generateCalibrationParameters)(SPMeasurementRUNX* param, char* sensorName);
    void (*evaluateImpedance)(SPMeasurementRUNX* this_c, SPMeasurementParameterEIS* param, sp_double output[][MEASURES_SIZE]);
    void (*getData)(SPMeasurementRUNX* this_c, int output[][ADC_NDATA_INT], SPMeasurementParameterADC* param, bool inPhase);
    void (*getDataByte)(SPMeasurementRUNX* this_c, byte output[][ADC_NDATA_BYTE], SPMeasurementParameterADC* param, bool inPhase);
    void (*getSingleEIS)(SPMeasurementRUNX* this_c, sp_double out[][MEASURES_SIZE], SPMeasurementParameterEIS* param);
    void (*getSENSOR)(SPMeasurementRUNX* this_c, sp_double output[][MEASURES_SIZE], SPMeasurementParameterSENSOR* param);
    void (*get)(SPMeasurementRUNX* this_c, SPMeasurementParameterADC* param, bool inPhase, uint8_t output[][ADC_NDATA_BYTE]);
    void (*getI)(SPMeasurementRUNX* this_c, sp_double output[][MEASURES_SIZE], SPMeasurementParameterEIS* param);
    void (*getQ)(SPMeasurementRUNX* this_c, sp_double output[][MEASURES_SIZE], SPMeasurementParameterEIS* param);
    void (*getIQ)(SPMeasurementRUNX* this_c, sp_double output[][MEASURES_SIZE], SPMeasurementParameterEIS* param);
    void (*setADC)(SPMeasurementRUNX* this_c, SPMeasurementParameterADC* param);
    void (*setSENSOR)(SPMeasurementRUNX* this_c, SPMeasurementParameterSENSOR* param);
    void (*setEIS)(SPMeasurementRUNX* this_c, SPMeasurementParameterEIS* param);
    void (*setPOT)(SPMeasurementRUNX* this_c, SPMeasurementParameterPOT* param);
    void (*setAUX)(SPMeasurementRUNX* this_c, SPMeasurementParameterAUX* param);


    void (*autoRange)(int);

    void (*getPOT)(SPMeasurementRUNX* this_c, sp_double output[][MEASURES_SIZE], SPMeasurementParameterPOT* param);
} SPMeasurementRUNXMethods;

struct SPMeasurementRUNX
{
    SPProtocol* spProtocol;
    SPConfiguration* spConfiguration;
    int8_t measureIndexToSave;
    SPCluster* spCluster;
    sp_double m[NUM_OF_CHIPS];
    sp_double n[NUM_OF_CHIPS];
    bool sequentialMode;
    bool isDigitalPortSelected;
    bool OSSChanged;		  /* To improve performance in SENSIMOX loops */
    bool needAnalogSMRestart; /* To improve performance in SENSIMOX loops */
    SPMeasurementStatus* status;
    SPMeasurementStatusPOT* potStatus;

    SPMeasurementRUNXMethods* methods;
};

void initSPMeasurementRUNX(SPMeasurementRUNX* this_c);

// /***********************************************************************************************************************************************************************************
//  * @fn      setINIT
//  *
//  * @brief
//  *
//  *	setINIT function configures CLOCK SYSTEM and VOLTAGE parameter on SENSIPLUS chip.
//  *
//  * @param	SPMeasurementRun5* spMeasurement - pointer to the SPMeasurementRun5 structured variable.
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int setINIT(SPMeasurementRunX *spMeasurement);

// /*********************************************************************
//  * @fn      getMeasurement
//  *
//  * @brief
//  *
//  *	getMeasurement function executes a single measure of the analyte indicated in the input parameter for each chip
//  *	contained in the cluster (which has chip belonging to the same family).Function controls if the analyte is measurable
//  *	with the cluster indicated in the SPMeasurementRun4 structured variable. If the result of this control is
//  *	positive measures start, otherwis it returns EXIT_FAIL indicating that there was an error.
//  *
//  * @param	SPMeasurementRun4* spMeasurement - pointer to the SPMeasurementRun4 structured variable.
//  * @param	out[] - Output Parameter. Array of NUM_OF_CHIP sp_double values. Values represent measure made on each chip belonging the cluster.
//  * @param	analyteName - string indicating the requested analyte name.
//  * Examples of Possible Values:
//  *
//  * Family: VOC1
//  * Analytes: Acetone - Alcol ...
//  *
//  * Family: MEASURING_INSTRUMENT
//  * RelativeHumidity - FlexSensor_Thumb - FlexSensor_MiddleFinger - Temperature
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int getMeasurement(SPMeasurementRunX *spMeasurement, sp_double out[], char *analyteName);

// /*********************************************************************
//  * @fn      getDevicesID
//  *
//  * @brief
//  *
//  *	getDeviceID function reads the devices ID from each SENSIPLUS chip connected.
//  *
//  * @param	char* dest[][] - matrix of pointers to char. Each row is relative to a single chip, so the number of rows is equal to the number of SENSIPLUS chip connected.
//            For each row there are DEVICE_ID_NUMOFBYTES pointer to char. The structure of a single row is described following:

//            1° byte (dest[i][0]) : FAMILY CODE	(hex format)
//            2° byte (dest[i][1]) : SERIAL NUMBER 0 					(hex format)
//            3° byte (dest[i][2]) : SERIAL NUMBER 1 					(hex format)
//            4° byte (dest[i][3]) : SERIAL NUMBER 2 					(hex format)
//            5° byte (dest[i][4]) : SERIAL NUMBER 3 					(hex format)
//            6° byte (dest[i][5]) : CHECKSUM 	   					(hex format)

//            Example:
//            elements on first row:
//            dest[i][0] = "0x01" dest[i][1] = "0x01" dest[i][2] = "0x01" dest[i][3] = "0x01" dest[i][4] = "0x01" dest[i][5] = "0x01"

//  *
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  */
// int getDevicesID(char *dest[NUM_OF_CHIPS][DEVICE_ID_NUMOFBYTES]);

// /******************************************************************************************************
//  * @fn      checkDevices
//  *
//  * @brief
//  *
//  *	checkDevices controls if chip are working properly and indicates it in a int array with a status code
//  *	for each chip.
//  *
//  * @param	SPMeasurementRun4* spMeasurement - pointer to the SPMeasurementRun4 structured variable.
//  *
//  * @param	status[] - Output Parameter. Array of NUM_OF_CHIP int values. Each value represents a status code
//  * indicating the working status of the relative chip.
//  *
//  *
//  * @return  int - 0 EXIT_SUCCESS or -1 EXIT_FAIL
//  *
//  *******************************************************************************************************/
// int checkDevices(SPMeasurementRunX *spMeasurement, int status[]);

#endif /* API_LEVEL2_SPMEASUREMENTRUNX_H_ */
