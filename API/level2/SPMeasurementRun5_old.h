// /*
//  * SPMeasurementRun5.h
//  *
//  *  Created on: 07 nov 2019
//  *      Author: Luca
//  */

// #ifndef API_LEVEL2_SPMEASUREMENTRUN5_H_
// #define API_LEVEL2_SPMEASUREMENTRUN5_H_

// #include "SPMeasurementRunX.h"
// #include "../level1/chip/SPOffChipSpecificSusceptance.h"
// #include "../level1/chip/SPOffChipPracticalSalinity.h"

// int setEIS(SPMeasurementRunX* this_c, SPMeasurementParameterEIS* param, SPMeasuramentOutput* m);
// int setADC(SPMeasurementRunX* this_c, SPMeasurementParameterADC* param, SPMeasuramentOutput* m);

// int setPOT(SPMeasurementRunX* spMeasurementRun5, SPMeasurementParameterPOT* param, SPMeasuramentOutput* m);
// int getPOT(SPMeasurementRunX* spMeasurementRun5, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterPOT* param, SPMeasuramentOutput* m);

// void setSENSOR(SPMeasurementRunX* sp_MeasurementRun5, SPMeasurementParameterSENSOR* param, SPMeasuramentOutput* m);

// void generateCalibrationParameters_new(SPMeasurementRunX* param, char* sensorName);
// int getDataByte(SPMeasurementRunX* spMeasurementRun5,uint8_t out[][SETTINGS_ADC_NDATA_BYTE],SPMeasurementParameterADC* param, bool inPhase);
// void get(SPMeasurementRunX* this_c, SendInstructionInOut* ref, SPMeasurementParameterADC* param, bool inPhase, uint8_t out[][SETTINGS_ADC_NDATA_BYTE]);
// int getData(SPMeasurementRunX* this_c,int output[][SETTINGS_ADC_NDATA_INT], SPMeasurementParameterADC* param, bool inPhase);
// int getIQ(SPMeasurementRunX* this_c, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// int getI(SPMeasurementRunX* spMeasurementRun5, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT],SPMeasurementParameterEIS* param);
// int getQ(SPMeasurementRunX* this_c, SendInstructionInOut* ref, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterEIS* param);
// int getSingleEIS(SPMeasurementRunX* spMeasurementRun5,sp_double out[][EIS_NUM_OUTPUT],SPMeasurementParameterEIS* param);
// int evaluateImpedance(SPMeasurementRunX* this_c, SPMeasurementParameterEIS* param, sp_double out[][EIS_NUM_OUTPUT], SendInstructionInOut* ref);
// int evaluateImpedanceDataReader(SPMeasurementRunX* this_c, SPMeasurementParameterEIS* param, sp_double out[][EIS_NUM_OUTPUT], SendInstructionInOut* ref);
// void evaluateImpedanceCalc(SPMeasurementRunX* this_c, SPMeasurementParameterEIS* param, sp_double out[][EIS_NUM_OUTPUT], SendInstructionInOut* ref);
// int getSENSOR(SPMeasurementRunX* sp_MeasurementRun5, sp_double out[][EIS_NUM_OUTPUT], SPMeasurementParameterSENSOR* param);



// #endif /* API_LEVEL2_SPMEASUREMENTRUN5_H_ */
