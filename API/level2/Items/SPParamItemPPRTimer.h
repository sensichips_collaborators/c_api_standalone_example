#ifndef SPPARAMITEM_PPRTIMER_H_
#define SPPARAMITEM_PPRTIMER_H_

#include "../../config/config.h"
#include "../../util/types.h"

typedef struct
{
    uint8_t value;
    uint8_t min;
    uint8_t max;

} SPParamItemPPRTimer;

void initSPParamItemPPRTimer(SPParamItemPPRTimer*);
#endif /* SPPARAMITEM_PPRTIMER_H_ */
