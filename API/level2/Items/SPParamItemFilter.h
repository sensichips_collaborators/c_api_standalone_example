#ifndef SPPARAMITEM_FILTER_H_
#define SPPARAMITEM_FILTER_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define FILTER_SIZE 8

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define FILTER_1 0
#define FILTER_4 1
#define FILTER_8 2
#define FILTER_16 3
#define FILTER_32 4
#define FILTER_64 5
#define FILTER_128 6
#define FILTER_256 7
#endif

typedef struct
{
    byte value;
    byte items[FILTER_SIZE];

} SPParamItemFilter;

void initSPParamItemFilter(SPParamItemFilter*);

#endif /* SPPARAMITEM_FILTER_H_ */
