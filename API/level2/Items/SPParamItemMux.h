#ifndef SPPARAMITEM_MUX_H_
#define SPPARAMITEM_MUX_H_

#include "../../config/config.h"
#include "../../util/types.h"

typedef struct
{
    byte _DF;
    byte _VF;
    byte _DS;
    byte _VS;

} SPParamItemMux;

void initSPParamItemMux(SPParamItemMux*);

#endif /* SPPARAMITEM_MUX_H_ */
