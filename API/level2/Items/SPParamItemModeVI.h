#ifndef SPPARAMITEM_MODEVI_H_
#define SPPARAMITEM_MODEVI_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define MODEVI_SIZE 4

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define VOUT_VIN 0
#define VOUT_IIN 1
#define IOUT_VIN 2
#define IOUT_IIN 3
#endif

typedef struct
{
    byte value;
    byte items[MODEVI_SIZE];

} SPParamItemModeVI;

void initSPParamItemModeVI(SPParamItemModeVI*);

#endif /* SPPARAMITEM_MODEVI_H_ */
