#ifndef SPPARAMITEM_PPRINPORT_H_
#define SPPARAMITEM_PPRINPORT_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define PPRINPORT_SIZE 4

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define PPRINPORT_PORT0 0
#define PPRINPORT_PORT1 1
#define PPRINPORT_PORT_EXT1 2
#define PPRINPORT_PORT_EXT2 3
#endif

typedef struct
{
    byte value;
    byte items[PPRINPORT_SIZE];

} SPParamItemPPRInPort;

void initSPParamItemPPRInPort(SPParamItemPPRInPort*);

#endif /* SPPARAMITEM_PPRINPORT_H_ */
