#ifndef SPPARAMITEM_PPRHITSTHRESHOLD_H_
#define SPPARAMITEM_PPRHITSTHRESHOLD_H_

#include "../../config/config.h"
#include "../../util/types.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPPRHitsThreshold;

void initSPParamItemPPRHitsThreshold(SPParamItemPPRHitsThreshold*);
#endif /* SPPARAMITEM_PPRHITSTHRESHOLD_H_ */
