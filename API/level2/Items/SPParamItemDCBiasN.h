#ifndef SPPARAMITEM_DCBIASN_H_
#define SPPARAMITEM_DCBIASN_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemDCBiasN;

void initSPParamItemDCBiasN(SPParamItemDCBiasN*);

#endif /* SPPARAMITEM_DCBIASN_H_ */
