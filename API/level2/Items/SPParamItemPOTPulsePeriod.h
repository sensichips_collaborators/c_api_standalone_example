#ifndef SPPARAMITEM_POTPULSEPERIOD_H_
#define SPPARAMITEM_POTPULSEPERIOD_H_

#include "../../config/config.h"

typedef struct
{
    unsigned short value;
    unsigned short min;
    unsigned short max;

} SPParamItemPOTPulsePeriod;

void initSPParamItemPOTPulsePeriod(SPParamItemPOTPulsePeriod*, unsigned short min, unsigned short max);

#endif /* SPPARAMITEM_POTPULSEPERIOD_H_ */
