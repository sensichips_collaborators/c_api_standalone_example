#ifndef SPPARAMITEM_POTDCBIASN_H_
#define SPPARAMITEM_POTDCBIASN_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPOTDCBiasN;

void initSPParamItemPOTDCBiasN(SPParamItemPOTDCBiasN*, short min, short max);

#endif /* SPPARAMITEM_POTDCBIASN_H_ */
