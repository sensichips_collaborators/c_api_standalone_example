#ifndef SPPARAMITEM_PPRELECTRODES_H_
#define SPPARAMITEM_PPRELECTRODES_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define PPRELECTRODES_SIZE 2

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define PPRELECTRODES_3_ELECTRODES 0
#define PPRELECTRODES_2_ELECTRODES 1
#endif

typedef struct
{
    byte value;
    byte items[PPRELECTRODES_SIZE];

} SPParamItemPPRElectrodes;

void initSPParamItemPPRElectrodes(SPParamItemPPRElectrodes*);

#endif /* SPPARAMITEM_PPRELECTRODES_H_ */
