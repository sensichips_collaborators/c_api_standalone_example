/*
 * SPParamItemAUXBias.h
 *
 *  Created on: Dec 7, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXBIAS_H_
#define API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXBIAS_H_

#include "../../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemAUXBias;

void initSPParamItemAUXBias(SPParamItemAUXBias* this_c);



#endif /* API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXBIAS_H_ */
