/*
 * SPParamItemAUXInPort.h
 *
 *  Created on: Dec 7, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_ITEMS_SPPARAMITEMAUXINPORT_H_
#define API_LEVEL2_ITEMS_SPPARAMITEMAUXINPORT_H_

#include "../../../config/config.h"
#include "../../../util/types.h"

#define AUX_IN_PORT_SIZE 5


#define AUX_IN_PORT_NOPORT 0
#define AUX_IN_PORT_AUX 1
#define AUX_IN_PORT_DAS 2
#define AUX_IN_PORT_DAS_CE 3
#define AUX_IN_PORT_DAS_RE 4


typedef struct struct_SPParamItemAUXInPort SPParamItemAUXInPort;

struct struct_SPParamItemAUXInPort
{
    byte portValue;
    /* byte ports[AUX_IN_PORT_SIZE]; */ /* NOT USED */
};

void initSPParamItemAUXInPort(SPParamItemAUXInPort* this_c);


#endif /* API_LEVEL2_ITEMS_SPPARAMITEMAUXINPORT_H_ */
