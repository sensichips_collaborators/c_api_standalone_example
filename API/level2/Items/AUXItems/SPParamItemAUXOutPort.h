/*
 * SPParamItemAUXOutPort.h
 *
 *  Created on: Dec 7, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_ITEMS_SPPARAMITEMAUXOUTPORT_H_
#define API_LEVEL2_ITEMS_SPPARAMITEMAUXOUTPORT_H_


#include "../../../config/config.h"
#include "../../../util/types.h"


#define AUX_OUT_PORT_SIZE 8

#define AUX_OUT_PORT_NOPORT 0
#define AUX_OUT_PORT_SHA 1
#define AUX_OUT_PORT_AUX 2
#define AUX_OUT_PORT_DPORT_INT 3
#define AUX_OUT_PORT_DPORT_RDY 4
#define AUX_OUT_PORT_DPORT_SDO 5
#define AUX_OUT_PORT_DPORT_SCL 6
#define AUX_OUT_PORT_DPORT_CS 7


typedef struct struct_SPParamItemAUXOutPort SPParamItemAUXOutPort;

struct struct_SPParamItemAUXOutPort
{
    byte portValue;
    byte *ports;
};

void initSPParamItemAUXOutPort(SPParamItemAUXOutPort* this_c);



#endif /* API_LEVEL2_ITEMS_SPPARAMITEMAUXOUTPORT_H_ */
