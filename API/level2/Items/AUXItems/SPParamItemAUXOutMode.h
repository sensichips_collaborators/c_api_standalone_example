/*
 * SPParamItemAUXOutMode.h
 *
 *  Created on: Dec 7, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXOUTMODE_H_
#define API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXOUTMODE_H_

#include "../../../config/config.h"
#include "../../../util/types.h"

#define AUX_OUTMODE_AUXI 		0
#define AUX_OUTMODE_AUXV 		1
#define AUX_OUTMODE_AUXN 		2
#define AUX_OUTMODE_DISABLED 	3

typedef struct {
	byte value;

}SPParamItemAUXOutMode;

void initSPParamItemAUXOutMode(SPParamItemAUXOutMode*  this_c);

#endif /* API_LEVEL2_ITEMS_AUXITEMS_SPPARAMITEMAUXOUTMODE_H_ */
