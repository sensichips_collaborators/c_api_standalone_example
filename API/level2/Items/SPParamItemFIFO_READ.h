#ifndef SPPARAMITEM_FIFO_READ_H_
#define SPPARAMITEM_FIFO_READ_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemFIFO_READ;

void initSPParamItemFIFO_READ(SPParamItemFIFO_READ*);

#endif /* SPPARAMITEM_FIFO_READ_H_ */
