#ifndef SPPARAMITEM_OUTGAIN_H_
#define SPPARAMITEM_OUTGAIN_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define OUTGAIN_SIZE 8

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define OUTGAIN_0 0
#define OUTGAIN_1 1
#define OUTGAIN_2 2
#define OUTGAIN_3 3
#define OUTGAIN_4 4
#define OUTGAIN_5 5
#define OUTGAIN_6 6
#define OUTGAIN_7 7
#endif

typedef struct
{
    byte value;
    byte items[OUTGAIN_SIZE];

} SPParamItemOutGain;

void initSPParamItemOutGain(SPParamItemOutGain*);

#endif /* SPPARAMITEM_OUTGAIN_H_ */
