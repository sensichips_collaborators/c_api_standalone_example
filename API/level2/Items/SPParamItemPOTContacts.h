#ifndef SPPARAMITEM_POTCONTACTS_H_
#define SPPARAMITEM_POTCONTACTS_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define POTCONTACTS_SIZE 2

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define POTCONTACTS_TWO 0
#define POTCONTACTS_THREE 1
#endif

typedef struct
{
    byte value;
    byte items[POTCONTACTS_SIZE];

} SPParamItemPOTContacts;

void initSPParamItemPOTContacts(SPParamItemPOTContacts*);

#endif /* SPPARAMITEM_POTCONTACTS_H_ */
