#ifndef SPPARAMITEM_FIFO_DEEP_H_
#define SPPARAMITEM_FIFO_DEEP_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemFIFO_DEEP;

void initSPParamItemFIFO_DEEP(SPParamItemFIFO_DEEP*);

#endif /* SPPARAMITEM_DCBIASP_H_ */
