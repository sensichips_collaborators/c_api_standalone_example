#ifndef SPPARAMITEM_QIPHASESHIFT_H_
#define SPPARAMITEM_QIPHASESHIFT_H_

#include "../../config/config.h"
#include "../../util/types.h"

typedef struct
{
    uint8_t value;
    uint8_t min;
    uint8_t max;

} SPParamItemQIPhaseShift;

void initSPParamItemQIPhaseShift(SPParamItemQIPhaseShift*);

#endif /* SPPARAMITEM_QIPHASESHIFT_H_ */
