#ifndef SPPARAMITEM_MEASURES_H_
#define SPPARAMITEM_MEASURES_H_

#include "../../config/config.h"
#include "../../util/types.h"

#include <string.h>

#define MEASURES_SIZE 13

#if defined(RUN7) || defined(RUN8) || defined(RUN9)

#define MEASURES_IN_PHASE 0
#define MEASURES_QUADRATURE 1
#define MEASURES_CONDUCTANCE 2
#define MEASURES_SUSCEPTANCE 3
#define MEASURES_MODULE 4
#define MEASURES_PHASE 5
#define MEASURES_RESISTANCE 6
#define MEASURES_CAPACITANCE 7
#define MEASURES_INDUCTANCE 8
#define MEASURES_VOLTAGE 9
#define MEASURES_CURRENT 10
#define MEASURES_DELTA_V_APPLIED 11
#define MEASURES_CURRENT_APPLIED 12

#endif

typedef struct
{
    byte value;
    byte items[MEASURES_SIZE];
    char unitMeasuresLabel[MEASURES_SIZE][4];

} SPParamItemMeasures;

void initSPParamItemMeasures(SPParamItemMeasures *);

#endif /* SPPARAMITEM_MEASURES_H_ */
