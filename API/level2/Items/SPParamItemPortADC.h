#ifndef SPPARAMITEM_PORTADC_H_
#define SPPARAMITEM_PORTADC_H_

#include "../../config/config.h"
#include "../../util/types.h"

#if defined(RUN7) || defined(RUN8)
#define PORTADC_SIZE 4
#define PORTADC_IA 0
#define PORTADC_MUX 1
#define PORTADC_DAC 2
#define PORTADC_PPR 3
#elif defined(RUN9)
#define PORTADC_SIZE 2
#define PORTADC_IA 0
#define PORTADC_MUX 1
#endif

typedef struct
{
    byte value;
    byte items[PORTADC_SIZE];

} SPParamItemPortADC;

void initSPParamItemPortADC(SPParamItemPortADC*);

#endif /* SPPARAMITEM_PORTADC_H_ */
