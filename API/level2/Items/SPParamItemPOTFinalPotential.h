#ifndef SPPARAMITEM_POTFINALPOTENTIAL_H_
#define SPPARAMITEM_POTFINALPOTENTIAL_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPOTFinalPotential;

void initSPParamItemPOTFinalPotential(SPParamItemPOTFinalPotential*, short min, short max);

#endif /* SPPARAMITEM_POTFINALPOTENTIAL_H_ */
