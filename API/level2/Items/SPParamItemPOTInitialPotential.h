#ifndef SPPARAMITEM_POTINTIALPOTENTIAL_H_
#define SPPARAMITEM_POTINTIALPOTENTIAL_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPOTInitialPotential;

void initSPParamItemPOTInitialPotential(SPParamItemPOTInitialPotential*, short min, short max);

#endif /* SPPARAMITEM_POTINTIALPOTENTIAL_H_ */
