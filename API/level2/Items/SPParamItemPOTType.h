#ifndef SPPARAMITEM_POTTYPE_H_
#define SPPARAMITEM_POTTYPE_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define POTTYPE_SIZE 8

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define POTTYPE_POTENTIOMETRIC 0
#define POTTYPE_CURRENT 1
#define POTTYPE_LINEAR_SWEEP_VOLTAMMETRY 2
#define POTTYPE_STAIRCASE_VOLTAMMETRY 3
#define POTTYPE_SQUAREWAVE_VOLTAMMETRY 4
#define POTTYPE_NORMAL_PULSE_VOLTAMMETRY 5
#define POTTYPE_DIFFERENTIAL_PULSE_VOLTAMMETRY 6
#define POTTYPE_DIFFERENTIAL_PULSE_SENSOR 7
#endif

typedef struct
{
    byte value;
    byte items[POTTYPE_SIZE];

} SPParamItemPOTType;

void initSPParamItemPOTType(SPParamItemPOTType*);

#endif /* SPPARAMITEM_POTTYPE_H_ */
