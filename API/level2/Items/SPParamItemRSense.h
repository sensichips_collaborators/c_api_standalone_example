#ifndef SPPARAMITEM_RSENSE_H_
#define SPPARAMITEM_RSENSE_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define RSENSE_SIZE 4

#if defined(RUN7) || defined(RUN8)
#define RSENSE_50000 0
#define RSENSE_5000 1
#define RSENSE_500 2
#define RSENSE_50 3

#define RSENSE_50000_LABEL 50000
#define RSENSE_5000_LABEL 5000
#define RSENSE_500_LABEL 500
#define RSENSE_50_LABEL 50

#elif defined(RUN9)
#define RSENSE_50000 0
#define RSENSE_5000 1
#define RSENSE_500 2
#define RSENSE_50 3
/* Average actual values */
#define RSENSE_50000_LABEL 52700
#define RSENSE_5000_LABEL 5400
#define RSENSE_500_LABEL 536
#define RSENSE_50_LABEL 63
#endif

typedef struct struct_SPParamItemRSense SPParamItemRSense;

typedef struct struct_SPParamItemRSense
{
    byte value;
    byte items[RSENSE_SIZE];
    unsigned short (*getRSense)(SPParamItemRSense *this_c);
} SPParamItemRSense;

void initSPParamItemRSense(SPParamItemRSense *);

#endif /* SPPARAMITEM_RSENSE_H_ */
