#ifndef SPPARAMITEM_QI_H_
#define SPPARAMITEM_QI_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define QI_SIZE 4

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define QI_IN_PHASE 0
#define QI_QUADRATURE 1
#define QI_ANTI_PHASE 2
#define QI_ANT_QUADRATURE 3
#endif

typedef struct
{
    byte value;
    byte items[QI_SIZE];

} SPParamItemQI;

void initSPParamItemQI(SPParamItemQI*);

#endif /* SPPARAMITEM_QI_H_ */
