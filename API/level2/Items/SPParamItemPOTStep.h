#ifndef SPPARAMITEM_POTSTEP_H_
#define SPPARAMITEM_POTSTEP_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPOTStep;

void initSPParamItemPOTStep(SPParamItemPOTStep*, short min, short max);

#endif /* SPPARAMITEM_POTSTEP_H_ */
