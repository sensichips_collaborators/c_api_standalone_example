#ifndef SPPARAMITEM_HARMONIC_H_
#define SPPARAMITEM_HARMONIC_H_


#include "../../config/config.h"
#include "../../util/types.h"

#define HARMONIC_SIZE 3

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define FIRST_HARMONIC 0
#define SECOND_HARMONIC 1
#define THIRD_HARMONIC 2
#endif

typedef struct
{
    byte value;
    byte items[HARMONIC_SIZE];

} SPParamItemHarmonic;

void initSPParamItemHarmonic(SPParamItemHarmonic*);

#endif /* SPPARAMITEM_HARMONIC_H_ */
