#ifndef SPPARAMITEM_POTMEASURES_H_
#define SPPARAMITEM_POTMEASURES_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define POTMEASURES_SIZE 4

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define MEASURES_VOLTAGE 0
#define MEASURES_CURRENT 1
#define MEASURES_CURRENT_VS_VOLTAGE 2
#define MEASURES_DERIVATIVES_CURRENT_VS_VOLTAGE 3
#endif

typedef struct
{
    byte value;
    byte items[POTMEASURES_SIZE];
    char* unitMeasuresLabel[POTMEASURES_SIZE];

} SPParamItemPOTMeasures;

void initSPParamItemPOTMeasures(SPParamItemPOTMeasures*);

#endif /* SPPARAMITEM_POTMEASURES_H_ */
