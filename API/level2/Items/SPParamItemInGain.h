#ifndef SPPARAMITEM_INGAIN_H_
#define SPPARAMITEM_INGAIN_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define INGAIN_SIZE 4

#if defined(RUN7)
#define INGAIN_1 0
#define INGAIN_12 1
#define INGAIN_20 2
#define INGAIN_40 3

#define INGAIN_1_LABEL 1
#define INGAIN_12_LABEL 12
#define INGAIN_20_LABEL 20
#define INGAIN_40_LABEL 40

#elif defined (RUN8) || defined(RUN9)
#define INGAIN_1 0
#define INGAIN_10 1
#define INGAIN_20 2
#define INGAIN_50 3

#define INGAIN_1_LABEL 1
#define INGAIN_10_LABEL 10
#define INGAIN_20_LABEL 20
#define INGAIN_50_LABEL 50
#endif

typedef struct struct_SPParamItemInGain SPParamItemInGain;

typedef struct struct_SPParamItemInGain
{
    byte value;
    byte items[INGAIN_SIZE];
    unsigned short (*getInGain)(SPParamItemInGain *this_c);

} SPParamItemInGain;

void initSPParamItemInGain(SPParamItemInGain*);

#endif /* SPPARAMITEM_INGAIN_H_ */
