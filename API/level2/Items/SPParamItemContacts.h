#ifndef SPPARAMITEM_CONTACTS_H_
#define SPPARAMITEM_CONTACTS_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define CONTACTS_SIZE 2

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define CONTACTS_TWO 0
#define CONTACTS_FOUR 1
#endif

typedef struct
{
    byte value;
    byte items[CONTACTS_SIZE];

} SPParamItemContacts;

void initSPParamItemContacts(SPParamItemContacts*);

#endif /* SPPARAMITEM_CONTACTS_H_ */
