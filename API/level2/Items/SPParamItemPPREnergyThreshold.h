#ifndef SPPARAMITEM_PPRENERGYTHRESHOLD_H_
#define SPPARAMITEM_PPRENERGYTHRESHOLD_H_

#include "../../config/config.h"
#include "../../util/types.h"

typedef struct
{
    uint8_t value;
    uint8_t min;
    uint8_t max;

} SPParamItemPPREnergyThreshold;

void initSPParamItemPPREnergyThreshold(SPParamItemPPREnergyThreshold*);
#endif /* SPPARAMITEM_PPRENERGYTHRESHOLD_H_ */
