#ifndef SPPARAMITEM_PPRDETECTORTYPE_H_
#define SPPARAMITEM_PPRDETECTORTYPE_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define PPRDETECTORTYPE_SIZE 2

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define PPRDETECTORTYPE_SCINTILLATOR 0
#define PPRDETECTORTYPE_SOLIDSTATE 1
#endif

typedef struct
{
    byte value;
    byte items[PPRDETECTORTYPE_SIZE];

} SPParamItemPPRDetectorType;

void initSPParamItemPPRDetectorType(SPParamItemPPRDetectorType*);

#endif /* SPPARAMITEM_PPRDETECTORTYPE_H_ */
