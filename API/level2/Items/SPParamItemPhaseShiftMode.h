#ifndef SPPARAMITEM_PHASESHIFTMODE_H_
#define SPPARAMITEM_PHASESHIFTMODE_H_

#include "../../config/config.h"
#include "../../util/types.h"

#define PHASESHIFTMODE_SIZE 3

#if defined(RUN7) || defined(RUN8) || defined(RUN9)
#define PHASESHIFTMODE_QUADRANTS 0
#define PHASESHIFTMODE_COARSE 1
#define PHASESHIFTMODE_FINE 2
#endif

typedef struct
{
    byte value;
    byte items[PHASESHIFTMODE_SIZE];

} SPParamItemPhaseShiftMode;

void initSPParamItemPhaseShiftMode(SPParamItemPhaseShiftMode*);

#endif /* SPPARAMITEM_PHASESHIFTMODE_H_ */
