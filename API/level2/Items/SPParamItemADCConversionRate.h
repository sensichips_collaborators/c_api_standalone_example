#ifndef SPPARAMITEM_ADC_CONVERSION_RATE_H_
#define SPPARAMITEM_ADC_CONVERSION_RATE_H_

#include "../../config/config.h"

typedef struct
{
    float value;
    unsigned short min;
    unsigned short max;

} SPParamItemADCConversionRate;

void initSPParamItemADCConversionRate(SPParamItemADCConversionRate*);

#endif /* SPPARAMITEM_ADC_CONVERSION_RATE_H_ */
