#ifndef SPPARAMITEM_DCBIASP_H_
#define SPPARAMITEM_DCBIASP_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemDCBiasP;

void initSPParamItemDCBiasP(SPParamItemDCBiasP*);

#endif /* SPPARAMITEM_DCBIASP_H_ */
