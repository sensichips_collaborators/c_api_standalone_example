#ifndef SPPARAMITEM_POTPULSEAMPLITUDE_H_
#define SPPARAMITEM_POTPULSEAMPLITUDE_H_

#include "../../config/config.h"

typedef struct
{
    short value;
    short min;
    short max;

} SPParamItemPOTPulseAmplitude;

void initSPParamItemPOTPulseAmplitude(SPParamItemPOTPulseAmplitude*, short min, short max);

#endif /* SPPARAMITEM_POTPULSEAMPLITUDE_H_ */
