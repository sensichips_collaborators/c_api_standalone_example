/*
 * AvailableFrequencies.h
 *
 *  Created on: 09 ago 2018
 *      Author: Luca
 */

#ifndef LEVEL2_AVAILABLEFREQUENCIES_H_
#define LEVEL2_AVAILABLEFREQUENCIES_H_

#include "../util/types.h"
#include "../config/config.h"
#include <math.h>

typedef struct struct_AvailableFrequencies AvailableFrequencies;

struct struct_AvailableFrequencies
{
	int _FRD;
	float _OSM;
	int _DSS_DIVIDER;
	int _PRE;
	float availableFrequency;
	void (*availableFrequencies)(AvailableFrequencies *this_c, float frequencyVal);
	void (*availableDigitalFrequencies)(AvailableFrequencies *this_c, float frequencyVal);
};

void initAvailableFrequencies(AvailableFrequencies *this_c);

float log_(float x, float base);

#endif /* LEVEL2_AVAILABLEFREQUENCIES_H_ */
