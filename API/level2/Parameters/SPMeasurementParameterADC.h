#ifndef SPMEASUREMENTPARAMETERADC_H_
#define SPMEASUREMENTPARAMETERADC_H_

#include "../../util/types.h"
#include "../Items/SPParamItemADCConversionRate.h"
#include "../Items/SPParamItemInGain.h"
#include "../Items/SPParamItemRSense.h"
#include "../Items/SPParamItemPortADC.h"
#include "../Items/SPParamItemMux.h"
#include "../Items/SPParamItemFIFO_DEEP.h"
#include "../Items/SPParamItemFIFO_READ.h"
#include "../Items/SPParamItemFilter.h"
#include "../../level1/chip/SPPort.h"

typedef struct
{
	SPParamItemADCConversionRate conversionRate;
	SPParamItemPortADC inPort;
	bool commandX20;
	bool burstMode;
	SPParamItemRSense rSense;
	SPParamItemInGain inGain;
	SPParamItemMux mux;
  SPParamItemFIFO_DEEP FIFO_DEEP;
  SPParamItemFIFO_READ FIFO_READ;
  SPParamItemFilter filter;

} SPMeasurementParameterADC;

void initSPMeasurementParameterADC(SPMeasurementParameterADC *);

#endif /* SPMEASUREMENTPARAMETERADC_H_ */
