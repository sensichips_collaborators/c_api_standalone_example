#ifndef SPMEASUREMENTPARAMETERSENSORS_H_
#define SPMEASUREMENTPARAMETERSENSORS_H_

#include "SPMeasurementParameterEIS.h"
#include "SPMeasurementParameterADC.h"
#include "../Items/SPParamItemFilter.h"
#include "../Items/SPParamItemRSense.h"
#include "../Items/SPParamItemInGain.h"
#include "../../util/stringHandler.h"
#include "../../level1/chip/SPOffChipTemperature.h"
#include "API/level1/chip/SPOffChipSENSIMOX.h"

#define SENSOR_NAME_MAXLENGHT 35

typedef struct {
    char sensorName[SENSOR_NAME_MAXLENGHT];
    SPParamItemFilter filter;
    SPMeasurementParameterEIS *paramInternalEIS;
    SPMeasurementParameterADC *spMeasurementParameterADC;

    SPPort port;
    SPParamItemRSense rSense;
    SPParamItemInGain inGain;

    char sensorTypeName[SENSOR_NAME_MAXLENGHT];

    /* Custom sensors */
    SPOffChipTemperature *spOffChipTemperature;
    SPOffChipSENSIMOX *spOffChipSENSIMOX;

} SPMeasurementParameterSENSOR;

void initSPMeasurementParameterSENSOR(SPMeasurementParameterSENSOR *);

#endif /* SPMEASUREMENTPARAMETERSENSORS_H_ */
