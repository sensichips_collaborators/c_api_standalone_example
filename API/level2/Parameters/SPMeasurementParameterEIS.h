#ifndef SPMEARUREMENTPARAMETEREIS_H_
#define SPMEARUREMENTPARAMETEREIS_H_

#include "SPMeasurementParameterQI.h"
#include "../Items/SPParamItemMeasures.h"
#include "../Items/SPParamItemContacts.h"
#include "../Items/SPParamItemDCBiasP.h"
#include "../Items/SPParamItemDCBiasN.h"

typedef struct
{
	SPParamItemMeasures measure;
	SPParamItemDCBiasP dcBiasP;
	SPParamItemDCBiasN dcBiasN;
	SPParamItemContacts contacts;
	SPMeasurementParameterQI *QIparam;
} SPMeasurementParameterEIS;

void initSPMeasurementParameterEIS(SPMeasurementParameterEIS *);

#endif /* SPMEARUREMENTPARAMETEREIS_H_ */
