#ifndef SPMEASUREMENTPARAMETERPOT_H_
#define SPMEASUREMENTPARAMETERPOT_H_

#include "../Items/SPParamItemPOTInitialPotential.h"
#include "../Items/SPParamItemPOTFinalPotential.h"
#include "../Items/SPParamItemPOTContacts.h"
#include "../Items/SPParamItemPOTType.h"
#include "../Items/SPParamItemRSense.h"
#include "../Items/SPParamItemPOTDCBiasN.h"
#include "../Items/SPParamItemPOTPulsePeriod.h"
#include "../Items/SPParamItemPOTPulseAmplitude.h"
#include "../Items/SPParamItemPOTStep.h"
#include "SPMeasurementParameterADC.h"
#include "../../config/SPConfiguration.h"

typedef struct
{
    SPParamItemPOTInitialPotential initialPotential;
    SPParamItemPOTFinalPotential finalPotential;
    SPParamItemPOTDCBiasN dcBiasN;
    SPParamItemPOTStep step;
    SPParamItemPOTPulsePeriod pulsePeriod;
    SPParamItemPOTPulseAmplitude pulseAmplitude;
    bool alternativeSignal;
    SPParamItemPOTContacts contacts;
    SPParamItemPOTType type;
    SPMeasurementParameterADC *ADCparam;
} SPMeasurementParameterPOT;

void initSPMeasurementParameterPOT(SPMeasurementParameterPOT *param, SPConfiguration *config);

#endif /* SPMEASUREMENTPARAMETERPOT_H_ */
