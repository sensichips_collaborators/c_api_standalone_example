/*
 * SPMeasurementParameterAUX.h
 *
 *  Created on: Dec 6, 2022
 *      Author: zen
 */

#ifndef API_LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERAUX_H_
#define API_LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERAUX_H_

#include "../../util/types.h"
#include "../../config/config.h"

#include "../Items/AUXItems/SPParamItemAUXBias.h"
#include "../Items/AUXItems/SPParamItemAUXInPort.h"
#include "../Items/AUXItems/SPParamItemAUXOutMode.h"
#include "../Items/AUXItems/SPParamItemAUXOutPort.h"

typedef struct{
	SPParamItemAUXBias bias;
	SPParamItemAUXOutMode outMode;
	SPParamItemAUXInPort inPort;
	SPParamItemAUXOutPort outPort;

}SPMeasurementParameterAUX;

void initSPMeasurementParameterAUX(SPMeasurementParameterAUX *this_c);

#endif /* API_LEVEL2_PARAMETERS_SPMEASUREMENTPARAMETERAUX_H_ */
