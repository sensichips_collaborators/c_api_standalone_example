#ifndef SPMEASUREMENTPARAMETERQI_H_
#define SPMEASUREMENTPARAMETERQI_H_

#include "../../config/config.h"
#include "SPMeasurementParameterADC.h"

#include "../Items/SPParamItemRSense.h"
#include "../Items/SPParamItemInGain.h"
#include "../Items/SPParamItemHarmonic.h"
#include "../Items/SPParamItemModeVI.h"
#include "../Items/SPParamItemOutGain.h"
#include "../Items/SPParamItemQI.h"
#include "../Items/SPParamItemFilter.h"
#include "../Items/SPParamItemQIPhaseShift.h"
#include "../Items/SPParamItemPhaseShiftMode.h"

typedef struct
{
    SPParamItemHarmonic harmonic;
    SPParamItemModeVI modeVI;
    float frequency;
    SPParamItemOutGain outGain;
    SPParamItemQI qi;
    /* TODO: controllare se "ports" puntano alla stessa area di memoria */
    SPPort inport;
    SPPort outport;

    SPParamItemQIPhaseShift phaseShift; 
    SPParamItemPhaseShiftMode phaseShiftMode;

    SPMeasurementParameterADC *spMeasurementParameterADC;

} SPMeasurementParameterQI;

void initSPMeasurementParameterQI(SPMeasurementParameterQI *);


#endif /* SPMEASUREMENTPARAMETERQI_H_ */
