#ifndef SENDINSTRUCTIONINOUT_H_
#define SENDINSTRUCTIONINOUT_H_

#include "../../config/config.h"


#include "../../level1/chip/SPRegister.h"
#include "../../util/types.h"
#include "../../level1/chip/SPCluster.h"

typedef struct
{
	SPRegister *spRegister;
	byte recValues[NUM_OF_CHIPS][MAX_BYTE_REC];
	SPCluster *cluster;
} SendInstructionInOut;

void initSendInstructionInOut(SendInstructionInOut *this_c, SPCluster *cluster, SPRegister *spRegister);

#endif /* SENDINSTRUCTIONINOUT_H_ */
