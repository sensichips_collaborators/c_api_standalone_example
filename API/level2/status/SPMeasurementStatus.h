/*
 * SPMeasurementStatus.h
 *
 *  Created on: 22 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_
#define LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_

#include "../../util/types.h"
#include "../../level2/Parameters/SPMeasurementParameterADC.h"
#include "../../level2/Items/SPParamItemMeasures.h"

typedef struct {
	SPMeasurementParameterADC* spParameterADC;
	sp_double lastValue[NUM_OF_CHIPS][MEASURES_SIZE]; //buffer
	unsigned short counter;
	bool firstMeasure;
	unsigned short dimBuffer;

}SPMeasurementStatus;

void updateStatus(SPMeasurementStatus *this_c, sp_double output[][MEASURES_SIZE], uint8_t numOfChips);

void setSPMeasurementStatus(SPMeasurementStatus* this_c, SPMeasurementParameterADC* param);

void initSPMeasurementStatus(SPMeasurementStatus *this_c);

#endif /* LEVEL2_STATUS_SPMEASUREMENTSTATUS_H_ */
