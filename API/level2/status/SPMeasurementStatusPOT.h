/*
 * SPMeasurementStatusPOT.h
 *
 *  Created on: 14 feb 2019
 *      Author: Luca
 */

#ifndef LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_
#define LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_
#include "../Items/SPParamItemPOTInitialPotential.h"
#include "../Items/SPParamItemPOTFinalPotential.h"
#include "../../util/types.h"
#include "../../config/SPConfiguration.h"
#include "../Parameters/SPMeasurementParameterPOT.h"
#include "SPMeasurementStatus.h"
#include <math.h>
#include "../../util/SPDelay.h"

typedef struct SPMeasurementStatusPOT SPMeasurementStatusPOT;

struct SPMeasurementStatusPOT{

	SPParamItemPOTInitialPotential initialPotential;
	SPParamItemPOTFinalPotential finalPotential;
	SPParamItemPOTStep step;
	SPParamItemPOTPulseAmplitude pulseAmplitude;
	SPParamItemPOTPulsePeriod pulsePeriod;
	SPParamItemRSense rSense;
	SPParamItemInGain inGain;
	SPParamItemPOTType type;



	sp_double Stimulus;	//paramitem short
	
	sp_double kADC;
	
	
	unsigned short count;
	
	bool alternativeSignal;

	SPConfiguration* spConfiguration;
	
	bool cycleFinished;
	
	unsigned short restartStimulusCounter;
	
	SPMeasurementStatus status;
};


void initSPMeasurementStatusPOT(SPMeasurementStatusPOT* param, SPConfiguration* spConfiguration,
		SPMeasurementParameterPOT* spParameter);
sp_double normalizeVoltage(SPMeasurementStatusPOT* param, sp_double ADC);

#endif /* LEVEL2_STATUS_SPMEASUREMENTSTATUSPOT_H_ */
