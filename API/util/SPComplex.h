/*
 * SPComplex.h
 *
 *  Created on: 27/dic/2016
 *      Author: Marco Ferdinandi
 *      Property: Sensichips s.r.l.
 */

#ifndef SPCOMPLEX_H_
#define SPCOMPLEX_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include  "types.h"
#include "errorHandler.h"
#include "spMath.h"


typedef struct spcomplex_struct{
	sp_double re;
	sp_double im;
}SPComplex;


sp_double module(SPComplex* c);
sp_double phase(SPComplex* c);
int complexReciprocal(SPComplex* base, SPComplex* rec);
SPComplex complexSum(SPComplex* c1, SPComplex* c2);
SPComplex complexSub(SPComplex* c1, SPComplex* c2);

void printSPComplex(SPComplex* c);

#endif /* SPCOMPLEX_H_ */
