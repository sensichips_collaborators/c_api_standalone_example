/*
 * stringHandler.h
 *
 *  Created on: 23/dic/2016
 *      Author: Marco
 */

#ifndef STRINGHANDLER_H_
#define STRINGHANDLER_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "types.h"
#include "../config/config.h"

#define MAX_NUM_TOKENS		6
#define MAX_NUM_BYTETOKEN   25

 void conversionFromByteToInt(int out[][ADC_NDATA_INT], uint8_t data[][ADC_NDATA_BYTE], int numOfChips, int numOfBytePerRow);
short int conversionFromByteToIntSingle(uint8_t b1, uint8_t b2);

bool is_lower(char c);
bool is_upper(char c);
bool is_space(char c);
char to_lower(char c);
char to_upper(char c);

/*! @fn replaceCharacter
 *
 * 	@brief			This function searches and replaces an old character with a new one inside the string passed as input parameter.
 *
 * 	@param	char* tomodify - Input/Output Parameter. It's the string that will be modified.
 * 	@param const char old - Input parameter. It's the character that the function searches in order to replace it.
 * 	@param const char new - Input parameter. It's the character that will be used to replace the old one.
 */
void replaceCharacter(char* tomodify, const char* old, const char* news);


/*! @fn removeSubstring
 *
 * 	@brief			This function remove a string from one other if the last one contains the first.
 *
 * 	@param	char* s - Input/Output Parameter. It's the source string from which the function will remove a substring.
 * 	@param const char* toremove - InputParameter. It's the string that will be searched and removed from the source string.
 */
void removeSubstring(char *s,const char *toremove);


/*! @fn trim
 *
 * 	@brief			This function remove leading and trailing whitespace from input parameter
 *
 * 	@param	char* str - Input/Output Parameter.
 */
void trim(char *str);


/*! @fn subString
 *
 * 	@brief			This function cut a source string into a substring by using start-end indexes.
 *
 * 	@param	char* dest - Output Parameter. It's the output String (subString).
 * 	@param char* src - Input parameter. It's the input string which will be used to create the subString.
 * 	@param int start - Input parameter. It indicates the starting character position (in src string) from which the substring will start.
 * 	@param int end - Input parameter. It indicates the ending character position (in src string) which will end the substring.
 *
 */
void subString(char* dest, char* src, int start, int end);


/*! @fn fromBitToHexWithConcat
 *
 * 	@brief			This function translate a number formatted in a binary string to a hexadecimal string. It's able to distinguish between
 * 					16-bit and 8-bit binary strings and create hexadecimal output string properly.
 *
 * 	@param	char* instr - pointer to the string where the hexadecimal string will be concatenated.
 *
 *	@param	char* bitstr - pointer to the input bit string.
 *
 *	 *	Usage example:
 *	char instr[20] = "0x";
 *
 *	Using a 16-bit binary string:
 *	char bitstr[] = "0000101000011011";
 *
 *	fromBitToHexWithConcat(instr,bitstr);
 *
 *	After calling this function instr will be:
 *	instr = "0x0A1B";
 *
 *	Using a 8-bit binary string:
 *	char bitstr[] = "00001010";
 *
 *	fromBitToHexWithConcat(instr,bitstr);
 *
 *	After calling this function instr will be:
 *	instr = "0x0A";
 *
 */
void fromBitToHexWithConcat(char* instr, char* bitstr);


/*! @fn tokenizeString
 *
 * 	@brief			This function split a string into tokens, which are sequences of contiguous characters separated
 * 					by any of the characters that are par of delimiters. It insert the result inside an input/output parameter
 * 					which is a bidimensional array of char.
 *
 * 	@param	char tokens[][MAX_NUM_BYTETOKEN] - Input/Output parameter. It's a bidimensional array which will contain tokens found
 * 			in "string" parameter. There is a maximum value of characters for each tokens which is MAX_NUM_BYTETOKEN (20) defined in "stringHandler.h"
 *
 *	@param	int* numOfTokens - pointer to integer variable. The pointed variable is filled with the number of tokens found in "string" parameter.
 *
 *	@param char* string - pointer to char. It contains the input parameter.
 *
 *	@param char* delimiters - C string containing the delimiter characters.
 *
 *	Usage Example:
 *
 *	input:
 *	string = "0x03 0xFF 0xFA"
 *
 *	char tokens[10][MAX_NUM_BYTETOKEN];
 *	int numOfTokens;
 *	char delimiters[] = " ";
 *	tokenizeString( tokens,numOfTokens ,string,delimiters );
 *
 *	output:
 *	tokens[0] = "0x03"
 *	tokens[1] = "0xFF"
 *	tokens[2] = "0xFA"
 *
 *	numOfTokens = 3;
 *
 */
void tokenizeString(char tokens[][MAX_NUM_BYTETOKEN],int* numOfTokens ,char* string, char* delimiters);


/*! @fn convertToUpperCase
 *
 * 	@brief			This function converts all characters of the input string to the relative upper case character.
 *
 * 	@param	char* sPtr - Input/Output Parameter.
 */
void convertToUpperCase(char *sPtr);


/*! @fn convertToLowerCase
 *
 * 	@brief			This function converts all characters of the input string to the relative lower case character.
 *
 * 	@param	char* s - Input/Output Parameter.
 */
void convertToLowerCase(char *s);


/*! @fn compareString
 *
 * 	@brief			This function compares two string in order to see if they are equal or not. It's case insensitive.
 *
 * 	@param	char* s1 - Input parameter.
 * 	@param  char* s2 - Input parameter.
 *
 * 	@return - true if s1 and s2 are equal, false otherwise.
 */
bool compareString(char* s1, char* s2);


/*! @fn fromInt2BinaryString
 *
 * 	@brief			This function translates an int value into a binary formatted string by using a particular number of bit.
 *
 * 	@param	char* str - Pointer to the String where the function inserts the result.
 * 	@param  int numOfBits - Number of bit that the function will use to create the binary string.
 * 	@param int value - Integer value that the function translates in a binary formatted string.
 *
 */
void fromInt2BinaryString(char* str, int numOfBits, int value);

void fromuint8_t2HexString(char* strOut, uint8_t input);

#endif /* STRINGHANDLER_H_ */
