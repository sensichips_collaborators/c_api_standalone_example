/*
 * printHandler.h
 *
 *  Created on: 06 mag 2018
 *      Author: Luca Gerevini
 */

#ifndef UTIL_PRINTMCU_H_
#define UTIL_PRINTMCU_H_

#include <stdarg.h>
#include <string.h>

#define PRINT      false//false
#define DEBUG_LEVEL_0   false//false
#define DEBUG_LEVEL_1   false//false
#define DEBUG_LEVEL_2  false//false
#define DIM_BUFFER    250


//void print(const char* toPrint, char* value);
//void printd(const char* toPrint, double value);
//void print(const char* toPrint, ...);
unsigned long millis_c();

void print_f(const char *toPrint, ...);

void print_deb(const char *toPrint, ...);

void _DEBUG_(const char *msg_type, char *toPrint, int debLvl);


//void arddelay(int milliseconds);

#endif /* UTIL_PRINTMCU_H_ */
