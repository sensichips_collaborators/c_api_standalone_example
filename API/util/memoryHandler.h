/*
 * memoryHandler.h
 *
 *  Created on: 22/dic/2016
 *      Author: Marco
 */

#ifndef MEMORYHANDLER_H_
#define MEMORYHANDLER_H_


void* sp_mem_alloc(int len);
void sp_mem_free(void* ptr);




#endif /* MEMORYHANDLER_H_ */
