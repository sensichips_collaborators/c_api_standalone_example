#ifndef TYPES_H_
#define TYPES_H_


#if defined (ARDUINO)
#include "Arduino.h"
#else
#include <stdbool.h>
#include <stdint.h>
typedef uint8_t byte;
#endif

typedef double sp_double;

#ifndef NULL
#define NULL   ((void *) 0)
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif



#endif /* TYPES_H_ */
