/*
 * spMath.h
 *
 *  Created on: 17/gen/2017
 *      Author: Marco
 */

#ifndef SPMATH_H_
#define SPMATH_H_
#include <stdio.h>
#include <stdlib.h>
#include "types.h"

sp_double sp_sqrt(sp_double val);

#endif /* SPMATH_H_ */
