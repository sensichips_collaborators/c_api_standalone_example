/*
 * SPDimension.h
 *
 *  Created on: 07 apr 2018
 *      Author: Luca Gerevini
 */

#ifndef UTIL_SPDIMENSION_H_
#define UTIL_SPDIMENSION_H_

int sizeOfStruct(int spStruct, int spDim);

#endif /* UTIL_SPDIMENSION_H_ */
