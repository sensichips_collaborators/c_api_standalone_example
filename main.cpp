/* DRIVER includes */
#include "SPIFFS.h"
#include "DRIVER/ESP8266/CommSENSIBUS.h"
#include "DRIVER/ESP8266/CommUSB.h"
#include "DRIVER/ESP8266/MsgSENSIBUS.h"
#include "DRIVER/ESP8266/StatusSENSIBUS.h"
#include "EXT_OPERATION/fileHandlerMCU.h"
#include "EXT_OPERATION/ExtendedOperation.h"
#include "DRIVER/ESP8266/SendData.h"

/* API includes */
extern "C"
{
    #include "EXT_OPERATION/set_SPMeasurementParameterEIS.h"
    #include "API/level1/protocols/esp8266/SPProtocolESP8266_SENSIBUS.h"
    #include "API/config/init.h"
    #include "API/level1/chip/SPOffChipTemperature.h"
    #include "TinyML/classificators/MLP/classifier_config.h"
}

int instanceID;
CommSENSIBUS commSENSIBUS(DATI);
StatusSENSIBUS statusSENSIBUS;
MsgSENSIBUS msgSENSIBUS;
CommUSB commUSB;

void setup() {
    /*
     *Configuration of serial communication on USB
     * where the measurement values will be printed
     */
    Serial.setDebugOutput(true);
    Serial.begin(460800);
    delay(500);
    Serial.flush();
    Serial.println("INIT...");

    /*
     *  GPIO pin Configuration
     */
    pinMode(SENSIBUS_VCC, OUTPUT_OPEN_DRAIN);
    digitalWrite(SENSIBUS_VCC, HIGH);
    pinMode(LED_BUILTIN, OUTPUT);

    /************* API INITIALIZATION *************/
    instanceID = 0;
    /* Data structures initialization: spPort, spFamily, spCluster, ...  */
    initSPVariables();
    /*
     * Operation that involves loading the portion of the LayoutConfiguration
     * containing information about the chip present.
     * This information is defined by a json file which resides in the flash
     * of the device (in this case contained within a "SPIFFS" partition)
     * In the project, the configuration files are located in the data folder.
     */
    readFile();

    if (commSENSIBUS.tryDetectRUN()) {
        Serial.println("Chip detected!");
    } else {
        Serial.println("NO Chip detected!");
    }

    /* Initialize SENSIPLUS chips */
    setINIT();

    /* Activate cache*/
    spProtocol.isCacheActivated = true;


    sp_double output[NUM_OF_CHIPS][MEASURES_SIZE];

    /************* API CALLS FOR MEASUREMENTS LOOP *************/

    while (true) {

        /* VOLTAGE CONFIGURATION E MEASURE WITH SENSICHIPS API */
        strcpy(paramSENSOR.sensorName, "ONCHIP_VOLTAGE");
        paramSENSOR.filter.value = paramSENSOR.filter.items[FILTER_1];
        paramSENSOR.spMeasurementParameterADC->conversionRate.value = 50.0;
        paramSENSOR.port.portIndex = PORT_VOLTAGE;
        spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
        spMeasurement[0].methods->getSENSOR(&spMeasurement[0], output, &paramSENSOR);
        Serial.print("ONCHIP_VOLTAGE: ");
        Serial.println(output[0][0]);

        /* TEMPERATURE CONFIGURATION E MEASURE WITH SENSICHIPS API */
        strcpy(paramSENSOR.sensorName, "ONCHIP_TEMPERATURE");
        paramSENSOR.filter.value = paramSENSOR.filter.items[FILTER_1];
        paramSENSOR.spMeasurementParameterADC->conversionRate.value = 50.0;
        paramSENSOR.port.portIndex = PORT_TEMPERATURE;
        spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
        spMeasurement[0].methods->getSENSOR(&spMeasurement[0], output, &paramSENSOR);
        Serial.print("ONCHIP_TEMPERATURE: ");
        Serial.println(output[0][0]);

        /* LIGHT CONFIGURATION E MEASURE WITH SENSICHIPS API */
        strcpy(paramSENSOR.sensorName, "ONCHIP_LIGHT");
        paramSENSOR.filter.value = paramSENSOR.filter.items[FILTER_1];
        paramSENSOR.spMeasurementParameterADC->conversionRate.value = 50.0;
        paramSENSOR.port.portIndex = PORT_LIGHT;
        spMeasurement[0].methods->setSENSOR(&spMeasurement[0], &paramSENSOR);
        spMeasurement[0].methods->getSENSOR(&spMeasurement[0], output, &paramSENSOR);
        Serial.print("ONCHIP_LIGHT: ");
        Serial.println(output[0][0]);

        /* EIS CONFIGURATION E MEASURE WITH SENSICHIPS API ON PORT6 (THE GAS SENSOR) */
        paramEIS.contacts.value = paramEIS.contacts.items[CONTACTS_TWO];
        paramEIS.measure.value = paramEIS.measure.items[MEASURES_CAPACITANCE];
        paramEIS.QIparam->spMeasurementParameterADC->filter.value = paramEIS.QIparam->spMeasurementParameterADC->filter.items[FILTER_1];
        paramEIS.dcBiasN.value = 0;
        paramEIS.dcBiasP.value = 0;
        paramEIS.QIparam->harmonic.value = paramEIS.QIparam->harmonic.items[FIRST_HARMONIC];
        paramEIS.QIparam->modeVI.value = paramEIS.QIparam->modeVI.items[VOUT_IIN];
        paramEIS.QIparam->frequency = 78125.0;
        paramEIS.QIparam->outGain.value = paramEIS.QIparam->outGain.items[OUTGAIN_7];
        paramEIS.QIparam->qi.value = paramEIS.QIparam->qi.items[QI_IN_PHASE];
        paramEIS.QIparam->inport.portIndex = PORT6;
        paramEIS.QIparam->outport.portIndex = PORT6;
        paramEIS.QIparam->phaseShift.value = 0;
        paramEIS.QIparam->phaseShiftMode.value = paramEIS.QIparam->phaseShiftMode.items[PHASESHIFTMODE_QUADRANTS];
        paramEIS.QIparam->spMeasurementParameterADC->inGain.value = paramEIS.QIparam->spMeasurementParameterADC->inGain.items[INGAIN_50];
        paramEIS.QIparam->spMeasurementParameterADC->mux._DF = 0;
        paramEIS.QIparam->spMeasurementParameterADC->mux._VF = 1;
        paramEIS.QIparam->spMeasurementParameterADC->mux._DS = 2;
        paramEIS.QIparam->spMeasurementParameterADC->mux._VS = 3;
        paramEIS.QIparam->spMeasurementParameterADC->conversionRate.value = 50;
        paramEIS.QIparam->spMeasurementParameterADC->rSense.value = paramEIS.QIparam->spMeasurementParameterADC->rSense.items[RSENSE_50000];

        spMeasurement[0].methods->setEIS(&spMeasurement[0], &paramEIS);
        spMeasurement[0].methods->setADC(&spMeasurement[0], paramEIS.QIparam->spMeasurementParameterADC);
        spMeasurement[0].methods->getSingleEIS(&spMeasurement[0], output, &paramEIS);

        Serial.print("CAPACITANCE: ");
        Serial.println(output[0][MEASURES_CAPACITANCE]);

        Serial.println();
        Serial.flush();
        delay(5000);
    }

}


void loop() {
    delay(1);
}


/* Called inside sendInstruction(sp_send), send data to the  sensiplus mcuDriver == sendData */
void sendData(uint8_t header, uint8_t command, byte *addressByte, uint8_t dimAddress,
              byte *data, uint8_t dimData, byte *out) {

    msgSENSIBUS.reset();
    msgSENSIBUS.command = (unsigned) header;

    if (msgSENSIBUS.command & EXT_INSTR) {
        // TODO: manage ext instruction
        msgSENSIBUS.instructionType = msgSENSIBUS.command;
    } else {
        // Manage normal instruction
        //*** Receive address
        statusSENSIBUS.addressingMode = msgSENSIBUS.command & ADDRESS_MODE_MASK;

        if (statusSENSIBUS.addressingMode == LastAddressValue) {
            statusSENSIBUS.addressingMode = statusSENSIBUS.lastAddressingMode;
        } else {
            statusSENSIBUS.lastAddressingMode = statusSENSIBUS.addressingMode;
            if (statusSENSIBUS.addressingMode == NoAddressValue) {
                // No addressing byte to receive
                msgSENSIBUS.chipAddressLength = 0;
            } else if (statusSENSIBUS.addressingMode == ShortAddressValue) {
                msgSENSIBUS.chipAddressLength = 1;
            } else if (statusSENSIBUS.addressingMode == FullAddressValue) {
                msgSENSIBUS.chipAddressLength = 6;
            }
        }
        msgSENSIBUS.dataRcvLength = msgSENSIBUS.command & DATA_LENGTH_MASK;
        msgSENSIBUS.instructionType = NORMAL_INSTR;
    }
    // TODO: definire caso NO_INSTR

    if (msgSENSIBUS.instructionType & EXT_INSTR) {
        //TODO: caso istruzione estesa da implementare.
    } else { // NORMAL INSTRUCTION
        // Receive address
        msgSENSIBUS.chipAddressLength = dimAddress;
        for (int i = 0; i < dimAddress; i++) {
            msgSENSIBUS.chipAddress[i] = (unsigned) addressByte[i];
        }
        msgSENSIBUS.registerAddress[0] = (unsigned) command;
        // Establish if is a read or write operation (1 read, 0 false)
        statusSENSIBUS.isRead = IS_WRITE_MASK & msgSENSIBUS.registerAddress[0];
        //**** Receive data
        if (statusSENSIBUS.isRead) { // READ
            for (int i = 0; i < msgSENSIBUS.dataRcvLength; i++) {
                msgSENSIBUS.dataRcv[i] = 0xFF; // FF in order to wait for data
            }
        } else { // WRITE
            for (int i = 0; i < msgSENSIBUS.dataRcvLength; i++) {
                msgSENSIBUS.dataRcv[i] = (unsigned) data[i];
            }
        }
    }

    commSENSIBUS.transaction(msgSENSIBUS, statusSENSIBUS);

    for (uint8_t i = 0; i < dimData; i++) {
        out[i] = msgSENSIBUS.dataToHost[i];
    }
}
